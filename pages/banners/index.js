import { useEffect, useState } from "react";
import styled from "styled-components";
import { message, Switch  } from "antd";
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';

import TopHeader from "../../components/TopHeader";
import SideMenu from "../../components/SideMenu";
import ContainerHeader,{FilterOptions} from "../../components/ContainerHeader";
import TableComponent, { UsualActions } from "../../components/Table";
import CenteredModal from "../../components/CenteredModal";

import BannerActions from "../../redux/actions/banners"
import Themes from "../../themes/themes";
import Router from "next/router";
import { onNavigate } from "../../util/navigation";
import { useDispatch, useSelector } from "react-redux";

const Banners = () => {
	const dispatch = useDispatch()
	const {error, banner, banners, fetchingBanners, fetchBannerSuccessful,deletingBanner,deleteBannerSuccessful,  updatingBanner, updateBannerSuccessful,} = useSelector(state => state.banners)

	const [deleteConfirmationModal, setDeleteModalOpen] = useState(false);
	const [deleteId, setDeleteId] = useState("");
	const [searchText, setSearchText] = useState("")
	useEffect(()=>{
		dispatch(BannerActions.stateReset({
			fetchingBanners : false,
			fetchBannerSuccessful : false,

			deletingBanner:false,
			deleteBannerSuccessful : false,

			updatingBanner: false, 
			updateBannerSuccessful: false,
		}))
	},[])

	useEffect(()=>{
		if(error){
			message.error(error)
			dispatch(BannerActions.clearError())
		}
	},[error])

	useEffect(()=>{
		var queryParams = ""
		dispatch(BannerActions.getBannerRequest(queryParams))
	},[])

	useEffect(()=> {
		if(!fetchingBanners && fetchBannerSuccessful){
			dispatch(BannerActions.stateReset({
				fetchBannerSuccessful : false
			}))
		}
	},[fetchBannerSuccessful])

	useEffect(()=> {
		if(!deletingBanner && deleteBannerSuccessful){
			dispatch(BannerActions.stateReset({
				deleteBannerSuccessful:false
			}))
			setDeleteModalOpen(false);
			message.success("Banner Deleted successfully");
			dispatch(BannerActions.getBannerRequest())
		}
	},[deleteBannerSuccessful])

	useEffect(()=>{
		var queryParams = searchText ? `?search=${searchText}` : "";
		dispatch(BannerActions.getBannerRequest(queryParams))
	},[searchText])

	useEffect(()=>{
		if(!updatingBanner && updateBannerSuccessful){
			dispatch(BannerActions.stateReset({
				updateBannerSuccessful : false
			}))
			message.success("Banner updated successfully");
			dispatch(BannerActions.getBannerRequest(""))
		}
	},[updateBannerSuccessful])

	const handleOpenDeleteModal = (_id) => {
		setDeleteId(_id)
		setDeleteModalOpen(true);
	};

	const handleDeleteBanner = () => {
		dispatch(BannerActions.deleteBannerRequest(deleteId))
	};

	const handleCancel = () => {
		setDeleteModalOpen(false);
	};

	const handleNavigation = (path) => {
		return onNavigate(Router, path);
	};

	const handleStatus = (bannerId, status) => {
		dispatch(BannerActions.updateBannerRequest(bannerId, {isEnabled: status}))	
	};


	const columns = [
		{title: "Name", dataIndex: "name", key: "name"},
		{title: "Web Image", dataIndex: "imageId",key: "imageId", 
            render: imageId => (
                <img style={{width: 70, height: 70, borderRadius: 10}} src={imageId.imageUrl}/>
            ) 
        },
		{title: "Mobile Image", dataIndex: "mobileImageId",key: "imageId", 
            render: mobileImageId => (
                <img style={{width: 70, height: 70, borderRadius: 10}} src={mobileImageId.imageUrl}/> 
            ) 
        },
		{title: "Priority",key: "priority",dataIndex: "priority"},
		{title: "Type",key: "type",dataIndex: "type"},
		{title: "Link",key: "link",dataIndex: "link"},
        {title: "Enable (Public View)",
            render: text => {
                if (text.isEnabled) {
                    return ( <img style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer" }} src={require("../../Images/toggle-success-button.png")} onClick={() => handleStatus(text._id, false)}/>)
                } else {
                    return ( <img style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer"}} src={require("../../Images/toggle-failure-button.png")} onClick={() => handleStatus(text._id, true)}/>)
                }
            }
        },
		{title: "Action",key: "_id",
			render: (text, record) => (
				<UsualActions 
					onEdit={() => handleNavigation("/banners/edit/" + text._id)}
					// onView={() => handleNavigation("/banners/view/" + text._id)}
					onView={false}
					onDelete={() => handleOpenDeleteModal(text._id)}
				/>
			)
		},
	];

	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"2"} />
			<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=your_api_key_here"></script>

			<div className="container">
				<div className="card-root">
					<ContainerHeader
						title="Banners"
						button="Create"
						onClickButton={() => handleNavigation("/banners/create")}
					/>
					<FilterOptions 
						search	
						inputValue={searchText}
						placeholder={"Search"}
						onChangeInput={(e) => setSearchText(e.target.value)}
					/>
					<TableComponent loading={fetchingBanners} data={banners} columns={columns} />
					<CenteredModal
						visible={deleteConfirmationModal}
						loading={deletingBanner}
						submit={"Delete"}
						cancel="Cancel"
						handleSubmit={handleDeleteBanner}
						handleCancel={handleCancel}
						title="Delete Banner"
					>
						<p>Are you sure you want to delete banner ?</p>
					</CenteredModal>
				</div>
			</div>
		</Wrapper>
	);
};

export default Banners;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow-y: scroll;
	overflow-x: hidden;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
			width: 100%;
			background-color: #fff;
			padding: 20px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
	}
`;

const data = [
	{
		name: "John Brown",
		imageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
        mobileImageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
		isEnabled: true,
		priority: 3,
        type: "Large",
        link: "https://url"
	},
    {
		name: "John Brown",
		imageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
        mobileImageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
		isEnabled: false,
		priority: 5,
        type: "Small",
        link: "https://url"
	}
];
