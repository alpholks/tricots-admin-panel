import {useState,useEffect} from "react"
import styled from "styled-components"
import {message,Row,Col,Button,Select,Typography} from "antd"

import TopHeader from "../../../components/TopHeader"
import SideMenu from "../../../components/SideMenu"
import ContainerHeader from "../../../components/ContainerHeader"
import MapComponent from "../../../components/MapComponent"

import LocationActions from "../../../redux/actions/locations"
import WasteBinActions from "../../../redux/actions/wastebins"
import Themes from "../../../themes/themes"
import {onNavigate} from "../../../util/navigation"
import Router, { useRouter } from "next/router"
// import Wrapper from "../../components/TopHeader/TopHeader.style"
import InputContainer from "../../../components/InputContainer/InputContainer"
import { useDispatch, useSelector } from "react-redux"

const { Title } = Typography;


const EditWasteBin = () =>{

    const dispatch = useDispatch()
    const router = useRouter()
    const LocationState = useSelector(state => state.locations);
    const {error,wasteBinDetails,updatingWasteBin,updateWasteBinsSuccessful,fetchingWasteBinDetails,fetchWasteBinDetailsSuccessfull} = useSelector(state=>state.wastebins)

    const [wasteBinId,setWasteBinId] = useState("")
    const [wasteBinName,setWasteBinName] = useState("")
    const [wasteBinDescription,setWasteBinDescription] = useState("")
    const [locationId, setLocationId] = useState("");
    const [option,setOption] = useState("depot")

    useEffect(()=>{
		dispatch(WasteBinActions.stateReset({
            
            fetchingWasteBinDetail : false,
            fetchWasteBinDetailsSuccessfull : false,
            
            updatingWasteBin:false,
            updateWasteBinsSuccessful:false,

		}))
	},[])

    useEffect(()=>{
		dispatch(WasteBinActions.clearError())
	},[])

    useEffect(()=>{
		if(error){
            message.error(error)
            dispatch(LocationActions.clearError())
            dispatch(WasteBinActions.clearError())
        }    
    },[error])
    
    useEffect(()=>{
        if(LocationState.error || error){
            message.error(error)
            dispatch(LocationActions.clearError())
        }    
    },[LocationActions.error, error])


    useEffect(()=>{
        let {wasteBinId} = router.query

        if(wasteBinId){
            setWasteBinId(wasteBinId)
        }
        else{
            onNavigate(Router,"waste-bins")
        }
        
    },[])

    useEffect(()=>{
        if(wasteBinId)		
            console.log(wasteBinId);
            dispatch(WasteBinActions.getWasteBinDetailsRequest(wasteBinId))
    },[wasteBinId])


    useEffect(()=> {
		if(!fetchingWasteBinDetails && fetchWasteBinDetailsSuccessfull){
            setWasteBinName(wasteBinDetails.name);
            setWasteBinDescription(wasteBinDetails.description)
            setOption(wasteBinDetails.type)
            setLocationId(wasteBinDetails.locationId);
            dispatch(WasteBinActions.stateReset({
				fetchWasteBinDetailsSuccessfull : false
			}))
		}
    },[fetchWasteBinDetailsSuccessfull])

    useEffect(()=>{
		dispatch(LocationActions.getLocationsRequest())
	},[])

    useEffect(()=> {
		if(!LocationState.fetchingLocations && LocationState.fetchLocationSuccessful){
			dispatch(LocationActions.stateReset({
				fetchLocationSuccessful : false
			}))
		}
	},[LocationState.fetchLocationSuccessful])

    useEffect(()=>{
		if(!updatingWasteBin && updateWasteBinsSuccessful){
			dispatch(WasteBinActions.stateReset({
				updateWasteBinsSuccessful : false
			}))
			message.success("WasteBin updated successfully");
		}
    },[updateWasteBinsSuccessful])
    

    const handleClickCancel = () => {
		onNavigate(Router, "/waste-bins");
	};
    const handleClickSave = () =>{
        if(wasteBinName === "" || wasteBinDescription === ""){
			return message.warn("Please fill all the fields")
		}
        if(option!="depot"){
            dispatch(WasteBinActions.updateWasteBinRequest(wasteBinId,{
                name:wasteBinName,
                description:wasteBinDescription,
                locationId:locationId,
                type:'location'
            }))
        }
        else{
            dispatch(WasteBinActions.updateWasteBinRequest(wasteBinId,{
                name:wasteBinName,
                description:wasteBinDescription,
                type:'depot'
            }))
        }
        message.success("waste bin edited successfully")
    }


    return(
        <Wrapper>
            <TopHeader/>
            <SideMenu selectedKey={"6"}/>
                <div className="container">
                    <div className="card-root">
                        <ContainerHeader title="Edit Waste Bin"
                            onClickBackButton={() => Router.back()}
                        />
                        <div>
                            <Row gutter={24}>
                                <Col span={12}>
                                    <InputContainer
                                        title="Bin name"
                                        value={wasteBinName}
                                        onChange={(e) => setWasteBinName(e.target.value)}
                                    />
                                </Col>  
                                <Col span={12}>
                                    <InputContainer
                                        title="Bin Type"
                                        value={wasteBinDescription}
                                        onChange={(e) => setWasteBinDescription(e.target.value)}
                                    />
                                </Col> 
                                <Col span={12}>
                                    <Title level={5} className="title">Option</Title>
                                    <Select
                                      placeholder="Select Option"
                                      className="select"
                                      value={option}
                                      onChange={(value) => setOption(value)}
                                      style={{ width: '100%',backgroundColor:"#f0f0f0"}}
                                    >
                                        <Option value="depot">Depot</Option>
                                        <Option value="Location">Location</Option>
                                    </Select>    
                                </Col>   
                                <Col span={12} className={option=="depot"?"hide":""} >
                                    <Title level={5} className="title">Location</Title>
                                    <Select
                                      placeholder="Select Locatoin"
                                      className="select"
                                      value={locationId}
                                      onChange={(value) => setLocationId(value)}
                                      style={{ width: '100%',backgroundColor:"#f0f0f0"}}
                                    >
                                        {LocationState.locations.map((locations)=>{
                                            return <Option value={locations["_id"]}>{locations["name"]}</Option>
                                        })}
                                    </Select>    
                                </Col>       
                            </Row>    
                            <div className="row">
                                <Button  className="create-btn" onClick={handleClickSave} >
                                    Save
                                </Button>
                                <Button className="cancel-btn" onClick={handleClickCancel} >
                                    Cancel
                                </Button>
						    </div>
                        </div>
                    </div>
                </div>    
        </Wrapper>    
    );
};

export default EditWasteBin

const Wrapper = styled.div`

    background-color:${Themes.backgroundMain};
    height:100vh;
    width:100vw;
    overflow:scroll;
    .container{
        margin-left:256px;
        padding: 20px;
        margin-top: 10vh;
        display:flex;
        justify-content:center;
        align-items:center;
        .card-root{
            width:80%;
            background-color: ${Themes.white};
            padding:40px;
        }
    }
    .create-btn{
        background-color:${Themes.primary};
        border:none;
        margin:20px;
        width:200px;
        height:40px;
        font-weight:bold;
        font-size:16px;
        border-radius:5px;
        color:${Themes.white}
    }
    .cancel-btn{
        background-color:${Themes.cancelButton};
        border:none;
        margin:20px;
        width:200px;
        height:40px;
        font-weight:bold;
        font-size:16px;
        border-radius:5px;
        color:${Themes.white};
    }
    .row{
        display:flex;
        flex-direction:row;
        justify-content:center;
        align-items:center;
    }
    .hide{
        display:none;
    }

`;