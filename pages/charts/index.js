import { useEffect, useState } from "react";
import styled from "styled-components";
import { message, Switch  } from "antd";
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';

import TopHeader from "../../components/TopHeader";
import SideMenu from "../../components/SideMenu";
import ContainerHeader,{FilterOptions} from "../../components/ContainerHeader";
import TableComponent, { UsualActions } from "../../components/Table";
import CenteredModal from "../../components/CenteredModal";

import ChartActions from "../../redux/actions/charts"
import Themes from "../../themes/themes";
import Router from "next/router";
import { onNavigate } from "../../util/navigation";
import { useDispatch, useSelector } from "react-redux";

const Charts = () => {
	const dispatch = useDispatch()
	const {error, chart, charts, fetchingCharts, fetchChartSuccessful,deletingChart,deleteChartSuccessful, updatingChart,updateChartSuccessful} = useSelector(state => state.charts)

	const [deleteConfirmationModal, setDeleteModalOpen] = useState(false);
	const [deleteId, setDeleteId] = useState("");
	const [searchText, setSearchText] = useState("")
	useEffect(()=>{
		dispatch(ChartActions.stateReset({
			fetchingCharts : false,
			fetchChartSuccessful : false,

			updatingChart:false,
			updateChartSuccessful : false,

			deletingChart:false,
			deleteChartSuccessful : false
		}))
	},[])

	useEffect(()=>{
		if(error){
			message.error(error)
			dispatch(ChartActions.clearError())
		}
	},[error])

	useEffect(()=>{
		var queryParams = ""
		dispatch(ChartActions.getChartRequest(queryParams))
	},[])

	useEffect(()=> {
		if(!fetchingCharts && fetchChartSuccessful){
			dispatch(ChartActions.stateReset({
				fetchChartSuccessful : false,
				fetchingCharts: false
			}))
		}
	},[fetchChartSuccessful])

	useEffect(()=> {
		if(!deletingChart && deleteChartSuccessful){
			dispatch(ChartActions.stateReset({
				deleteChartSuccessful:false
			}))
			setDeleteModalOpen(false);
			message.success("Chart Deleted successfully");
			dispatch(ChartActions.getChartRequest(""))
		}
	},[deleteChartSuccessful])

	useEffect(()=>{
		var queryParams = searchText ? `?search=${searchText}` : "";
		dispatch(ChartActions.getChartRequest(queryParams))
	},[searchText])

	useEffect(()=>{
		if(!updatingChart && updateChartSuccessful){
			dispatch(ChartActions.stateReset({
				updateChartSuccessful : false
			}))
			message.success("Chart updated successfully");
			dispatch(ChartActions.getChartRequest(""))
		}
	},[updateChartSuccessful])

	const handleStatus = (chartId, status) => {
		dispatch(ChartActions.updateChartRequest(chartId, {isEnabled: status}))	
	};

	const handleOpenDeleteModal = (_id) => {
		setDeleteId(_id)
		setDeleteModalOpen(true);
	};

	const handleDeleteChart = () => {
		dispatch(ChartActions.deleteChartRequest(deleteId))
	};

	const handleCancel = () => {
		setDeleteModalOpen(false);
	};

	const handleNavigation = (path) => {
		return onNavigate(Router, path);
	};

	const columns = [
		{title: "Web Image", dataIndex: "imageId",key: "imageUrl", 
            render: imageId => (
                <img style={{width: 70, height: 70, borderRadius: 10}} src={imageId?.imageUrl}/>
            ) 
        },
		{title: "Type", dataIndex: "type", key: "type", render: type => {
			return (<div>
				{type === "shirt" ? "Shirt" : type === "pant" ? "Pant" : null}
			</div>)
		}},
		{title: "Enable (Public View)",
            render: text => {
                if (text.isEnabled) {
                    return ( <img style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer" }} src={require("../../Images/toggle-success-button.png")} onClick={() => handleStatus(text._id, false)}/>)
                } else {
                    return ( <img style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer"}} src={require("../../Images/toggle-failure-button.png")} onClick={() => handleStatus(text._id, true)}/>)
                }
            }
        },
		{title: "Action",key: "_id",
			render: (text, record) => (
				<UsualActions 
					onEdit={() => handleNavigation("/charts/edit/" + text._id)}
					onView={() => handleNavigation("/charts/view/" + text._id)}
					onDelete={() => handleOpenDeleteModal(text._id)}
				/>
			)
		},
	];

	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"3"} />
			<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=your_api_key_here"></script>

			<div className="container">
				<div className="card-root">
					<ContainerHeader
						title="Charts"
						button="Create"
						onClickButton={() => handleNavigation("/charts/create")}
					/>
					{/* <FilterOptions 
						search	
						inputValue={searchText}
						placeholder={"Search"}
						onChangeInput={(e) => setSearchText(e.target.value)}
					/> */}
					<TableComponent loading={fetchingCharts} data={charts} columns={columns} />
					<CenteredModal
						visible={deleteConfirmationModal}
						loading={deletingChart}
						submit={"Delete"}
						cancel="Cancel"
						handleSubmit={handleDeleteChart}
						handleCancel={handleCancel}
						title="Delete Chart"
					>
						<p>Are you sure you want to delete chart ?</p>
					</CenteredModal>
				</div>
			</div>
		</Wrapper>
	);
};

export default Charts;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow-y: scroll;
	overflow-x: hidden;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
			width: 100%;
			background-color: #fff;
			padding: 20px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
	}
`;

const data = [
	{
		name: "John Brown",
		imageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
        mobileImageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
		isEnabled: true,
		priority: 3,
        type: "Large",
        link: "https://url"
	},
    {
		name: "John Brown",
		imageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
        mobileImageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
		isEnabled: false,
		priority: 5,
        type: "Small",
        link: "https://url"
	}
];
