import { useEffect, useState } from "react";
import styled from "styled-components";
import { message, Row, Col, Button, Select, Typography, Input, Upload, Divider } from "antd";
const { Option } = Select;
const { Title } = Typography;
import { LoadingOutlined, PlusOutlined, UploadOutlined, MinusCircleOutlined } from '@ant-design/icons';

import TopHeader from "../../../components/TopHeader/TopHeader";
import SideMenu from "../../../components/SideMenu/SideMenu";
import ContainerHeader from "../../../components/ContainerHeader/ContainerHeader";
import InputContainer from "../../../components/InputContainer/InputContainer";

import CustomerActions from "../../../redux/actions/customers"
import ChartActions from "../../../redux/actions/charts"
import Themes from "../../../themes/themes";
import { onNavigate } from "../../../util/navigation";
import Router, { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";

const ViewChart = () => {
    const router = useRouter()
    const dispatch = useDispatch()

    const { error, chartDetails, fetchChartDetailsSuccessful, fetchingChartDetails } = useSelector(state => state.charts)


    const [chartId, setChartId] = useState("");
    const [type, setType] = useState("");
    const [details, setDetails] = useState([]);
    const [isEnabled, setIsEnabled] = useState(false);
    const [imageUrl, setImageUrl] = useState("");
    const [imageId, setImageId] = useState("");
    const [webLoading, setWebLoading] = useState(false);
    const [mobileLoading, setMobileLoading] = useState(false);

    useEffect(() => {
        dispatch(ChartActions.stateReset({
            fetchChartDetailsSuccessful: false,
            fetchingChartDetails: false
        }))
        let { chartId } = router.query
        if (chartId) {
            setChartId(chartId)
        } else {
            onNavigate(Router, "charts")
        }
    }, [])


    useEffect(() => {
        if (chartId) {
            dispatch(CustomerActions.getCustomerRequest())
            dispatch(ChartActions.getChartDetailsRequest(chartId))
        }
    }, [chartId])

    useEffect(() => {
        if (!fetchingChartDetails && fetchChartDetailsSuccessful) {
            dispatch(ChartActions.stateReset({
                fetchChartDetailsSuccessful: false
            }))
            console.log("chartDetails", chartDetails)
            setType(chartDetails.type)
            setIsEnabled(chartDetails.isEnabled)
            setDetails(chartDetails.details)
            setImageUrl(chartDetails.imageId.imageUrl)
            setImageId(chartDetails.imageId._id)
        }
    }, [fetchChartDetailsSuccessful])

    return (
        <Wrapper>
            <TopHeader />
            <SideMenu selectedKey={"10"} />
            <div className="container">
                <div className="card-root">
                    <ContainerHeader title={name}
                        onClickBackButton={() => Router.back()}
                    />
                    <div>
                        <Row gutter={24}>
                            <Col span={12}>
                                <Title level={5} className="title">Type</Title>
                                <Select
                                    placeholder=""
                                    className="select"
                                    value={type}
                                    disabled
                                    onChange={(value) => setType(value)}
                                    style={{ width: '100%', backgroundColor: "#f0f0f0" }}
                                >
                                    <Option value="shirt">Shirt</Option>
                                    <Option value="pant">Pant</Option>
                                </Select>
                            </Col>
                            <Col span={24}>
                                <Title level={5} className="title" style={{ display: "inline-block", marginTop: "5%" }}>Enable</Title>
                                {isEnabled
                                    ? <img
                                        style={{ width: 50, height: 30, marginLeft: 50,  }}
                                        src={require("../../../Images/toggle-success-button.png")} />
                                    : <img
                                        style={{ width: 50, height: 30, marginLeft: 50,  }}
                                        src={require("../../../Images/toggle-failure-button.png")} />
                                }
                            </Col>

                            <Col span={24} style={{ marginTop: "3%" }}>
                                <Title level={5} className="title">Web Image</Title>
                                <Upload
                                    name="avatar"
                                    disabled
                                    listType="picture-card"
                                    className="avatar-uploader"
                                    showUploadList={false}
                                    onStart={(file) => {
                                        console.log("onStart", file, file.name);
                                    }}
                                >
                                    {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%', height: "100%" }} /> : (
                                        <div>
                                            {webLoading ? <LoadingOutlined /> : <PlusOutlined />}
                                            <div style={{ marginTop: 8 }}>Upload</div>
                                        </div>
                                    )}
                                </Upload>
                            </Col>
                            <Col>
                                {type === "shirt" && details.length ?
                                    details.map((detail, index) => {
                                        return (
                                            <Row gutter={24}>
                                                <Col span={5}>
                                                    <InputContainer
                                                        title="Size"
                                                        disabled
                                                        value={detail.size}
                                                        onChange={async (e) => {
                                                            await __func__updateDetails("shirt", e.target.value, detail.chest, detail.length, detail.sleeveLength, index)
                                                        }}
                                                    />
                                                </Col>
                                                <Col span={5}>
                                                    <InputContainer
                                                        title="Chest"
                                                        disabled
                                                        value={detail.chest}
                                                        onChange={async (e) => {
                                                            await __func__updateDetails("shirt", detail.size, e.target.value, detail.length, detail.sleeveLength, index)
                                                        }}
                                                    />
                                                </Col>
                                                <Col span={5}>
                                                    <InputContainer
                                                        title="Length"
                                                        disabled
                                                        value={detail.length}
                                                        onChange={async (e) => {
                                                            await __func__updateDetails("shirt", detail.size, detail.chest, e.target.value, detail.sleeveLength, index)
                                                        }}
                                                    />
                                                </Col>
                                                <Col span={5}>
                                                    <InputContainer
                                                        title="Sleeve Length"
                                                        disabled
                                                        value={detail.sleeveLength}
                                                        onChange={async (e) => {
                                                            await __func__updateDetails("shirt", detail.size, detail.chest, detail.length, e.target.value, index)
                                                        }}
                                                    />
                                                </Col>
                                                <Col span={2}></Col>
                                                {/* <Col span={2} style={{ marginTop: 50 }}>
                                                    <MinusCircleOutlined onClick={() => { __func__removeDetails('shirt', index) }} />
                                                </Col> */}
                                            </Row>
                                        )
                                    }) : type === "pant" && details.length ?
                                        details.map((detail, index) => {
                                            return (
                                                <Row gutter={24}>
                                                    <Col span={5}>
                                                        <InputContainer
                                                            title="Size"
                                                            disabled
                                                            value={detail.size}
                                                            onChange={async (e) => {
                                                                await __func__updateDetails("pant", e.target.value, detail.waist, detail.totalLength, detail.inseamLength, index)
                                                            }}
                                                        />
                                                    </Col>
                                                    <Col span={5}>
                                                        <InputContainer
                                                            title="Waist"
                                                            disabled
                                                            value={detail.waist}
                                                            onChange={async (e) => {
                                                                await __func__updateDetails("pant", detail.size, e.target.value, detail.totalLength, detail.inseamLength, index)
                                                            }}
                                                        />
                                                    </Col>
                                                    <Col span={5}>
                                                        <InputContainer
                                                            title="Total Length"
                                                            disabled
                                                            value={detail.totalLength}
                                                            onChange={async (e) => {
                                                                await __func__updateDetails("pant", detail.size, detail.waist, e.target.value, detail.inseamLength, index)
                                                            }}
                                                        />
                                                    </Col>
                                                    <Col span={5}>
                                                        <InputContainer
                                                            title="Inseam Length"
                                                            disabled
                                                            value={detail.inseamLength}
                                                            onChange={async (e) => {
                                                                await __func__updateDetails("pant", detail.size, detail.waist, detail.totalLength, e.target.value, index)
                                                            }}
                                                        />
                                                    </Col>
                                                    <Col span={2}></Col>
                                                    {/* <Col span={2} style={{ marginTop: 50 }}>
                                                        <MinusCircleOutlined onClick={() => { __func__removeDetails('pant', index) }} />
                                                    </Col> */}
                                                </Row>
                                            )
                                        }) : null
                                }
                            </Col>
                        </Row>
                    </div>
                </div>
            </div>
        </Wrapper>
    );
};

export default ViewChart;

const Wrapper = styled.div`
	background-color: ${Themes.backgroundMain};
	height: 100%;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		display: flex;
		justify-content: center;
		align-items: center;
		height: 90%;
		.card-root {
			width: 80%;
			height: 100%;
			background-color: ${Themes.white};
			padding: 40px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.cancel-btn {
		background-color: ${Themes.cancelButton};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.row {
		display: flex;
		flex-direction: row;
		justify-content: center;
		align-items: center;
	}
`;
