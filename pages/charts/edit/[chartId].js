import { useEffect, useState } from "react";
import styled from "styled-components";
import { message, Input, Row, Col, Typography, Button, Select, Upload } from "antd";
import { LoadingOutlined, PlusOutlined, MinusCircleOutlined } from '@ant-design/icons';

import TopHeader from "../../../components/TopHeader/TopHeader";
import SideMenu from "../../../components/SideMenu/SideMenu";
import ContainerHeader from "../../../components/ContainerHeader/ContainerHeader";
import InputContainer from "../../../components/InputContainer/InputContainer";

import ChartActions from "../../../redux/actions/charts"
import Themes from "../../../themes/themes";
import { onNavigate } from "../../../util/navigation";
import Router, { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";

import axios from 'axios';
import { API_URL } from '../../../constants';

const { Title } = Typography;

const EditChart = () => {
	const router = useRouter()
	const dispatch = useDispatch()

	const { error, chartDetails, updatingChart, updateChartSuccessful, fetchingChartDetails, fetchChartDetailsSuccessful } = useSelector(state => state.charts)

	const [chartId, setChartId] = useState("");
	const [accessToken, setAccessToken] = useState("");
    const [type, setType] = useState("");
    const [details, setDetails] = useState([]);
    const [isEnabled, setIsEnabled] = useState(false);
    const [imageUrl, setImageUrl] = useState("");
    const [imageId, setImageId] = useState("");
    const [webLoading, setWebLoading] = useState(false);
    const [mobileLoading, setMobileLoading] = useState(false);

	const customRequest = ({
		action,
		data,
		file,
		filename,
		headers,
		onError,
		onProgress,
		onSuccess,
		withCredentials,
	}) => {
		const formData = new FormData();
		if (data) {
			Object.keys(data).forEach((key) => {
				formData.append(key, data[key]);
			});
		}
		formData.append("file", file);
		formData.append("type", "chart");

		onSuccess(formData);

		return {
			abort() {
				// message.error('File Upload Aborted')
			},
		};
	};

	const __func__uploadImage = (data, imageType) => {
		if (imageType == "web") {
			setImageUrl("")
			setWebLoading(true);
		} else if (imageType == "mobile") {
			setMobileImageUrl("")
			setMobileLoading(true);
		}
		axios
			.post(`${API_URL}/admin/thumbnail/upload`, data, {
				"content-type": "multipart/form-data",
				headers: { Authorization: `JWT ${accessToken}` },
			})
			.then((res) => {
				setWebLoading(false);
				setMobileLoading(false);
				if (res.data && res.data.data && res.data.data.imageUrl) {
					if (imageType === "web") {
						setImageUrl(res.data.data.imageUrl);
						setImageId(res.data.data._id);
					} else if (imageType === "mobile") {
						setMobileImageUrl(res.data.data.imageUrl);
						setMobileImageId(res.data.data._id);
					}
				} else {
					message.error("Image Upload Failed. Try Again.")
				}
			}).catch((error) => {
				if (imageType == "web") {
					setWebLoading(false);
				} else if (imageType == "mobile") {
					setMobileLoading(false);
				}
				message.error("Image Upload Failed. Try Again.")
			});
	};

	const __func__setDetails = () => {
		let updatedDetails = [];
		if (details.length) updatedDetails = updatedDetails.concat(details)
		if (type === "shirt") {
			updatedDetails.push({
				size: null,
				length: null,
				chest: null,
				sleeveLength: null
			})
		} else if (type === "pant") {
			updatedDetails.push({
				size: null,
				waist: null,
				totalLength: null,
				inseamLength: null
			})
		}
		setDetails(updatedDetails)
	}

	const __func__updateDetails = (type, field1, field2, field3, field4, index) => {
		let updatedDetails = [];
		if (type === "pant") {
			for (let i in details) {
				var detail = details[i];
				if (i == index) {
					updatedDetails.push({
						size: field1,
						waist: field2,
						totalLength: field3,
						inseamLength: field4
					})
				} else {
					updatedDetails.push({
						size: detail.size,
						waist: detail.waist,
						totalLength: detail.totalLength,
						inseamLength: detail.inseamLength
					})
				}
			}
		} else if (type === "shirt") {
			for (let i in details) {
				var detail = details[i];
				if (i == index) {
					updatedDetails.push({
						size: field1,
						chest: field2,
						length: field3,
						sleeveLength: field4
					})
				} else {
					updatedDetails.push({
						size: detail.size,
						chest: detail.chest,
						length: detail.length,
						sleeveLength: detail.sleeveLength
					})
				}
			}
		}
	
		setDetails(updatedDetails);
	}

	const __func__removeDetails = (type, index) => {
		let updatedDetails = [];
		if (type === "pant") {
			for (let i in details) {
				var detail = details[i];
				if (i != index) {
					updatedDetails.push({
						size: detail.size,
						waist: detail.waist,
						totalLength: detail.totalLength,
						inseamLength: detail.inseamLength
					})
				}
			}
		} else if (type === "shirt") {
			for (let i in details) {
				var detail = details[i];
				if (i != index) {
					updatedDetails.push({
						size: detail.size,
						chest: detail.chest,
						length: detail.length,
						sleeveLength: detail.sleeveLength
					})
				}
			}
		}
	
		setDetails(updatedDetails);
	}

	useEffect(() => {
		if (type === chartDetails.type) setDetails(chartDetails.details)
		else setDetails([])
	}, [type])

	useEffect(() => {
		let token = localStorage.getItem('accessToken');
		setAccessToken(token);
	}, [])

	useEffect(() => {
		dispatch(ChartActions.stateReset({
			fetchingChartDetails: false,
			fetchChartDetailsSuccessful: false,

			updatingChart: false,
			updateChartSuccessful: false,
		}))
	}, [])

	useEffect(() => {
		if (error) {
			message.error(error)
			dispatch(ChartActions.clearError())
		}
	}, [error])

	useEffect(() => {
		let { chartId } = router.query
		if (chartId) {
			setChartId(chartId)
		} else {
			onNavigate(Router, "charts")
		}
	}, [])

	useEffect(() => {
		if (chartId)
			dispatch(ChartActions.getChartDetailsRequest(chartId))
	}, [chartId])

	useEffect(() => {
		if (!fetchingChartDetails && fetchChartDetailsSuccessful) {
			setType(chartDetails.type);
			setImageUrl(chartDetails.imageId.imageUrl);
			setImageId(chartDetails.imageId._id);
			setIsEnabled(chartDetails.isEnabled);
			setDetails(chartDetails.details);

			dispatch(ChartActions.stateReset({
				fetchChartDetailsSuccessful: false
			}))
		}
	}, [fetchChartDetailsSuccessful]);

	useEffect(() => {
		if (!updatingChart && updateChartSuccessful) {
			dispatch(ChartActions.stateReset({
				updateChartSuccessful: false
			}))
			message.success("Chart updated successfully");
			onNavigate(Router, "/charts");
		}
	}, [updateChartSuccessful])


	const handleClickCancel = () => {
		onNavigate(Router, "/charts");
	};
	const handleClickUpdate = () => {
		if (type === "" || details.length === 0) {
			return message.warn("Please fill all the fields")
		}
		dispatch(ChartActions.updateChartRequest(chartId, { type, imageId, isEnabled, details}))
	};


	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"3"} />
			<div className="container">
				<div className="card-root">
					<ContainerHeader title="Edit Chart"
						onClickBackButton={() => Router.back()}
					/>
					<div>
						<Row gutter={24}>
							<Col span={12}>
								<Title level={5} className="title">Type</Title>
								<Select
									placeholder=""
									className="select"
									value={type}
									onChange={(value) => setType(value)}
									style={{ width: '100%', backgroundColor: "#f0f0f0" }}
								>
									<Option value="shirt">Shirt</Option>
									<Option value="pant">Pant</Option>
								</Select>
							</Col>
							<Col span={24}>
								<Title level={5} className="title" style={{ display: "inline-block", marginTop: "5%" }}>Enable</Title>
								{isEnabled
									? <img
										style={{ width: 50, height: 30, marginLeft: 50, cursor: "pointer" }}
										onClick={() => { setIsEnabled(false) }}
										src={require("../../../Images/toggle-success-button.png")} />
									: <img
										style={{ width: 50, height: 30, marginLeft: 50, cursor: "pointer" }}
										onClick={() => { setIsEnabled(true) }}
										src={require("../../../Images/toggle-failure-button.png")} />
								}
							</Col>

							<Col span={24} style={{ marginTop: "3%" }}>
								<Title level={5} className="title">Web Image</Title>
								<Upload
									name="avatar"
									listType="picture-card"
									className="avatar-uploader"
									showUploadList={false}
									onStart={(file) => {
										console.log("onStart", file, file.name);
									}}
									onSuccess={(data) => {
										__func__uploadImage(data, "web");
									}}
									onError={(err) => {
										console.log(err);
										message.error(`${err} File upload failed.`);
									}}
									customRequest={customRequest}
								>
									{imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%', height: "100%" }} /> : (
										<div>
											{webLoading ? <LoadingOutlined /> : <PlusOutlined />}
											<div style={{ marginTop: 8 }}>Upload</div>
										</div>
									)}
								</Upload>
							</Col>
							<Col>
								{type === "shirt" && details.length ?
									details.map((detail, index) => {
										return (
											<Row gutter={24}>
												<Col span={5}>
													<InputContainer
														title="Size"
														value={detail.size}
														onChange={async (e) => {
															await __func__updateDetails("shirt", e.target.value, detail.chest, detail.length, detail.sleeveLength, index)
														}}
													/>
												</Col>
												<Col span={5}>
													<InputContainer
														title="Chest"
														value={detail.chest}
														onChange={async (e) => {
															await __func__updateDetails("shirt", detail.size, e.target.value, detail.length, detail.sleeveLength, index)
														}}
													/>
												</Col>
												<Col span={5}>
													<InputContainer
														title="Length"
														value={detail.length}
														onChange={async (e) => {
															await __func__updateDetails("shirt", detail.size, detail.chest, e.target.value, detail.sleeveLength, index)
														}}
													/>
												</Col>
												<Col span={5}>
													<InputContainer
														title="Sleeve Length"
														value={detail.sleeveLength}
														onChange={async (e) => {
															await __func__updateDetails("shirt", detail.size, detail.chest, detail.length, e.target.value, index)
														}}
													/>
												</Col>
												<Col span={2}></Col>
												<Col span={2} style={{ marginTop: 50 }}>
													<MinusCircleOutlined onClick={() => { __func__removeDetails('shirt', index) }} />
												</Col>
											</Row>
										)
									}) : type === "pant" && details.length ?
										details.map((detail, index) => {
											return (
												<Row gutter={24}>
													<Col span={5}>
														<InputContainer
															title="Size"
															value={detail.size}
															onChange={async (e) => {
																await __func__updateDetails("pant", e.target.value, detail.waist, detail.totalLength, detail.inseamLength, index)
															}}
														/>
													</Col>
													<Col span={5}>
														<InputContainer
															title="Waist"
															value={detail.waist}
															onChange={async (e) => {
																await __func__updateDetails("pant", detail.size, e.target.value, detail.totalLength, detail.inseamLength, index)
															}}
														/>
													</Col>
													<Col span={5}>
														<InputContainer
															title="Total Length"
															value={detail.totalLength}
															onChange={async (e) => {
																await __func__updateDetails("pant", detail.size, detail.waist, e.target.value, detail.inseamLength, index)
															}}
														/>
													</Col>
													<Col span={5}>
														<InputContainer
															title="Inseam Length"
															value={detail.inseamLength}
															onChange={async (e) => {
																await __func__updateDetails("pant", detail.size, detail.waist, detail.totalLength, e.target.value, index)
															}}
														/>
													</Col>
													<Col span={2}></Col>
													<Col span={2} style={{ marginTop: 50 }}>
														<MinusCircleOutlined onClick={() => { __func__removeDetails('pant', index) }} />
													</Col>
												</Row>
											)
										}) : null
								}
							</Col>
						</Row>
						<div className="row">
							<Button className="add-btn" onClick={__func__setDetails}>
								<PlusOutlined />
								Add Field
							</Button>
						</div>
						<div className="row">
							<Button className="cancel-btn" onClick={handleClickCancel}>
								Cancel
							</Button>
							<Button loading={updatingChart} className="create-btn" onClick={handleClickUpdate}>
								Save
							</Button>
						</div>
					</div>
				</div>
			</div>
		</Wrapper>
	);
};

export default EditChart;

const Wrapper = styled.div`
	background-color: ${Themes.backgroundMain};
	height: 100vh;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		display: flex;
		justify-content: center;
		align-items: center;
		.card-root {
			width: 80%;
			background-color: ${Themes.white};
			padding: 40px;
		}
	}
	.add-btn {
		background-color: ${Themes.white};
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-color: #c52bf0;
		border-radius: 5px;
		color: #c52bf0;
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.cancel-btn {
		background-color: ${Themes.cancelButton};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.row {
		display: flex;
		flex-direction: row;
		justify-content: center;
		align-items: center;
	}
	.customInputBox{
		border-radius: 5px;
    	background-color: #f0f0f0;
    	border: none;
		height: 40px;
		width: 90px;
		margin-left:30px;
	}	
	.title1{
		margin-top:20px;
		margin-left:30px
	}
	.hide{
		display:none;
	}
	.title{
        margin-top:20px
    }
`;
