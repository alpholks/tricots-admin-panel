import { useEffect, useState } from "react";
import styled from "styled-components";
import { message, Switch  } from "antd";
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';

import TopHeader from "../../components/TopHeader";
import SideMenu from "../../components/SideMenu";
import ContainerHeader,{FilterOptions} from "../../components/ContainerHeader";
import TableComponent, { UsualActions } from "../../components/Table";
import CenteredModal from "../../components/CenteredModal";

import FeedbackActions from "../../redux/actions/feedbacks"
import Themes from "../../themes/themes";
import Router from "next/router";
import { onNavigate } from "../../util/navigation";
import { useDispatch, useSelector } from "react-redux";

const Feedbacks = () => {
	const dispatch = useDispatch()
	const {error, feedbacks, fetchingFeedbacks, fetchFeedbackSuccessful} = useSelector(state => state.feedbacks)

	useEffect(()=>{
		dispatch(FeedbackActions.stateReset({
			fetchingFeedbacks : false,
			fetchFeedbackSuccessful : false,

		}))
	},[])

	useEffect(()=>{
		if(error){
			message.error(error)
			dispatch(FeedbackActions.clearError())
		}
	},[error])

	useEffect(()=>{
		var queryParams = ""
		dispatch(FeedbackActions.getFeedbackRequest(queryParams))
	},[])

	useEffect(()=> {
		if(!fetchingFeedbacks && fetchFeedbackSuccessful){
			dispatch(FeedbackActions.stateReset({
				fetchFeedbackSuccessful : false
			}))
		}
	},[fetchFeedbackSuccessful])

	const columns = [
		{title: "Feedback", dataIndex: "content", key: "content"},
        {title: "Customer Details", dataIndex: 'customerId', key: 'customerId', 
			render: (text, record) => {
                if (text) {
                    return (<div>{text.firstName + " " + text.lastName + ` (${text.emailId}) `}</div>)
                } else {
                    return (<div> {"Guest User"} </div>)
                }
            }
        },
	];

	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"7"} />
			<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=your_api_key_here"></script>

			<div className="container">
				<div className="card-root">
					<ContainerHeader
						title="Feedbacks"
					/>
					<TableComponent loading={fetchingFeedbacks} data={feedbacks} columns={columns} />
					
				</div>
			</div>
		</Wrapper>
	);
};

export default Feedbacks;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow-y: scroll;
	overflow-x: hidden;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
			width: 100%;
			background-color: #fff;
			padding: 20px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
	}
`;
