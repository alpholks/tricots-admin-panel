import { useEffect, useState } from "react";
import styled from "styled-components";
import { message, Row, Col, Button ,Select ,Typography ,Input, Upload, Divider } from "antd";
const { Option } = Select;
const { Title } = Typography;
import { LoadingOutlined, PlusOutlined, UploadOutlined, MinusCircleOutlined  } from '@ant-design/icons';

import TopHeader from "../../../components/TopHeader";
import SideMenu from "../../../components/SideMenu";
import ContainerHeader from "../../../components/ContainerHeader";
import InputContainer from "../../../components/InputContainer";

import ProductActions from "../../../redux/actions/products"
import Themes from "../../../themes/themes";
import { onNavigate } from "../../../util/navigation";
import Router,{ useRouter } from "next/router";
import { useDispatch , useSelector} from "react-redux";
import Head from "next/head";
import dynamic from "next/dynamic";
const ReactQuill = dynamic(() => import("react-quill"), { ssr: false });

const ViewProduct = () => {
	const router = useRouter()
	const dispatch = useDispatch()

	const {error, productDetails, fetchProductDetailsSuccessful, fetchingProductDetails} = useSelector(state => state.products)


	const [productId, setProductId] = useState("");
	const [collectionName, setCollectionName] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [defaultPrice, setDefaultPrice] = useState("");
	const [specialPrice, setSpecialPrice] = useState("");
	const [isFeatured, setIsFeatured] = useState(false);
	const [isNewArrival, setIsNewArrival] = useState(false);
	const [isWholeSale, setIsWholeSale] = useState(false);
	const [isBestSale, setIsBestSale] = useState(false);
	const [isEnabled, setIsEnabled] = useState(false);
	const [inventories, setInventories] = useState([]);
	const [tags, setTags] = useState([]);
	const [sizes, setSizes] = useState([]);
	const [colors, setColors] = useState([]);
	const [fileList, setFileList] = useState([]);
	const [stock, setStock] = useState();
	const [imageUrl, setImageUrl] = useState("");
	const [imageId, setImageId] = useState("");
	const [webLoading, setWebLoading] = useState(false);
    const [mobileLoading, setMobileLoading] = useState(false);

	useEffect(()=>{
		dispatch(ProductActions.stateReset({
			fetchProductDetailsSuccessful: false,
			fetchingProductDetails: false
		}))
		let {productId} = router.query
		if(productId){
			setProductId(productId)
		}else {
			onNavigate(Router, "products")
		}
	},[])


	useEffect(()=>{
		if(productId){
			console.log(productId)
			dispatch(ProductActions.getProductDetailsRequest(productId))
		}
	},[productId])

	useEffect(()=> {
		if(!fetchingProductDetails && fetchProductDetailsSuccessful){
			dispatch(ProductActions.stateReset({
				fetchProductDetailsSuccessful : false
			}))
			setCollectionName(productDetails.collectionId?.name)
            setName(productDetails.name)
            setDescription(productDetails.description)
            setDefaultPrice(productDetails.defaultPrice)
            setSpecialPrice(productDetails.specialPrice)
            setIsFeatured(productDetails.isFeatured)
            setIsNewArrival(productDetails.isNewArrival)
            setIsWholeSale(productDetails.isWholeSale)
            setIsBestSale(productDetails.isBestSale)
            setIsEnabled(productDetails.isEnabled)
            setTags(productDetails.tags)
            setImageUrl(productDetails.imageId.imageUrl)
            setImageId(productDetails.imageId._id)

			let existingColors = [];
            let existingSizes = [];
            let existingInventories = [];
            for (let index in productDetails.inventories) {
                let inventory = productDetails.inventories[index];
                if (!existingColors.includes(inventory.color + ":" + inventory.colorName)) existingColors.push(inventory.color + ":" + inventory.colorName);
                if (!existingSizes.includes(inventory.size)) existingSizes.push(inventory.size);

                existingInventories.push({
                    size: inventory.size,
                    color: inventory.color + ":" + inventory.colorName,
                    name: inventory.name,
                    imageUrl: inventory?.imageId?.imageUrl,
                    imageId: inventory?.imageId?._id,
                    stock: inventory.stock,
                    isEnabled: inventory.isEnabled
                })
            }

            setColors(existingColors)
            setSizes(existingSizes)
            setInventories(existingInventories);

			let additionalImages = []
			for (let i in productDetails.additionalImages) {
				var singleImage = productDetails.additionalImages[i]
				additionalImages.push({
					status: 'done',
					url: singleImage
				})
			}
			setFileList(additionalImages)
		}
	},[fetchProductDetailsSuccessful])

	return (
		<Wrapper>
			<TopHeader />
			<Head>
				<link
					rel="stylesheet"
					href="//cdn.quilljs.com/1.2.6/quill.snow.css"
				/>
			</Head>
			<SideMenu selectedKey={"10"} />
			<div className="container">
				<div className="card-root">
					<ContainerHeader title={name} 
						onClickBackButton={() => Router.back()}
					/>
					<div>
						<Row gutter={24}>
							<Col span={24}>
									<ReactQuill
									value={description}
									disabled
								/>
                                
							</Col>
                            <Col span={12}>
                                <InputContainer
                                        disabled
                                        title="Collection Name"
                                        value={collectionName}
                                    />
                                    <Title level={5} className="title">Tags</Title>
                                    <Select
                                        mode="multiple"
                                        disabled
										value={tags}
                                        style={{ width: '100%' }}
                                        onChange={false}
                                        >
                                            {tags}
                                    </Select>
                                
							</Col>
                            <Col span={12}>
                                    
                            <InputContainer
                                        disabled
                                        title="Default Price"
                                        value={defaultPrice}
                                    />
									<InputContainer
									disabled
									title="Special Price"
									value={specialPrice}
								/>
							</Col>
							<Col span={12}>
                            <Title level={5} className="title">Image Upload</Title>
                            <Upload
								disabled
                                name="avatar"
                                listType="picture-card"
                                className="avatar-uploader"
                                showUploadList={false}
                                onStart={(file) => {
                                    console.log("onStart", file, file.name);
                                }}
                            >
                                {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%', height: "100%" }} /> : (
                                    <div>
                                        {webLoading ? <LoadingOutlined /> : <PlusOutlined />}
                                        <div style={{ marginTop: 8 }}>Upload</div>
                                  </div>
                                )}
                            </Upload>
                            </Col>
							<Col span={24}>
								<Title level={5} className="title">Additional Images Upload</Title>
								<Upload
									disabled
									listType="picture-card"
									fileList={fileList}
								>
								</Upload>
							</Col>
							<Col span={24} style={{ marginTop: 20 }}>
								<Row gutter={24}>
									<Col span={4}>
										<Title level={5} className="title" style={{display: "inline-block",}}>Best Sale</Title>
											{isBestSale 
												? <img 
													style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer" }} 
													src={require("../../../Images/toggle-success-button.png")}/>
												: <img 
													style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer" }}
													src={require("../../../Images/toggle-failure-button.png")}/>
											}
									
									</Col>

									<Col span={1}>
									
									</Col>
									<Col span={4}>
										<Title level={5} className="title" style={{display: "inline-block",}}>Whole Sale</Title>
										{isWholeSale 
											? <img 
												style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer" }} 
												src={require("../../../Images/toggle-success-button.png")}/>
											: <img 
												style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer" }}
												src={require("../../../Images/toggle-failure-button.png")}/>
										}
									
									</Col>

									<Col span={1}>
									
									</Col>

									<Col span={4}>
										<Title level={5} className="title" style={{display: "inline-block",}}>New Arrival</Title>
										{isNewArrival 
											? <img 
												style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer" }} 
												src={require("../../../Images/toggle-success-button.png")}/>
											: <img 
												style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer" }}
												src={require("../../../Images/toggle-failure-button.png")}/>
										}
									
									</Col>

									<Col span={1}>
									
									</Col>

									<Col span={4}>
									<Title level={5} className="title" style={{display: "inline-block",}}>Feature</Title>
										{isFeatured 
											? <img 
												style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer" }} 
												src={require("../../../Images/toggle-success-button.png")}/>
											: <img 
												style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer" }}
												src={require("../../../Images/toggle-failure-button.png")}/>
										}
									</Col>

									<Col span={1}>
									
									</Col>

									<Col span={4}>
									<Title level={5} className="title" style={{display: "inline-block",}}>Enable</Title>
										{isEnabled 
											? <img 
												style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer" }} 
												src={require("../../../Images/toggle-success-button.png")}/>
											: <img 
												style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer" }}
												src={require("../../../Images/toggle-failure-button.png")}/>
										}
									</Col>
                                </Row>
                            </Col>
							<Col span={24} style={{marginTop: 40}}>
								<Divider plain style={{fontSize: 16, fontWeight: 600}}>Set Your Inventory</Divider>
							</Col>
							<Col span={24}>
								<Row gutter={24}>
									<Col span={12}>
										<Title level={5} className="title">Size</Title>
										<Select disabled defaultValue={sizes}  value={sizes} style={{ width: '100%' }}>
										</Select>
									</Col>
									
									<Col span={12}>
										<Title level={5} className="title">Color</Title>
										<Select disabled defaultValue={colors}  value={colors} mode="multiple" style={{ width: '100%' }} >
										</Select>
									</Col>

								</Row>
							</Col>
							
							<Col span={24} style={{marginTop: 30}}>
								{inventories?.map(inventory => {
									return (
									<Row gutter={24}>
										<Col span={5}>
											<InputContainer
												title="Variant"
												disabled
												value={inventory.size + " - " + inventory.color}
											/>
										</Col>
										<Col span={5}>
											<InputContainer
												title="Name"
                                                disabled
												value={inventory.name}
											/>
										</Col>
										<Col span={5}>
											<InputContainer
												title="Stock"
                                                disabled
												value={inventory.stock}
											/>
										</Col>
										<Col span={5} >
                                        <div style={{marginTop: 50}}><a href={inventory?.imageUrl} target="_blank">{inventory?.imageUrl?.split("/")[inventory?.imageUrl?.split("/").length - 1]}</a></div>
										</Col>
										<Col span={4} style={{marginTop: 20}}>
											<Title level={5} className="title" style={{display: "inline-block",}}>Enabled</Title>
												{inventory.isEnabled 
													? <img 
														style={{width: 35, height: 20, marginLeft: 50,  }} 
														src={require("../../../Images/toggle-success-button.png")}/>
													: <img 
														style={{width: 35, height: 20, marginLeft: 50,  }}
														src={require("../../../Images/toggle-failure-button.png")}/>
												}
											</Col>
									</Row>
									)
								})}
							</Col>
						</Row>
					</div>
				</div>
			</div>
		</Wrapper>
	);
};

export default ViewProduct;

const Wrapper = styled.div`
	background-color: ${Themes.backgroundMain};
	height: 100%;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		display: flex;
		justify-content: center;
		align-items: center;
		height: 90%;
		.card-root {
			width: 80%;
			height: 100%;
			background-color: ${Themes.white};
			padding: 40px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.cancel-btn {
		background-color: ${Themes.cancelButton};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.row {
		display: flex;
		flex-direction: row;
		justify-content: center;
		align-items: center;
	}
`;
