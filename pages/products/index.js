import { useEffect, useState } from "react";
import styled from "styled-components";
import { message, Switch  } from "antd";
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';

import TopHeader from "../../components/TopHeader";
import SideMenu from "../../components/SideMenu";
import ContainerHeader,{FilterOptions} from "../../components/ContainerHeader";
import TableComponent, { UsualActions } from "../../components/Table";
import CenteredModal from "../../components/CenteredModal";

import ProductActions from "../../redux/actions/products"
import Themes from "../../themes/themes";
import Router from "next/router";
import { onNavigate } from "../../util/navigation";
import { useDispatch, useSelector } from "react-redux";

const Products = () => {
	const dispatch = useDispatch()
	const {error, product, products, fetchingProducts, fetchProductSuccessful,deletingProduct,deleteProductSuccessful, updatingProduct, updateProductSuccessful,} = useSelector(state => state.products)

	const [deleteConfirmationModal, setDeleteModalOpen] = useState(false);
	const [deleteId, setDeleteId] = useState("");
	const [searchText, setSearchText] = useState("")
	useEffect(()=>{
		dispatch(ProductActions.stateReset({
			fetchingProducts : false,
			fetchProductSuccessful : false,

			deletingProduct:false,
			deleteProductSuccessful : false,

			updatingProduct: false, 
			updateProductSuccessful: false,
		}))
	},[])

	useEffect(()=>{
		if(error){
			message.error(error)
			dispatch(ProductActions.clearError())
		}
	},[error])

	useEffect(()=>{
		var queryParams = ""
		dispatch(ProductActions.getProductRequest(queryParams))
	},[])

	useEffect(()=> {
		if(!fetchingProducts && fetchProductSuccessful){
			dispatch(ProductActions.stateReset({
				fetchProductSuccessful : false
			}))
		}
	},[fetchProductSuccessful])

	useEffect(()=>{
		if(!updatingProduct && updateProductSuccessful){
			dispatch(ProductActions.stateReset({
				updateProductSuccessful : false
			}))
			message.success("Product updated successfully");
			dispatch(ProductActions.getProductRequest(""))
		}
	},[updateProductSuccessful])

	useEffect(()=> {
		if(!deletingProduct && deleteProductSuccessful){
			dispatch(ProductActions.stateReset({
				deleteProductSuccessful:false
			}))
			setDeleteModalOpen(false);
			message.success("Product Deleted successfully");
			dispatch(ProductActions.getProductRequest())
		}
	},[deleteProductSuccessful])

	useEffect(()=>{
		var queryParams = searchText ? `?search=${searchText}` : "";
		dispatch(ProductActions.getProductRequest(queryParams))
	},[searchText])

	const handleOpenDeleteModal = (_id) => {
		setDeleteId(_id)
		setDeleteModalOpen(true);
	};

	const handleDeleteProduct = () => {
		dispatch(ProductActions.deleteProductRequest(deleteId))
	};

	const handleCancel = () => {
		setDeleteModalOpen(false);
	};

	const handleNavigation = (path) => {
		return onNavigate(Router, path);
	};

	const handleStatus = (productId, name, status) => {
		dispatch(ProductActions.updateProductRequest(productId, {name, isEnabled: status}))	
	};

	const columns = [
		{title: "Web Image", dataIndex: "imageId",key: "imageUrl", 
            render: imageId => (
                <img style={{width: 70, height: 70, borderRadius: 10}} src={imageId?.imageUrl}/>
            ) 
        },
		{title: "Name", dataIndex: "name", key: "name"},
		{title: "Collection Name",key: "collectionName",dataIndex: "collectionId", render: collectionId => (collectionId?.name)},
		{title: "Default Price",key: "defaultPrice",dataIndex: "defaultPrice"},
		{title: "Special Price",key: "specialPrice",dataIndex: "specialPrice"},
		{
			title: "Enable (Public View)",
			render: text => {
				if (text.isEnabled) {
					return (<img style={{ width: 50, height: 30, marginLeft: 50, cursor: "pointer" }} src={require("../../Images/toggle-success-button.png")} onClick={() => handleStatus(text._id, text.name, false)} />)
				} else {
					return (<img style={{ width: 50, height: 30, marginLeft: 50, cursor: "pointer" }} src={require("../../Images/toggle-failure-button.png")} onClick={() => handleStatus(text._id, text.name, true)} />)
				}
			}
		},
		{title: "Action",key: "_id",
			render: (text, record) => (
				<UsualActions 
					onEdit={() => handleNavigation("/products/edit/" + text._id)}
					onView={() => handleNavigation("/products/view/" + text._id)}
					onDelete={false}
				/>
			)
		},
	];

	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"10"} />
			<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=your_api_key_here"></script>

			<div className="container">
				<div className="card-root">
					<ContainerHeader
						title="Products"
						button="Create"
						onClickButton={() => handleNavigation("/products/create")}
					/>
					<FilterOptions 
						search	
						inputValue={searchText}
						placeholder={"Search"}
						onChangeInput={(e) => setSearchText(e.target.value)}
					/>
					<TableComponent loading={fetchingProducts} data={products} columns={columns} />
					<CenteredModal
						visible={deleteConfirmationModal}
						loading={deletingProduct}
						submit={"Delete"}
						cancel="Cancel"
						handleSubmit={handleDeleteProduct}
						handleCancel={handleCancel}
						title="Delete Product"
					>
						<p>Are you sure you want to delete product ?</p>
					</CenteredModal>
				</div>
			</div>
		</Wrapper>
	);
};

export default Products;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow-y: scroll;
	overflow-x: hidden;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
			width: 100%;
			background-color: #fff;
			padding: 20px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
	}
`;

const data = [
	{
		name: "John Brown",
		imageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
        mobileImageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
		isEnabled: true,
		priority: 3,
        type: "Large",
        link: "https://url"
	},
    {
		name: "John Brown",
		imageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
        mobileImageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
		isEnabled: false,
		priority: 5,
        type: "Small",
        link: "https://url"
	}
];
