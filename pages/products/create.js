import { useState, useEffect, useRef } from "react";
import styled from "styled-components";
import {
	message,
	Row,
	Col,
	Button,
	Select,
	Typography,
	Input,
	Upload,
	Divider,
} from "antd";
const { Option } = Select;
import {
	LoadingOutlined,
	PlusOutlined,
	UploadOutlined,
	MinusCircleOutlined,
} from "@ant-design/icons";

import TopHeader from "../../components/TopHeader";
import SideMenu from "../../components/SideMenu";
import ContainerHeader from "../../components/ContainerHeader";
import InputContainer from "../../components/InputContainer";

import ProductActions from "../../redux/actions/products";
import CollectionActions from "../../redux/actions/collections";
import { useDispatch, useSelector } from "react-redux";
import Themes from "../../themes/themes";
import { onNavigate } from "../../util/navigation";
import Router from "next/router";
import axios from "axios";
import { API_URL } from "../../constants";
// import ReactQuill from "react-quill"; // ES6

// import { Editor, EditorState } from "draft-js";
import SunEditor from "suneditor-react";
import Head from "next/head";
import dynamic from "next/dynamic";
const ReactQuill = dynamic(() => import("react-quill"), { ssr: false });

const { Title } = Typography;

const CreateProduct = () => {
	const dispatch = useDispatch();
	const { error, creatingProduct, createProductSuccessful } = useSelector(
		(state) => state.products
	);
	const {
		error: collectionError,
		collections,
		fetchingCollections,
		fetchCollectionSuccessful,
	} = useSelector((state) => state.collections);

	const [accessToken, setAccessToken] = useState("");
	const [collectionId, setCollectionId] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	// 	EditorState.createEmpty()
	// );

	// const editor = useRef(null);
	// function focusEditor() {
	// 	editor.current.focus();
	// }
	const [defaultPrice, setDefaultPrice] = useState("");
	const [specialPrice, setSpecialPrice] = useState("");
	const [isFeatured, setIsFeatured] = useState(false);
	const [isNewArrival, setIsNewArrival] = useState(false);
	const [isWholeSale, setIsWholeSale] = useState(false);
	const [isBestSale, setIsBestSale] = useState(false);
	const [isEnabled, setIsEnabled] = useState(false);
	const [isSetInventory, setIsSetInventory] = useState(false);
	const [inventories, setInventories] = useState([]);
	const [tags, setTags] = useState([]);
	const [defaultSizes, setDefaultSizes] = useState([
		"XS",
		"S",
		"M",
		"L",
		"XL",
		"XXL",
	]);
	const [sizes, setSizes] = useState([]);
	const [colors, setColors] = useState([]);
	const [fileList, setFileList] = useState([]);
	const [stock, setStock] = useState();
	const [imageUrl, setImageUrl] = useState("");
	const [imageId, setImageId] = useState("");
	const [mobileImageUrl, setMobileImageUrl] = useState("");
	const [mobileImageId, setMobileImageId] = useState("");
	const [webLoading, setWebLoading] = useState(false);
	const [mobileLoading, setMobileLoading] = useState(false);

	const customRequest = ({
		action,
		data,
		file,
		filename,
		headers,
		onError,
		onProgress,
		onSuccess,
		withCredentials,
	}) => {
		const formData = new FormData();
		if (data) {
			Object.keys(data).forEach((key) => {
				formData.append(key, data[key]);
			});
		}
		formData.append("file", file);
		formData.append("type", "product");

		onSuccess(formData);

		return {
			abort() {
				// message.error('File Upload Aborted')
			},
		};
	};

	const __func__uploadImage = (data, imageType, isMultiple = false) => {
		if (isMultiple === false) {
			if (imageType == "web") {
				setWebLoading(true);
			} else if (imageType == "mobile") {
				setMobileLoading(true);
			}
		}
		axios
			.post(`${API_URL}/admin/thumbnail/upload`, data, {
				"content-type": "multipart/form-data",
				headers: { Authorization: `JWT ${accessToken}` },
			})
			.then((res) => {
				setWebLoading(false);
				setMobileLoading(false);
				if (res.data && res.data.data && res.data.data.imageUrl) {
					if (isMultiple === false) {
						if (imageType === "web") {
							setImageUrl(res.data.data.imageUrl);
							setImageId(res.data.data._id);
						} else if (imageType === "mobile") {
							setMobileImageUrl(res.data.data.imageUrl);
							setMobileImageId(res.data.data._id);
						}
					} else {
						let additionalImages = [];
						for (let i in fileList) {
							var singleFile = fileList[i];
							additionalImages.push({
								imageId: singleFile.imageId,
								url: singleFile.url,
								status: singleFile.status,
							});
						}
						additionalImages.push({
							imageId: res.data.data._id,
							url: res.data.data.imageUrl,
							status: "done",
						});

						setFileList(additionalImages);
					}
				} else {
					message.error("Image Upload Failed. Try Again.");
				}
			})
			.catch((error) => {
				if (imageType == "web") {
					setWebLoading(false);
				} else if (imageType == "mobile") {
					setMobileLoading(false);
				}
				message.error("Image Upload Failed. Try Again.");
			});
	};

	const __func__uploadInventoryImage = (data, imageType, size, color) => {
		if (imageType == "web") {
			setWebLoading(true);
		} else if (imageType == "mobile") {
			setMobileLoading(true);
		}
		axios
			.post(`${API_URL}/admin/thumbnail/upload`, data, {
				"content-type": "multipart/form-data",
				headers: { Authorization: `JWT ${accessToken}` },
			})
			.then((res) => {
				setWebLoading(false);
				setMobileLoading(false);
				if (res.data && res.data.data && res.data.data.imageUrl) {
					if (imageType === "web") {
						__func__updateImage(
							size,
							color,
							res.data.data.imageUrl,
							res.data.data._id
						);
					}
				} else {
					message.error("Image Upload Failed. Try Again.");
				}
			})
			.catch((error) => {
				if (imageType == "web") {
					setWebLoading(false);
				} else if (imageType == "mobile") {
					setMobileLoading(false);
				}
				message.error("Image Upload Failed. Try Again.");
			});
	};

	useEffect(() => {
		let token = localStorage.getItem("accessToken");
		setAccessToken(token);
	}, []);

	useEffect(() => {
		dispatch(
			ProductActions.stateReset({
				creatingProduct: false,
				createProductSuccessful: false,
			})
		);
	}, []);

	useEffect(() => {
		var queryParams = "";
		dispatch(CollectionActions.getCollectionRequest(queryParams));
	}, []);

	useEffect(() => {
		if (!fetchingCollections && fetchCollectionSuccessful) {
			dispatch(
				CollectionActions.stateReset({
					fetchCollectionSuccessful: false,
				})
			);
		}
	}, [fetchCollectionSuccessful]);

	useEffect(() => {
		dispatch(ProductActions.clearError());
	}, []);

	useEffect(() => {
		if (error) message.error(error);
		dispatch(
			CollectionActions.stateReset({
				fetchingCollections: false,
			})
		);
		dispatch(
			ProductActions.stateReset({
				creatingProduct: false,
			})
		);
		dispatch(ProductActions.clearError());
	}, [error]);

	useEffect(() => {
		if (!creatingProduct && createProductSuccessful) {
			dispatch(
				ProductActions.stateReset({
					createProductSuccessful: false,
				})
			);
			message.success("Product created successfully");
			onNavigate(Router, "/products");
		}
	}, [createProductSuccessful]);

	useEffect(() => {
		__func__setInventory();
	}, [sizes, colors]);

	const handleClickCancel = () => {
		onNavigate(Router, "/products");
	};
	const handleClickCreate = () => {
		if (
			name === "" ||
			defaultPrice === "" ||
			specialPrice === "" ||
			collectionId === ""
		) {
			return message.warn("Please fill all the fields");
		}
		let additionalImages = [];
		for (let i in fileList) {
			var singleFile = fileList[i];
			additionalImages.push(singleFile.url);
		}
		dispatch(
			ProductActions.createProductRequest({
				collectionId,
				name,
				description,
				defaultPrice,
				specialPrice,
				isFeatured,
				isNewArrival,
				isWholeSale,
				isBestSale,
				isEnabled,
				tags,
				imageId,
				inventories,
				additionalImages,
			})
		);
	};

	const handleClickSetInventory = () => {
		setIsSetInventory(true);
	};

	const __func__setInventory = () => {
		var newInventory = [];
		for (let i in sizes) {
			var size = sizes[i];
			for (let j in colors) {
				var color = colors[j];
				var oldInventory = inventories.find(
					(i) => i.size === size && i.color === color
				);
				if (oldInventory) {
					newInventory.push({
						size: oldInventory.size,
						color: oldInventory.color,
						name: oldInventory.name,
						imageUrl: oldInventory.imageUrl,
						imageId: oldInventory.imageId,
						stock: oldInventory.stock,
						isEnabled: oldInventory.isEnabled,
					});
				} else {
					newInventory.push({
						size: size,
						color: color,
						name: null,
						imageUrl: null,
						imageId: null,
						stock: null,
						isEnabled: false,
					});
				}
			}
		}

		newInventory = newInventory.sort((a, b) =>
			a.color > b.color ? 1 : a.color < b.color ? -1 : 0
		);
		setInventories(newInventory);
	};

	const __func__updateName = async (size, color, inventoryName) => {
		var newInventory = [];
		await Promise.all(
			inventories.map((i) => {
				if (size == i.size && color == i.color) {
					newInventory.push({
						size: i.size,
						color: i.color,
						name: inventoryName,
						stock: i.stock,
						imageUrl: i.imageUrl,
						imageId: i.imageId,
						isEnabled: i.isEnabled,
					});
				} else {
					newInventory.push({
						size: i.size,
						color: i.color,
						name: i.name,
						stock: i.stock,
						imageUrl: i.imageUrl,
						imageId: i.imageId,
						isEnabled: i.isEnabled,
					});
				}
			})
		);
		newInventory = newInventory.sort((a, b) =>
			a.color > b.color ? 1 : a.color < b.color ? -1 : 0
		);
		setInventories(newInventory);
		console.log(newInventory, "????");
	};

	const __func__updateStock = async (size, color, inventoryStock) => {
		var newInventory = [];
		await Promise.all(
			inventories.map((i) => {
				if (size == i.size && color == i.color) {
					newInventory.push({
						size: i.size,
						color: i.color,
						name: i.name,
						stock: inventoryStock,
						imageUrl: i.imageUrl,
						imageId: i.imageId,
						isEnabled: i.isEnabled,
					});
				} else {
					newInventory.push({
						size: i.size,
						color: i.color,
						stock: i.stock,
						name: i.name,
						imageUrl: i.imageUrl,
						imageId: i.imageId,
						isEnabled: i.isEnabled,
					});
				}
			})
		);
		newInventory = newInventory.sort((a, b) =>
			a.color > b.color ? 1 : a.color < b.color ? -1 : 0
		);
		setInventories(newInventory);
		console.log(newInventory, "????");
	};

	const __func__updateImage = async (
		size,
		color,
		inventoryImageUrl,
		inventoryImageId
	) => {
		var newInventory = [];
		await Promise.all(
			inventories.map((i) => {
				if (size == i.size && color == i.color) {
					newInventory.push({
						size: i.size,
						color: i.color,
						name: i.name,
						stock: i.stock,
						imageUrl: inventoryImageUrl,
						imageId: inventoryImageId,
						isEnabled: i.isEnabled,
					});
				} else {
					newInventory.push({
						size: i.size,
						color: i.color,
						name: i.name,
						stock: i.stock,
						imageUrl: i.imageUrl,
						imageId: i.imageId,
						isEnabled: i.isEnabled,
					});
				}
			})
		);
		newInventory = newInventory.sort((a, b) =>
			a.color > b.color ? 1 : a.color < b.color ? -1 : 0
		);
		setInventories(newInventory);
	};

	const __func__updateIsEnabled = async (size, color, isInventoryEnabled) => {
		var newInventory = [];
		await Promise.all(
			inventories.map((i) => {
				if (size == i.size && color == i.color) {
					newInventory.push({
						size: i.size,
						color: i.color,
						name: i.name,
						stock: i.stock,
						imageUrl: i.imageUrl,
						imageId: i.imageId,
						isEnabled: isInventoryEnabled,
					});
				} else {
					newInventory.push({
						size: i.size,
						color: i.color,
						name: i.name,
						stock: i.stock,
						imageUrl: i.imageUrl,
						imageId: i.imageId,
						isEnabled: i.isEnabled,
					});
				}
			})
		);
		newInventory = newInventory.sort((a, b) =>
			a.color > b.color ? 1 : a.color < b.color ? -1 : 0
		);
		setInventories(newInventory);
	};

	const __func__removeInventory = async (size, color) => {
		var newInventory = [];
		for (let i in inventories) {
			var inventory = inventories[i];
			if (inventory.size == size && inventory.color == color) {
				continue;
			}
			newInventory.push({
				size: inventory.size,
				color: inventory.color,
				name: inventory.name,
				stock: inventory.stock,
				imageUrl: inventory.imageUrl,
				imageId: inventory.imageId,
				isEnabled: inventory.isEnabled,
			});
		}

		newInventory = newInventory.sort((a, b) =>
			a.color > b.color ? 1 : a.color < b.color ? -1 : 0
		);
		setInventories(newInventory);
	};

	const __func__updateSize = async (value) => {
		setSizes(value);
	};

	const __func__updateColor = async (value) => {
		setColors(value);
	};

	const handleRemoveImage = (item) => {
		let additionalImages = [];
		for (let i in fileList) {
			var singleFile = fileList[i];
			if (item.imageId != singleFile.imageId) {
				additionalImages.push({
					imageId: singleFile.imageId,
					url: singleFile.url,
					status: singleFile.status,
				});
			}
		}

		setFileList(additionalImages);
	};

	return (
		<Wrapper>
			<TopHeader />
			<Head>
				<link
					rel="stylesheet"
					href="//cdn.quilljs.com/1.2.6/quill.snow.css"
				/>
			</Head>
			<SideMenu selectedKey={"10"} />
			<div className="container">
				<div className="card-root">
					<ContainerHeader
						title="Create Product"
						onClickBackButton={() => Router.back()}
					/>
					<div>
						<Row gutter={24}>
							<Col span={12}>
								<InputContainer
									title="Name"
									value={name}
									onChange={(e) => setName(e.target.value)}
								/>

								<InputContainer
									title="Default Price"
									value={defaultPrice}
									onChange={(e) =>
										setDefaultPrice(e.target.value)
									}
								/>
							</Col>
							<Col span={12}>
								{/* <div
									style={{
										border: "1px solid black",
										minHeight: "6em",
										cursor: "text",
									}}
									onClick={focusEditor}
								>
									<Editor
										ref={editor}
										editorState={description}
										onChange={setDescription}
										placeholder="Write something!"
									/>
								</div> */}
								{/* <SunEditor /> */}
								<ReactQuill
									value={description}
									onChange={setDescription}
								/>
								{/* <InputContainer
									title="Description"
									value={description}
									onChange={(e) => setDescription(e.target.value)}
								/> */}
								<InputContainer
									title="Special Price"
									value={specialPrice}
									onChange={(e) =>
										setSpecialPrice(e.target.value)
									}
								/>
							</Col>
							<Col span={24}>
								<Row gutter={24}>
									<Col span={12}>
										<Title level={5} className="title">
											Collection
										</Title>
										<Select
											placeholder="Select Collection"
											style={{ width: 180 }}
											value={collectionId}
											onChange={(value) =>
												setCollectionId(value)
											}
										>
											{collections.map((item) => {
												return (
													<Option value={item._id}>
														{item.name}
													</Option>
												);
											})}
										</Select>
									</Col>
									<Col span={12}>
										<Title level={5} className="title">
											Tags
										</Title>
										<Select
											mode="tags"
											style={{ width: "100%" }}
											placeholder="Enter the tags"
											onChange={(value) => {
												setTags(value);
											}}
										></Select>
									</Col>
								</Row>
							</Col>
							<Col span={12}>
								<Title level={5} className="title">
									Image Upload
								</Title>
								<Upload
									name="avatar"
									listType="picture-card"
									className="avatar-uploader"
									showUploadList={false}
									onStart={(file) => {
										console.log("onStart", file, file.name);
									}}
									onSuccess={(data) => {
										__func__uploadImage(data, "web");
									}}
									onError={(err) => {
										console.log(err);
										message.error(
											`${err} File upload failed.`
										);
									}}
									customRequest={customRequest}
								>
									{imageUrl ? (
										<img
											src={imageUrl}
											alt="avatar"
											style={{
												width: "100%",
												height: "100%",
											}}
										/>
									) : (
										<div>
											{webLoading ? (
												<LoadingOutlined />
											) : (
												<PlusOutlined />
											)}
											<div style={{ marginTop: 8 }}>
												Upload
											</div>
										</div>
									)}
								</Upload>
							</Col>
							<Col span={24}>
								<Title level={5} className="title">
									Additional Images Upload
								</Title>
								<Upload
									listType="picture-card"
									onStart={(file) => {
										console.log("onStart", file, file.name);
									}}
									onSuccess={(data) => {
										__func__uploadImage(data, "web", true);
									}}
									onError={(err) => {
										console.log(err);
										message.error(
											`${err} File upload failed.`
										);
									}}
									customRequest={customRequest}
									fileList={fileList}
									// onPreview={handlePreview}
									onRemove={handleRemoveImage}
								>
									{fileList.length > 100 ? null : (
										<div>
											<PlusOutlined />
											<div style={{ marginTop: 8 }}>
												Upload
											</div>
										</div>
									)}
								</Upload>
							</Col>
							<Col span={24} style={{ marginTop: 20 }}>
								<Row gutter={24}>
									<Col span={4}>
										<Title
											level={5}
											className="title"
											style={{ display: "inline-block" }}
										>
											Best Sale
										</Title>
										{isBestSale ? (
											<img
												style={{
													width: 50,
													height: 30,
													marginLeft: 50,
													cursor: "pointer",
												}}
												onClick={() => {
													setIsBestSale(false);
												}}
												src={require("../../Images/toggle-success-button.png")}
											/>
										) : (
											<img
												style={{
													width: 50,
													height: 30,
													marginLeft: 50,
													cursor: "pointer",
												}}
												onClick={() => {
													setIsBestSale(true);
												}}
												src={require("../../Images/toggle-failure-button.png")}
											/>
										)}
									</Col>

									<Col span={1}></Col>
									<Col span={4}>
										<Title
											level={5}
											className="title"
											style={{ display: "inline-block" }}
										>
											Whole Sale
										</Title>
										{isWholeSale ? (
											<img
												style={{
													width: 50,
													height: 30,
													marginLeft: 50,
													cursor: "pointer",
												}}
												onClick={() => {
													setIsWholeSale(false);
												}}
												src={require("../../Images/toggle-success-button.png")}
											/>
										) : (
											<img
												style={{
													width: 50,
													height: 30,
													marginLeft: 50,
													cursor: "pointer",
												}}
												onClick={() => {
													setIsWholeSale(true);
												}}
												src={require("../../Images/toggle-failure-button.png")}
											/>
										)}
									</Col>

									<Col span={1}></Col>

									<Col span={4}>
										<Title
											level={5}
											className="title"
											style={{ display: "inline-block" }}
										>
											New Arrival
										</Title>
										{isNewArrival ? (
											<img
												style={{
													width: 50,
													height: 30,
													marginLeft: 50,
													cursor: "pointer",
												}}
												onClick={() => {
													setIsNewArrival(false);
												}}
												src={require("../../Images/toggle-success-button.png")}
											/>
										) : (
											<img
												style={{
													width: 50,
													height: 30,
													marginLeft: 50,
													cursor: "pointer",
												}}
												onClick={() => {
													setIsNewArrival(true);
												}}
												src={require("../../Images/toggle-failure-button.png")}
											/>
										)}
									</Col>

									<Col span={1}></Col>

									<Col span={4}>
										<Title
											level={5}
											className="title"
											style={{ display: "inline-block" }}
										>
											Feature
										</Title>
										{isFeatured ? (
											<img
												style={{
													width: 50,
													height: 30,
													marginLeft: 50,
													cursor: "pointer",
												}}
												onClick={() => {
													setIsFeatured(false);
												}}
												src={require("../../Images/toggle-success-button.png")}
											/>
										) : (
											<img
												style={{
													width: 50,
													height: 30,
													marginLeft: 50,
													cursor: "pointer",
												}}
												onClick={() => {
													setIsFeatured(true);
												}}
												src={require("../../Images/toggle-failure-button.png")}
											/>
										)}
									</Col>

									<Col span={1}></Col>

									<Col span={4}>
										<Title
											level={5}
											className="title"
											style={{ display: "inline-block" }}
										>
											Enable
										</Title>
										{isEnabled ? (
											<img
												style={{
													width: 50,
													height: 30,
													marginLeft: 50,
													cursor: "pointer",
												}}
												onClick={() => {
													setIsEnabled(false);
												}}
												src={require("../../Images/toggle-success-button.png")}
											/>
										) : (
											<img
												style={{
													width: 50,
													height: 30,
													marginLeft: 50,
													cursor: "pointer",
												}}
												onClick={() => {
													setIsEnabled(true);
												}}
												src={require("../../Images/toggle-failure-button.png")}
											/>
										)}
									</Col>
								</Row>
							</Col>

							{isSetInventory === false ? (
								<Col span={24} style={{ marginTop: 30 }}>
									<div className="row">
										<Button
											className="create-btn"
											onClick={handleClickSetInventory}
										>
											Set Inventory
										</Button>
									</div>
								</Col>
							) : null}

							{isSetInventory === true ? (
								<Row gutter={24} style={{ width: "100%" }}>
									<Col span={24} style={{ marginTop: 40 }}>
										<Divider
											plain
											style={{
												fontSize: 16,
												fontWeight: 600,
											}}
										>
											Set Your Inventory
										</Divider>
									</Col>
									<Col span={24}>
										<Row gutter={24}>
											<Col span={12}>
												<Title
													level={5}
													className="title"
												>
													Size
												</Title>
												<Select
													mode="multiple"
													style={{ width: "100%" }}
													placeholder="Enter the sizes"
													onChange={async (value) => {
														__func__updateSize(
															value
														);
													}}
												>
													{defaultSizes.map(
														(item) => {
															return (
																<Option
																	value={item}
																>
																	{item}
																</Option>
															);
														}
													)}
												</Select>
											</Col>

											<Col span={12}>
												<Title
													level={5}
													className="title"
												>
													Color
												</Title>
												<Select
													mode="tags"
													style={{ width: "100%" }}
													placeholder="Enter the colors"
													onChange={async (value) => {
														__func__updateColor(
															value
														);
													}}
												></Select>
											</Col>
										</Row>
									</Col>

									<Col span={24} style={{ marginTop: 30 }}>
										{inventories.map((inventory) => {
											return (
												<Row gutter={24}>
													<Col span={5}>
														<InputContainer
															title="Variant"
															disabled
															value={
																inventory.size +
																" - " +
																inventory.color
															}
														/>
													</Col>
													<Col span={5}>
														<InputContainer
															title="Name"
															value={
																inventory.name
															}
															onChange={async (
																e
															) => {
																await __func__updateName(
																	inventory.size,
																	inventory.color,
																	e.target
																		.value
																);
															}}
														/>
													</Col>
													<Col span={5}>
														<InputContainer
															title="Stock"
															value={
																inventory.stock
															}
															onChange={async (
																e
															) => {
																await __func__updateStock(
																	inventory.size,
																	inventory.color,
																	e.target
																		.value
																);
															}}
														/>
													</Col>
													<Col span={5}>
														{inventory.imageUrl ? (
															<div
																style={{
																	marginTop: 50,
																}}
															>
																<a
																	href={
																		inventory.imageUrl
																	}
																	target="_blank"
																>
																	{inventory.imageUrl
																		? inventory.imageUrl.split(
																				"/"
																		  )[
																				inventory.imageUrl.split(
																					"/"
																				)
																					.length -
																					1
																		  ]
																		: null}
																</a>
															</div>
														) : (
															<div>
																<Title
																	level={5}
																	className="title"
																>
																	Image Upload
																</Title>
																<Upload
																	showUploadList={
																		false
																	}
																	onStart={(
																		file
																	) => {
																		console.log(
																			"onStart",
																			file,
																			file.name
																		);
																	}}
																	onSuccess={(
																		data
																	) => {
																		__func__uploadInventoryImage(
																			data,
																			"web",
																			inventory.size,
																			inventory.color
																		);
																	}}
																	onError={(
																		err
																	) => {
																		console.log(
																			err
																		);
																		message.error(
																			`${err} File upload failed.`
																		);
																	}}
																	customRequest={
																		customRequest
																	}
																>
																	<Button
																		icon={
																			<UploadOutlined />
																		}
																	>
																		Click to
																		Upload
																	</Button>
																</Upload>
															</div>
														)}
													</Col>
													<Col span={2}>
														<Title
															level={5}
															className="title"
															style={{
																display:
																	"inline-block",
															}}
														>
															Enabled
														</Title>
														{inventory.isEnabled ? (
															<img
																style={{
																	width: 35,
																	height: 20,
																	cursor:
																		"pointer",
																}}
																onClick={async () => {
																	await __func__updateIsEnabled(
																		inventory.size,
																		inventory.color,
																		false
																	);
																}}
																src={require("../../Images/toggle-success-button.png")}
															/>
														) : (
															<img
																style={{
																	width: 35,
																	height: 20,
																	cursor:
																		"pointer",
																}}
																onClick={async () => {
																	await __func__updateIsEnabled(
																		inventory.size,
																		inventory.color,
																		true
																	);
																}}
																src={require("../../Images/toggle-failure-button.png")}
															/>
														)}
													</Col>
													<Col span={1}></Col>
													<Col
														span={1}
														style={{
															marginTop: 50,
														}}
													>
														<MinusCircleOutlined
															onClick={() => {
																__func__removeInventory(
																	inventory.size,
																	inventory.color
																);
															}}
														/>
													</Col>
												</Row>
											);
										})}
									</Col>
								</Row>
							) : null}

							<Col></Col>
						</Row>
						{isSetInventory == true ? (
							<div className="row" style={{ marginTop: 40 }}>
								<Button
									className="cancel-btn"
									onClick={handleClickCancel}
								>
									Cancel
								</Button>
								<Button
									loading={creatingProduct}
									className="create-btn"
									onClick={handleClickCreate}
								>
									Create
								</Button>
							</div>
						) : null}
					</div>
				</div>
			</div>
		</Wrapper>
	);
};

export default CreateProduct;

const Wrapper = styled.div`
	background-color: ${Themes.backgroundMain};
	height: 100vh;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		display: flex;
		justify-content: center;
		align-items: center;
		.card-root {
			width: 80%;
			background-color: ${Themes.white};
			padding: 40px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.cancel-btn {
		background-color: ${Themes.cancelButton};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.create-inventory-btn {
		background-color: ${Themes.primary};
		border: none;
		margin-top: 50px;
		width: 150px;
		height: 30px;
		font-weight: bold;
		font-size: 12px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.customInputBox {
		border-radius: 5px;
		background-color: #f0f0f0;
		border: none;
		height: 40px;
		width: 90px;
		margin-left: 30px;
	}
	.title1 {
		margin-top: 20px;
		margin-left: 30px;
	}
	.row {
		display: flex;
		flex-direction: row;
		justify-content: center;
		align-items: center;
	}
	.title {
		margin-top: 20px;
	}
	.hide {
		display: none;
	}
`;

const categories = [];
