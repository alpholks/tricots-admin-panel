import { useEffect, useState } from "react";
import styled from "styled-components";
import { message } from "antd";

import TopHeader from "../../components/TopHeader";
import SideMenu from "../../components/SideMenu";
import ContainerHeader,{FilterOptions} from "../../components/ContainerHeader";
import TableComponent, { UsualActions } from "../../components/Table";
import CenteredModal from "../../components/CenteredModal";

import CustomerActions from "../../redux/actions/customers"
import Themes from "../../themes/themes";
import Router from "next/router";
import { onNavigate } from "../../util/navigation";
import { useDispatch, useSelector } from "react-redux";

const Customers = () => {
	const dispatch = useDispatch()
	const {error, customers, fetchingCustomers, fetchCustomerSuccessful,deletingCustomer,deleteCustomerSuccessful} = useSelector(state => state.customers)

	const [deleteConfirmationModal, setDeleteModalOpen] = useState(false);
	const [deleteId, setDeleteId] = useState("");
	const [searchText, setSearchText] = useState("")
	useEffect(()=>{
		dispatch(CustomerActions.stateReset({
			fetchingCustomers : false,
			fetchCustomerSuccessful : false,

			deletingCustomer:false,
			deleteCustomerSuccessful : false
		}))
	},[])

	useEffect(()=>{
		dispatch(CustomerActions.clearError())
	},[])

	useEffect(()=>{
		if(error){
			message.error(error)
			dispatch(CustomerActions.clearError())
		}
	},[error])

	useEffect(()=>{
		var queryParams = ""
		dispatch(CustomerActions.getCustomerRequest(queryParams))
	},[])

	useEffect(()=> {
		if(!fetchingCustomers && fetchCustomerSuccessful){
			dispatch(CustomerActions.stateReset({
				fetchCustomerSuccessful : false
			}))
		}
	},[fetchCustomerSuccessful])

	useEffect(()=> {
		if(!deletingCustomer && deleteCustomerSuccessful){
			dispatch(CustomerActions.stateReset({
				deleteCustomerSuccessful:false
			}))
			setDeleteModalOpen(false);
			message.success("Customer Deleted successfully");
			dispatch(CustomerActions.getCustomerRequest())
		}
	},[deleteCustomerSuccessful])

	useEffect(()=>{
		var queryParams = searchText ? `?search=${searchText}` : "";
		dispatch(CustomerActions.getCustomerRequest(queryParams))
	},[searchText])

	const handleOpenDeleteModal = (_id) => {
		setDeleteId(_id)
		setDeleteModalOpen(true);
	};

	const handleDeleteCustomer = () => {
		dispatch(CustomerActions.deleteCustomerRequest(deleteId))
	};

	const handleCancel = () => {
		setDeleteModalOpen(false);
	};

	const handleNavigation = (path) => {
		return onNavigate(Router, path);
	};

	const columns = [
		{title: "Name", 
			render: (text, record) => <div>{text.firstName + " " + text.lastName}</div>},
		{title: "Email",dataIndex: "emailId",key: "emailId"},
		{title: "Orders",dataIndex: "ordersCount",key: "ordersCount"},
		{title: "Action",key: "_id",
			render: (text, record) => (
				<UsualActions 
					onEdit={false}
					onView={() => handleNavigation("/customers/view/" + text._id)}
					onDelete={false}
				/>
			)
		},
	];

	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"6"} />
			<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=your_api_key_here"></script>

			<div className="container">
				<div className="card-root">
					{/* <ContainerHeader
						title="Customers"
						button="Create"
						onClickButton={() => handleNavigation("/customers/create")}
					/> */}
					<FilterOptions 
						search	
						inputValue={searchText}
						placeholder={"Search"}
						onChangeInput={(e) => setSearchText(e.target.value)}
					/>
					<TableComponent loading={fetchingCustomers} data={customers} columns={columns} />
					<CenteredModal
						visible={deleteConfirmationModal}
						loading={deletingCustomer}
						submit={"Delete"}
						cancel="Cancel"
						handleSubmit={handleDeleteCustomer}
						handleCancel={handleCancel}
						title="Delete Customer"
					>
						<p>Are you sure you want to delete customer ?</p>
					</CenteredModal>
				</div>
			</div>
		</Wrapper>
	);
};

export default Customers;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow-y: scroll;
	overflow-x: hidden;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
			width: 100%;
			background-color: #fff;
			padding: 20px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
	}
`;

const data = [
	{
		key: "1",
		name: "John Brown",
		email: "johnbrown@gmail.com",
		mobile: "+91 987654321",
		locations: 23,
	},
	{
		key: "2",
		name: "Jim Green",
		email: "johnbrown@gmail.com",
		mobile: "+91 987654321",
		locations: 22,
	},
	{
		key: "3",
		name: "Joe Black",
		email: "johnbrown@gmail.com",
		mobile: "+91 987654321",
		locations: 24,
	},
	{
		key: "2",
		name: "Jim Green",
		email: "johnbrown@gmail.com",
		mobile: "+91 987654321",
		locations: 22,
	},
	{
		key: "3",
		name: "Joe Black",
		email: "johnbrown@gmail.com",
		mobile: "+91 987654321",
		locations: 24,
	},
	{
		key: "1",
		name: "John Brown",
		email: "johnbrown@gmail.com",
		mobile: "+91 987654321",
		locations: 23,
	},
	{
		key: "2",
		name: "Jim Green",
		email: "johnbrown@gmail.com",
		mobile: "+91 987654321",
		locations: 22,
	},
	{
		key: "3",
		name: "Joe Black",
		email: "johnbrown@gmail.com",
		mobile: "+91 987654321",
		locations: 24,
	},
	{
		key: "2",
		name: "Jim Green",
		email: "johnbrown@gmail.com",
		mobile: "+91 987654321",
		locations: 22,
	},
	{
		key: "3",
		name: "Joe Black",
		email: "johnbrown@gmail.com",
		mobile: "+91 987654321",
		locations: 24,
	},
];
