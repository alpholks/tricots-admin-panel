import { useEffect, useState } from "react";
import styled from "styled-components";
import { message, Row, Col, Button, Select, Typography, Input, Upload, Card, Divider } from "antd";
const { Option } = Select;
const { Title } = Typography;
import { LoadingOutlined, PlusOutlined, UploadOutlined, MinusCircleOutlined } from '@ant-design/icons';

import TopHeader from "../../../components/TopHeader";
import SideMenu from "../../../components/SideMenu";
import ContainerHeader from "../../../components/ContainerHeader";
import InputContainer from "../../../components/InputContainer";

import CustomerActions from "../../../redux/actions/customers"
import Themes from "../../../themes/themes";
import { onNavigate } from "../../../util/navigation";
import Router, { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";

import moment from 'moment';

const ViewCustomer = () => {
	const router = useRouter()
	const dispatch = useDispatch()

	const { error, customerDetails, fetchCustomerDetailsSuccessful, fetchingCustomerDetails } = useSelector(state => state.customers)


	const [customerId, setCustomerId] = useState("");
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [emailId, setEmailId] = useState("");
	const [createdAt, setCreatedAt] = useState("");
	const [addresses, setAddresses] = useState([]);

	useEffect(() => {
		dispatch(CustomerActions.stateReset({
			fetchCustomerDetailsSuccessful: false,
			fetchingCustomerDetails: false
		}))
		let { customerId } = router.query
		if (customerId) {
			setCustomerId(customerId)
		} else {
			onNavigate(Router, "customers")
		}
	}, [])


	useEffect(() => {
		if (customerId) {
			dispatch(CustomerActions.getCustomerRequest())
			console.log(customerId)
			dispatch(CustomerActions.getCustomerDetailsRequest(customerId))
		}
	}, [customerId])

	useEffect(() => {
		if (!fetchingCustomerDetails && fetchCustomerDetailsSuccessful) {
			dispatch(CustomerActions.stateReset({
				fetchCustomerDetailsSuccessful: false
			}))
			setFirstName(customerDetails.firstName)
			setLastName(customerDetails.lastName)
			setEmailId(customerDetails.emailId)
			setCreatedAt(customerDetails.createdAt)
			setAddresses(customerDetails.addresses)

		}
	}, [fetchCustomerDetailsSuccessful])

	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"6"} />
			<div className="container">
				<div className="card-root">
					<ContainerHeader title={"Customer View Page"}
						onClickBackButton={() => Router.back()}
					/>
					<div>
						<Row gutter={24}>
							<Col span={24} >
								<Card title="Customer Details" bordered={true} >
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>First Name : </p>
										</Col>
										<Col span={12} >
											{firstName}
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>Last Name : </p>
										</Col>
										<Col span={12} >
											{lastName}
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>EmailID : </p>
										</Col>
										<Col span={12} >
											{emailId}
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>Registered At : </p>
										</Col>
										<Col span={12} >
											{moment(createdAt).format("MMMM Do YYYY, h:mm a").toString()}
										</Col>
									</Row>
								</Card>
							</Col>


							<Col span={24} style={{ marginTop: "2%" }} >
								<Card title="Address Details" bordered={true}>
									{addresses.map((address, index) => {
										return (
											<div>
												{index !== 0 ? (
												<Divider></Divider>
												) : null}
												<Row gutter={24}>
													<Col span={12} >
														<p style={{ fontWeight: "bold" }}>Address Type :  </p>
													</Col>
													<Col span={12} >
														{address.type}
													</Col>
												</Row>
												<Row gutter={24}>
													<Col span={12} >
														<p style={{ fontWeight: "bold" }}>Door Number :  </p>
													</Col>
													<Col span={12} >
														{address.doorNumber}
													</Col>
												</Row>
												<Row gutter={24}>
													<Col span={12} >
														<p style={{ fontWeight: "bold" }}>LandMark : </p>
													</Col>
													<Col span={12} >
														{address.landmark}
													</Col>
												</Row>
												<Row gutter={24}>
													<Col span={12} >
														<p style={{ fontWeight: "bold" }}>Street : </p>
													</Col>
													<Col span={12} >
														{address.street}
													</Col>
												</Row>
												<Row gutter={24}>
													<Col span={12} >
														<p style={{ fontWeight: "bold" }}>District : </p>
													</Col>
													<Col span={12} >
														{address.district}
													</Col>
												</Row>
												<Row gutter={24}>
													<Col span={12} >
														<p style={{ fontWeight: "bold" }}>State : </p>
													</Col>
													<Col span={12} >
														{address.state}
													</Col>
												</Row>
												<Row gutter={24}>
													<Col span={12} >
														<p style={{ fontWeight: "bold" }}>Pincode : </p>
													</Col>
													<Col span={12} >
														{address.pincode}
													</Col>
												</Row>
												<Row gutter={24}>
													<Col span={12} >
														<p style={{ fontWeight: "bold" }}>Phone Number : </p>
													</Col>
													<Col span={12} >
														{address.phoneNumber}
													</Col>
												</Row>
												<Row gutter={24}>
													<Col span={12} >
														<p style={{ fontWeight: "bold" }}>Secondary Phone Number : </p>
													</Col>
													<Col span={12} >
														{address.secondaryPhoneNumber}
													</Col>
												</Row>
											</div>
										)
									})}
								</Card>
							</Col>

						</Row>
					</div>
				</div>
			</div>
		</Wrapper>
	);
};

export default ViewCustomer;

const Wrapper = styled.div`
	background-color: ${Themes.backgroundMain};
	height: 100%;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		display: flex;
		justify-content: center;
		align-items: center;
		height: 90%;
		.card-root {
			width: 80%;
			height: 100%;
			background-color: ${Themes.white};
			padding: 40px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.cancel-btn {
		background-color: ${Themes.cancelButton};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.row {
		display: flex;
		flex-direction: row;
		justify-content: center;
		align-items: center;
	}
`;
