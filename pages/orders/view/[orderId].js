import { useEffect, useState } from "react";
import styled from "styled-components";
import { message, Row, Col, Button, Select, Typography, Input, Upload, Card , Avatar } from "antd";
const { Meta } = Card;
const { Option } = Select;
const { Title } = Typography;
import { LoadingOutlined, PlusOutlined, UploadOutlined, MinusCircleOutlined, CheckOutlined, CloseOutlined } from '@ant-design/icons';

import TopHeader from "../../../components/TopHeader";
import SideMenu from "../../../components/SideMenu";
import ContainerHeader from "../../../components/ContainerHeader";
import InputContainer from "../../../components/InputContainer";

import OrderActions from "../../../redux/actions/orders"
import Themes from "../../../themes/themes";
import { onNavigate } from "../../../util/navigation";
import Router, { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";

import moment from 'moment';
import theme from "styled-theming";

const ViewOrder = () => {
	const router = useRouter()
	const dispatch = useDispatch()

	const { error, orderDetails, fetchOrderDetailsSuccessful, fetchingOrderDetails } = useSelector(state => state.orders)


	const [orderId, setOrderId] = useState("");
	const [orderID, setOrderID] = useState("");
	const [customerDetails, setCustomerDetails] = useState({});
	const [deliveryCharge, setDeliveryCharge] = useState();
	const [totalPrice, setTotalPrice] = useState();
	const [status, setStatus] = useState("");
	const [isCombo, setIsCombo] = useState(false);
	const [createdAt, setCreatedAt] = useState("");
	const [address, setAddress] = useState({});
	const [isPaid, setIsPaid] = useState(false);
	const [paymentMethod, setPaymentMethod] = useState("");
	const [orderItems, setOrderItems] = useState([]);
	const [trackingId, setTrackingId] = useState("")
	const [trackingLink, setTrackingLink] = useState("")
	const [deliveryPartner, setDeliveryPartner] = useState("")
	const [razorPay, setRazorPay] = useState({})
	const [combo, setCombo] = useState({})

	useEffect(() => {
		dispatch(OrderActions.stateReset({
			fetchOrderDetailsSuccessful: false,
			fetchingOrderDetails: false
		}))
		let { orderId } = router.query
		if (orderId) {
			setOrderId(orderId)
		} else {
			onNavigate(Router, "orders")
		}
	}, [])


	useEffect(() => {
		if (orderId) {
			dispatch(OrderActions.getOrderDetailsRequest(orderId))
		}
	}, [orderId])

	useEffect(() => {
		if (!fetchingOrderDetails && fetchOrderDetailsSuccessful) {
			dispatch(OrderActions.stateReset({
				fetchOrderDetailsSuccessful: false
			}))
			setOrderID(orderDetails.orderID);
			setCustomerDetails(orderDetails.customerId);
			setDeliveryCharge(orderDetails.deliveryCharge);
			setTotalPrice(orderDetails.totalPrice);
			setStatus(orderDetails.status);
			setIsCombo(orderDetails.isCombo);
			setCreatedAt(orderDetails.createdAt);
			setAddress(orderDetails.address);
			setIsPaid(orderDetails.isPaid);
			setTrackingId(orderDetails.trackingId);
			setTrackingLink(orderDetails.trackingLink);
			setDeliveryPartner(orderDetails.deliveryPartner);
			setPaymentMethod(orderDetails.paymentMethod);
			setRazorPay(orderDetails.paymentDetails);
			setOrderItems(orderDetails.orderItems && orderDetails.orderItems.length ? orderDetails.orderItems : []);
			setCombo(orderDetails.combo)

		}
	}, [fetchOrderDetailsSuccessful])

	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"9"} />
			<div className="container">
				<div className="card-root">
					<ContainerHeader title={"Order View Page"}
						onClickBackButton={() => Router.back()}
					/>
					<div>
						<Row gutter={24}>
							<Col span={12} >
								<Card title="Order Details" bordered={true} >
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>Order ID : </p>
										</Col>
										<Col span={12} >
											{orderID}
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>Customer Name : </p>
										</Col>
										<Col span={12} >
											{customerDetails ? customerDetails.firstName + " " + customerDetails.lastName : "-"}
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>Customer Email ID : </p>
										</Col>
										<Col span={12} >
											{customerDetails ? customerDetails.emailId : "-"}
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>Status : </p>
										</Col>
										<Col span={12} >
											{
												status == "ordered" ? "Ordered" :
													status == "packed" ? "Packed" :
														status == "shipped" ? "Shipped" :
															status == "delivered" ? "Delivered" :
																status == "cancelled" ? "Cancelled" :
																	null
											}
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>Ordered At : </p>
										</Col>
										<Col span={12} >
											{moment(createdAt).format("MMMM Do YYYY, h:mm a").toString()}
										</Col>
									</Row>
									{/* <Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>Combo Pack : </p>
										</Col>
										<Col span={12} >
										{isCombo == true ? <CheckOutlined style={{color: "green"}} /> : <CloseOutlined style={{color: "Red"}}/> }
										</Col>
									</Row> */}
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>Tracking ID : </p>
										</Col>
										<Col span={12} >
											{trackingId ? trackingId : "-"}
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>Tracking Link : </p>
										</Col>
										<Col span={12} >
											{trackingLink ? trackingLink : "-"}
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>Delivery Partner : </p>
										</Col>
										<Col span={12} >
											{deliveryPartner ? deliveryPartner : "-"}
										</Col>
									</Row>
								</Card>
							</Col>
							<Col span={12} >
								<Card title="Payment Details" bordered={true}>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>Payment Method :  </p>
										</Col>
										<Col span={12} >
											{paymentMethod == "COD" ? "Cash On Delivery" : "Online Payment"}
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>Ordered Price :  </p>
										</Col>
										<Col span={12} >
											<p> Rs. {totalPrice}</p>
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>Delivery Charge : </p>
										</Col>
										<Col span={12} >
											<p> Rs. {deliveryCharge ? deliveryCharge : 0}</p>
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>Total Amount : </p>
										</Col>
										<Col span={12} >
											<p style={{ fontWeight: "bold", color: "red" }}>Rs. {deliveryCharge ? parseInt(totalPrice) + parseInt(deliveryCharge) : parseInt(totalPrice)}</p>
										</Col>
									</Row>
									<Row gutter={24} style={{ marginTop: "2%" }}>
										<Col span={24} >
											<p style={{ fontWeight: "600", textAlign: "center" }}>RazorPay Details :   </p>
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>RazorPay Order ID : </p>
										</Col>
										<Col span={12} >
											<p>  {razorPay && razorPay.razorpayOrderId ? razorPay.razorpayOrderId : '-'}</p>
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>RazorPay Payment ID : </p>
										</Col>
										<Col span={12} >
											<p>  {razorPay && razorPay.razorpayPaymentId ? razorPay.razorpayPaymentId : '-'}</p>
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>RazorPay Signature : </p>
										</Col>
										<Col span={12} >
											<p style={{ wordWrap: "break-word" }}>  {razorPay && razorPay.razorpaySignature ? razorPay.razorpaySignature : '-'}</p>
										</Col>
									</Row>
								</Card>

							</Col>
							<Col span={24} style={{ marginTop: "2%" }} >
								<Card title="Address" bordered={true}>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>Customer Name :  </p>
										</Col>
										<Col span={12} >
											{address.name}
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>Address Type :  </p>
										</Col>
										<Col span={12} >
											{address.type}
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>Door Number :  </p>
										</Col>
										<Col span={12} >
											{address.doorNumber}
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>LandMark : </p>
										</Col>
										<Col span={12} >
											{address.landmark}
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>Street : </p>
										</Col>
										<Col span={12} >
											{address.street}
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>District : </p>
										</Col>
										<Col span={12} >
											{address.district}
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>State : </p>
										</Col>
										<Col span={12} >
											{address.state}
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>Pincode : </p>
										</Col>
										<Col span={12} >
											{address.pincode}
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>Phone Number : </p>
										</Col>
										<Col span={12} >
											{address.phoneNumber}
										</Col>
									</Row>
									<Row gutter={24}>
										<Col span={12} >
											<p style={{ fontWeight: "bold" }}>Secondary Phone Number : </p>
										</Col>
										<Col span={12} >
											{address.secondaryPhoneNumber}
										</Col>
									</Row>
								</Card>
							</Col>
							<Col span={24} style={{ marginTop: "4%" }} >
								<Card title="Retail Products " bordered={true}>
									{orderItems.map(item => {
										return (
											<div style={{ display: "block", marginTop: "5%" }}>
												<Row gutter={24}>
													<Col span={3} >
														<p style={{ fontWeight: "bold" }}> Image Url : </p>
														<img style={{ width: 50, height: 50, borderRadius: 50 }} src={item.imageUrl}></img>
													</Col>
													<Col span={5} >
														<p style={{ fontWeight: "bold" }}> Inventory Name : </p>
														{item.inventoryName}
													</Col>
													<Col span={2} >
														<p style={{ fontWeight: "bold" }}> Size : </p>
														{item.size}
													</Col>
													<Col span={2} >
														<p style={{ fontWeight: "bold" }}> Color : </p>
														<div
															style={{
																backgroundColor: item.color,
																height: 30,
																width: 30,
																borderRadius: 50,
																border: "2px solid #000"
															}}
														/>
													</Col>
													<Col span={4} >
														<p style={{ fontWeight: "bold" }}> Quantity : </p>
														{item.quantity}
													</Col>
													<Col span={4} >
														<p style={{ fontWeight: "bold" }}> Price : </p>
														{item.price}
													</Col>
													<Col span={4} >
														<p style={{ fontWeight: "bold" }}> Special Price : </p>
														{item.specialPrice}
													</Col>
												</Row>
											</div>)

									})}
								</Card>
							</Col>

							<Col span={24} style={{ marginTop: "4%" }} >
								<Card title="Combo List " bordered={true}>

								{combo && Object.keys(combo).length != 0 ? Object.keys(combo).map((comboKey, index) => {
										let comboValue = combo[comboKey]
										return (
											<div>
												<Card cover={
												<img
													alt="example"
													style={{ width: "100%", height: "300px",  }}
													src={comboValue[0].comboImageUrl}
												/>} title={comboValue[0].comboName} bordered={true}>
													<Card type="inner" style={{width:"25%" ,display: "inline-block", marginRight: "90px"}} title="Price">
													{comboValue[0].price}
													</Card>
													<Card type="inner" style={{width:"25%", display: "inline-block", marginRight: "90px"}} title="Special Price">
													{comboValue[0].specialPrice}
													</Card>
													<Card type="inner" style={{width:"25%" ,display: "inline-block"}} title="Number Of Items">
													{comboValue[0].comboItems}
													</Card>
												{comboValue.map(item => {
													return (
													<div style={{ display: "block", marginTop: "5%" }}>
														<Row gutter={24}>
															<Col span={3} >
																<p style={{ fontWeight: "bold" }}> Image Url : </p>
																<img style={{ width: 50, height: 50, borderRadius: 50 }} src={item.imageUrl}></img>
															</Col>
															<Col span={5} >
																<p style={{ fontWeight: "bold" }}> Inventory Name : </p>
																{item.inventoryName}
															</Col>
															<Col span={2} >
																<p style={{ fontWeight: "bold" }}> Size : </p>
																{item.size}
															</Col>
															<Col span={2} >
																<p style={{ fontWeight: "bold" }}> Color : </p>
																<div
																	style={{
																		backgroundColor: item.color,
																		height: 30,
																		width: 30,
																		borderRadius: 50,
																		border: "2px solid #000"
																	}}
																/>
															</Col>
															<Col span={4} >
																<p style={{ fontWeight: "bold" }}> Quantity : </p>
																{item.quantity}
															</Col>
															<Col span={4} >
																<p style={{ fontWeight: "bold" }}> Price : </p>
																{item.price}
															</Col>
															<Col span={4} >
																<p style={{ fontWeight: "bold" }}> Special Price : </p>
																{item.specialPrice}
															</Col>
														</Row>
													</div>)
												})}
											</Card>
											</div>
										)

									}) : null}
								</Card>
							</Col>
						</Row>
					</div>
				</div>
			</div>
		</Wrapper>
	);
};

export default ViewOrder;

const Wrapper = styled.div`
	background-color: ${Themes.backgroundMain};
	height: 100%;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		display: flex;
		justify-content: center;
		align-items: center;
		height: 90%;
		.card-root {
			width: 80%;
			height: 100%;
			background-color: ${Themes.white};
			padding: 40px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.cancel-btn {
		background-color: ${Themes.cancelButton};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.row {
		display: flex;
		flex-direction: row;
		justify-content: center;
		align-items: center;
	}
`;
