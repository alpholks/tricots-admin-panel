import { useEffect, useState } from "react";
import styled from "styled-components";
import { message, Switch, Row, Col, Typography, Select } from "antd";
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';

const { Title } = Typography;
const { Option } = Select;
import TopHeader from "../../components/TopHeader";
import SideMenu from "../../components/SideMenu";
import ContainerHeader, { FilterOptions } from "../../components/ContainerHeader";
import TableComponent, { UsualActions } from "../../components/Table";
import CenteredModal from "../../components/CenteredModal";

import OrderActions from "../../redux/actions/orders"
import Themes from "../../themes/themes";
import Router from "next/router";
import { onNavigate } from "../../util/navigation";
import { useDispatch, useSelector } from "react-redux";
import InputContainer from "../../components/InputContainer";

const Orders = () => {
	const dispatch = useDispatch()
	const { error, order, orders, fetchingOrders, fetchOrderSuccessful, updatingOrder, updateOrderSuccessful } = useSelector(state => state.orders)

	const [deleteConfirmationModal, setDeleteModalOpen] = useState(false);
	const [updateOrderModal, setUpdateOrderModal] = useState(false);
	const [deleteId, setDeleteId] = useState("");
	const [orderId, setOrderId] = useState("");
	const [searchText, setSearchText] = useState("")
	const [status, setStatus] = useState("")
	const [isPaid, setIsPaid] = useState(false)
	const [trackingId, setTrackingId] = useState("")
	const [trackingLink, setTrackingLink] = useState("")
	const [deliveryPartner, setDeliveryPartner] = useState("")

	useEffect(() => {
		dispatch(OrderActions.stateReset({
			fetchingOrders: false,
			fetchOrderSuccessful: false,

			updatingOrder: false,
			updateOrderSuccessful: false
		}))
	}, [])

	useEffect(() => {
		if (error) {
			message.error(error)
			dispatch(OrderActions.clearError())
		}
	}, [error])

	useEffect(() => {
		var queryParams = ""
		dispatch(OrderActions.getOrderRequest(queryParams))
	}, [])

	useEffect(() => {
		if (updateOrderModal == false) {
			var queryParams = ""
			dispatch(OrderActions.getOrderRequest(queryParams))
		}
	}, [updateOrderModal])

	useEffect(() => {
		if (!fetchingOrders && fetchOrderSuccessful) {
			dispatch(OrderActions.stateReset({
				fetchOrderSuccessful: false
			}))
		}
	}, [fetchOrderSuccessful])

	useEffect(() => {
		if (!updatingOrder && updateOrderSuccessful) {
			dispatch(OrderActions.stateReset({
				updateOrderSuccessful: false
			}))
			setDeleteModalOpen(false);
			message.success("Order Updated successfully");
			setUpdateOrderModal(false)
		}
	}, [updateOrderSuccessful])

	useEffect(() => {
		var queryParams = searchText ? `?search=${searchText}` : "";
		dispatch(OrderActions.getOrderRequest(queryParams))
	}, [searchText])

	const handleOpenUpdateModal = (order) => {
		setOrderId(order._id)
		setTrackingId(order.trackingId)
		setIsPaid(order.isPaid)
		setTrackingLink(order.trackingLink)
		setDeliveryPartner(order.deliveryPartner)
		setStatus(order.status)
		setUpdateOrderModal(true);
	};

	const handleCancel = () => {
		setUpdateOrderModal(false)
	};

	const handleNavigation = (path) => {
		return onNavigate(Router, path);
	};

	const handleUpdateOrder = () => {
		dispatch(OrderActions.updateOrderRequest(orderId, {
			status, isPaid, trackingId, trackingLink, deliveryPartner
		}))
	};

	const columns = [
		{ title: "Order ID", dataIndex: "orderID", key: "orderID" },
		{
			title: "Customer Name", key: "customerId", dataIndex: "customerId", render: customerId => (
				<p> {customerId.firstName + " " + customerId.lastName}</p>
			)
		},
		{ title: "Payment Method", key: "paymentMethod", dataIndex: "paymentMethod" },
		{ title: "Price", key: "totalPrice", dataIndex: "totalPrice", render: totalPrice => (
			<p>Rs. {totalPrice}</p>
		) },
		{ title: "Delivery Charge", key: "deliveryCharge", dataIndex: "deliveryCharge", render: deliveryCharge => (
			<p>Rs. {deliveryCharge ? deliveryCharge : 0}</p>
		) },
		{
			title: "Status", key: "status", dataIndex: "status", render: status => (
				<p>{
					status == "ordered" ? "Ordered" :
						status == "packed" ? "Packed" :
							status == "shipped" ? "Shipped" :
								status == "delivered" ? "Delivered" :
									status == "cancelled" ? "Cancelled" :
										status == "RTO" ? "RTO" :
											null
				}</p>
			)
		},
		{
			title: "Paid", key: "isPaid", dataIndex: "isPaid", render: isPaid => (
				<p> {isPaid == true ? "Paid" : "Unpaid"}</p>
			)
		},
		// {
		// 	title: "Combo Pack", key: "isCombo", dataIndex: "isCombo", render: isCombo => (
		// 		<p> {isCombo == true ? "Yes" : "No"}</p>
		// 	)
		// },
		{
			title: "Action", key: "_id",
			render: (text, record) => (
				<UsualActions
					onEdit={() => handleOpenUpdateModal(text)}
					onView={() => handleNavigation("/orders/view/" + text._id)}
					onDelete={false}
				/>
			)
		},
	];

	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"9"} />
			<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=your_api_key_here"></script>

			<div className="container">
				<div className="card-root">
					<ContainerHeader
						title="Orders"
					/>
					{/* <FilterOptions
						search
						inputValue={searchText}
						placeholder={"Search"}
						onChangeInput={(e) => setSearchText(e.target.value)}
					/> */}
					<TableComponent loading={fetchingOrders} data={orders} columns={columns} />
					{/* <CenteredModal
						visible={deleteConfirmationModal}
						loading={deletingOrder}
						submit={"Delete"}
						cancel="Cancel"
						handleSubmit={handleDeleteOrder}
						handleCancel={handleCancel}
						title="Delete Order"
					>
						<p>Are you sure you want to delete order ?</p> */}
					{/* </CenteredModal> */}

					<CenteredModal
						visible={updateOrderModal}
						submit={"Submit"}
						cancel="Cancel"
						handleSubmit={handleUpdateOrder}
						handleCancel={handleCancel}
						title="Update Order"
					>
						<div>
							<Row gutter={24}>
								<Col span={12}>
									<Title level={5} className="title">Status</Title>
									<Select
										className="select"
										value={status}
										onChange={(value) => setStatus(value)}
										style={{ width: '100%', backgroundColor: "#f0f0f0" }}
									>
										<Option value="ordered">Ordered</Option>
										<Option value="packed">Packed</Option>
										<Option value="shipped">Shipped</Option>
										<Option value="delivered">Delivered</Option>
										<Option value="cancelled">Cancelled</Option>
										<Option value="RTO">RTO</Option>
									</Select>
								</Col>
								<Col span={12}>
									<Title level={5} className="title">Is Paid</Title>
									<Switch checked={isPaid} onChange={() => {
										if (isPaid == true) {
											setIsPaid(false)
										} else {
											setIsPaid(true)
										}
									}}>

									</Switch>
									{/* <Select
										className="select"
										value={isPaid}
										onChange={(value) => setIsPaid(value)}
										style={{ width: '100%',backgroundColor:"#f0f0f0"}}
									>
											<Option value={true}>Paid</Option>
											<Option value={false}>Unpaid</Option>
									</Select> */}
								</Col>
								<Col span={12}>
									<InputContainer
										title="Tracking ID"
										value={trackingId}
										onChange={(e) => setTrackingId(e.target.value)}
									/>
								</Col>
								<Col span={12}>
									<InputContainer
										title="Tracking Link"
										value={trackingLink}
										onChange={(e) => setTrackingLink(e.target.value)}
									/>
								</Col>
								<Col span={12}>
									<InputContainer
										title="Delivery Partner"
										value={deliveryPartner}
										onChange={(e) => setDeliveryPartner(e.target.value)}
									/>
								</Col>
							</Row>
						</div>
					</CenteredModal>
				</div>
			</div>
		</Wrapper>
	);
};

export default Orders;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow-y: scroll;
	overflow-x: hidden;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
			width: 100%;
			background-color: #fff;
			padding: 20px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
	}
`;

const data = [
	{
		name: "John Brown",
		imageId: {
			imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
		},
		mobileImageId: {
			imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
		},
		isEnabled: true,
		priority: 3,
		type: "Large",
		link: "https://url"
	},
	{
		name: "John Brown",
		imageId: {
			imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
		},
		mobileImageId: {
			imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
		},
		isEnabled: false,
		priority: 5,
		type: "Small",
		link: "https://url"
	}
];
