import { useEffect, useState } from "react";
import styled from "styled-components";
import { message, Row, Col, Button ,Select ,Typography ,Input, Upload, Divider } from "antd";
const { Option } = Select;
const { Title } = Typography;
import { LoadingOutlined, PlusOutlined, UploadOutlined, MinusCircleOutlined  } from '@ant-design/icons';

import TopHeader from "../../../components/TopHeader";
import SideMenu from "../../../components/SideMenu";
import ContainerHeader from "../../../components/ContainerHeader";
import InputContainer from "../../../components/InputContainer";

import ComboActions from "../../../redux/actions/combos"
import CustomerActions from "../../../redux/actions/customers"
import Themes from "../../../themes/themes";
import { onNavigate } from "../../../util/navigation";
import Router,{ useRouter } from "next/router";
import { useDispatch , useSelector} from "react-redux";

const ViewCombo = () => {
	const router = useRouter()
	const dispatch = useDispatch()

	const {error, comboDetails, fetchComboDetailsSuccessful, fetchingComboDetails} = useSelector(state => state.combos)


	const [comboId, setComboId] = useState("");
    const [name, setName] = useState("");
    const [price, setPrice] = useState("");
    const [specialPrice, setSpecialPrice] = useState("");
    const [productIds, setProductIds] = useState([]);
    const [additionalImageIds, setAdditionalImageIds] = useState([]);
    const [existingProducts, setExistingProducts] = useState([]);
    const [isEnabled, setIsEnabled] = useState(false);
    const [numberOfItems, setNumberOfItems] = useState("");
    const [imageUrl, setImageUrl] = useState("");
    const [imageId, setImageId] = useState("");
	const [webLoading, setWebLoading] = useState(false);
    const [mobileLoading, setMobileLoading] = useState(false);

	useEffect(()=>{
		dispatch(ComboActions.stateReset({
			fetchComboDetailsSuccessful: false,
			fetchingComboDetails: false
		}))
		let {comboId} = router.query
		if(comboId){
			setComboId(comboId)
		}else {
			onNavigate(Router, "combos")
		}
	},[])


	useEffect(()=>{
		if(comboId){
			dispatch(CustomerActions.getCustomerRequest())
			console.log(comboId)
			dispatch(ComboActions.getComboDetailsRequest(comboId))
		}
	},[comboId])

	useEffect(()=> {
		if(!fetchingComboDetails && fetchComboDetailsSuccessful){
			dispatch(ComboActions.stateReset({
				fetchComboDetailsSuccessful : false
			}))
			setName(comboDetails.name);
            setImageUrl(comboDetails.imageId.imageUrl);
            setImageId(comboDetails.imageId._id);
            setPrice(comboDetails.defaultPrice);
            setIsEnabled(comboDetails.isEnabled);
            setSpecialPrice(comboDetails.specialPrice);
            setNumberOfItems(comboDetails.numberOfItems);
            setExistingProducts(comboDetails.productIds);
			setProductIds(comboDetails.productIds ? comboDetails.productIds.map(product => {
				return product._id;
			}) : null)
		}
	},[fetchComboDetailsSuccessful])

	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"5"} />
			<div className="container">
				<div className="card-root">
					<ContainerHeader title={name} 
						onClickBackButton={() => Router.back()}
					/>
					<div>
                    <Row gutter={24}>
							<Col span={12}>
								<InputContainer
									title="Name"
									value={name}
									disabled
									onChange={(e) => setName(e.target.value)}
								/>
                                <InputContainer
									title="Price"
									disabled
									value={price}
									onChange={(e) => setPrice(e.target.value)}
								/>
							</Col>
							<Col span={12}>
                                <InputContainer
								disabled
                                        title="Number Of Items"
                                        value={numberOfItems}
                                        onChange={(e) => setNumberOfItems(e.target.value)}
                                />
                                <InputContainer
									title="Special Price"
									disabled
									value={specialPrice}
									onChange={(e) => setSpecialPrice(e.target.value)}
								/>
							</Col>
                            <Col span={12}>
                                <Title level={5} className="title">Product</Title>
                                <Select mode="multiple" disabled value={productIds} style={{ width: '100%' }} placeholder="Select the products" >
                                        {existingProducts.map(item => {
                                            return <Option value={item._id}>{item.name}</Option>
                                        })}
                                </Select>
                            </Col>
                        
                            <Col span={12}>
                            <Title level={5} className="title">Web Image</Title>
                            <Upload
                                name="avatar"
                                listType="picture-card"
                                className="avatar-uploader"
                                showUploadList={false}
                                onStart={(file) => {
                                    console.log("onStart", file, file.name);
                                }}
                                onSuccess={(data) => {
                                    __func__uploadImage(data, "web");
                                }}
                                onError={(err) => {
                                    console.log(err);
                                    message.error(`${err} File upload failed.`);
                                }}
                            >
                                {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%', height: "100%" }} /> : (
                                    <div>
                                        {webLoading ? <LoadingOutlined /> : <PlusOutlined />}
                                        <div style={{ marginTop: 8 }}>Upload</div>
                                  </div>
                                )}
                            </Upload>
                            </Col>
                            <Col span={24}>
                            <Title level={5} className="title" style={{display: "inline-block"}}>Enable</Title>
                                {isEnabled 
                                    ? <img 
                                        style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer" }} 
                                        onClick={() => {setIsEnabled(false)}} 
                                        src={require("../../../Images/toggle-success-button.png")}/>
                                    : <img 
                                        style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer" }}
                                        onClick={() => {setIsEnabled(true)}} 
                                        src={require("../../../Images/toggle-failure-button.png")}/>
                                }
                            </Col>
						</Row>
					</div>
				</div>
			</div>
		</Wrapper>
	);
};

export default ViewCombo;

const Wrapper = styled.div`
	background-color: ${Themes.backgroundMain};
	height: 100%;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		display: flex;
		justify-content: center;
		align-items: center;
		height: 90%;
		.card-root {
			width: 80%;
			height: 100%;
			background-color: ${Themes.white};
			padding: 40px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.cancel-btn {
		background-color: ${Themes.cancelButton};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.row {
		display: flex;
		flex-direction: row;
		justify-content: center;
		align-items: center;
	}
`;
