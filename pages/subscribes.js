import { useEffect, useState } from "react";
import styled from "styled-components";
import { message, Switch  } from "antd";
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';

import TopHeader from "../components/TopHeader/TopHeader";
import SideMenu from "../components/SideMenu/SideMenu";
import ContainerHeader,{FilterOptions} from "../components/ContainerHeader/ContainerHeader";
import TableComponent, { UsualActions } from "../components/Table/Table";
import CenteredModal from "../components/CenteredModal/CenteredModal";

import SubscribeActions from "../redux/actions/subscribes"
import Themes from "../themes/themes";
import Router from "next/router";
import { onNavigate } from "../util/navigation";
import { useDispatch, useSelector } from "react-redux";
import moment from 'moment'

const Subscribes = () => {
	const dispatch = useDispatch()
	const {error, subscribe, subscribes, fetchingSubscribes, fetchSubscribeSuccessful,} = useSelector(state => state.subscribes)

	const [searchText, setSearchText] = useState("")
	useEffect(()=>{
		dispatch(SubscribeActions.stateReset({
			fetchingSubscribes : false,
			fetchSubscribeSuccessful : false,

		}))
	},[])

	useEffect(()=>{
		if(error){
			message.error(error)
			dispatch(SubscribeActions.clearError())
		}
	},[error])

	useEffect(()=>{
		var queryParams = ""
		dispatch(SubscribeActions.getSubscribeRequest(queryParams))
	},[])

	useEffect(()=> {
		if(!fetchingSubscribes && fetchSubscribeSuccessful){
			dispatch(SubscribeActions.stateReset({
				fetchSubscribeSuccessful : false
			}))
		}
	},[fetchSubscribeSuccessful])

	useEffect(()=>{
		var queryParams = searchText ? `?search=${searchText}` : "";
		dispatch(SubscribeActions.getSubscribeRequest(queryParams))
	},[searchText])

	const handleNavigation = (path) => {
		return onNavigate(Router, path);
	};

	const columns = [
		{title: "Email", dataIndex: "email", key: "email"},
		{title: "Message",key: "message",dataIndex: "message"},
        {title: "Subscribed At",
            render: text => {
                return (
					<div>
						{moment(text.createdAt).format('YYYY-MM-DD').toString()}
					</div>
				)
            }
        },
	];

	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"14"} />
			<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=your_api_key_here"></script>

			<div className="container">
				<div className="card-root">
					<ContainerHeader
						title="Subscribes"
					/>
					<TableComponent loading={fetchingSubscribes} data={subscribes} columns={columns} />
				</div>
			</div>
		</Wrapper>
	);
};

export default Subscribes;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow-y: scroll;
	overflow-x: hidden;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
			width: 100%;
			background-color: #fff;
			padding: 20px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
	}
`;

const data = [
	{
		name: "John Brown",
		imageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
        mobileImageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
		isEnabled: true,
		priority: 3,
        type: "Large",
        link: "https://url"
	},
    {
		name: "John Brown",
		imageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
        mobileImageId: {
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVMGnIlLZ2kmhsYZ7-VaQm2-r0HhRve9OGxw&usqp=CAU"
        },
		isEnabled: false,
		priority: 5,
        type: "Small",
        link: "https://url"
	}
];
