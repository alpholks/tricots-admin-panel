import { useEffect, useState } from "react";
import styled from "styled-components";
import { message, Input, Row, Col, Typography, Button, Select, Upload } from "antd";
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';

import TopHeader from "../components/TopHeader/TopHeader";
import SideMenu from "../components/SideMenu/SideMenu";
import ContainerHeader from "../components/ContainerHeader/ContainerHeader";
import InputContainer from "../components/InputContainer/InputContainer";

import ProfileActions from "../redux/actions/profiles"
import Themes from "../themes/themes";
import { onNavigate } from "../util/navigation";
import Router, { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";

import axios from 'axios';
import { API_URL } from '../constants';
import Head from "next/head"
import dynamic from "next/dynamic";
const ReactQuill = dynamic(() => import("react-quill"), { ssr: false });
const { Title } = Typography;

const EditProfile = () => {
    const router = useRouter()
    const dispatch = useDispatch()

    const { error, profileDetails, updatingProfile, updateProfileSuccessful, fetchingProfileDetails, fetchProfileDetailsSuccessful } = useSelector(state => state.profiles)

    const [accessToken, setAccessToken] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [website, setWebsite] = useState("");
    const [instagram, setInstagram] = useState("");
    const [whatsapp, setWhatsapp] = useState("");
    const [youtube, setYoutube] = useState("");
    const [twitter, setTwitter] = useState("");
    const [deliveryCharge, setDeliveryCharge] = useState("");
    const [happyCustomers, setHappyCustomers] = useState("");
    const [facebook, setFacebook] = useState("");
    const [brandName, setBrandName] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [secondaryPhoneNumber, setSecondaryPhoneNumber] = useState("");
    const [address, setAddress] = useState("");
    const [emailId, setEmailId] = useState("");
    const [termsAndConditions, setTermsAndConditions] = useState("");
    const [privacyPolicy, setPrivacyPolicy] = useState("");
    const [shippingPolicy, setShippingPolicy] = useState("");
    const [returnPolicy, setReturnPolicy] = useState("");
    const [about, setAbout] = useState("");

	const [imageUrl, setImageUrl] = useState("");
	const [imageId, setImageId] = useState("");
	const [mobileImageUrl, setMobileImageUrl] = useState("");
	const [mobileImageId, setMobileImageId] = useState("");
    const [webLoading, setWebLoading] = useState(false);
    const [mobileLoading, setMobileLoading] = useState(false);
    const customRequest = ({
        action,
        data,
        file,
        filename,
        headers,
        onError,
        onProgress,
        onSuccess,
        withCredentials,
    }) => {
        const formData = new FormData();
        if (data) {
            Object.keys(data).forEach((key) => {
                formData.append(key, data[key]);
            });
        }
        formData.append("file", file);
        formData.append("type", "profile");

        onSuccess(formData);

        return {
            abort() {
                // message.error('File Upload Aborted')
            },
        };
    };

    const __func__uploadImage = (data, imageType) => {
        if (imageType == "web") {
            setImageUrl("")
            setWebLoading(true);
        } else if (imageType == "mobile") {
            setMobileImageUrl("")
            setMobileLoading(true);
        }
        axios
            .post(`${API_URL}/admin/thumbnail/upload`, data, {
                "content-type": "multipart/form-data",
                headers: { Authorization: `JWT ${accessToken}` },
            })
            .then((res) => {
                setWebLoading(false);
                setMobileLoading(false);
                if (res.data && res.data.data && res.data.data.imageUrl) {
                    if (imageType === "web") {
                        setImageUrl(res.data.data.imageUrl);
                        setImageId(res.data.data._id);
                    } else if (imageType === "mobile") {
                        setMobileImageUrl(res.data.data.imageUrl);
                        setMobileImageId(res.data.data._id);
                    }
                } else {
                    message.error("Image Upload Failed. Try Again.")
                }
            }).catch((error) => {
                if (imageType == "web") {
                    setWebLoading(false);
                } else if (imageType == "mobile") {
                    setMobileLoading(false);
                }
                message.error("Image Upload Failed. Try Again.")
            });
    };

    useEffect(() => {
        let token = localStorage.getItem('accessToken');
        setAccessToken(token);
    }, [])

    useEffect(() => {
        dispatch(ProfileActions.stateReset({
            fetchingProfileDetails: false,
            fetchProfileDetailsSuccessful: false,

            updatingProfile: false,
            updateProfileSuccessful: false,
        }))
    }, [])

    useEffect(() => {
        if (error) {
            message.error(error)
            dispatch(ProfileActions.clearError())
        }
    }, [error])

    useEffect(() => {
        dispatch(ProfileActions.getProfileDetailsRequest())
    }, [])

    useEffect(() => {
        if (!fetchingProfileDetails && fetchProfileDetailsSuccessful) {
            setFirstName(profileDetails.firstName)
            setLastName(profileDetails.lastName)
            setWhatsapp(profileDetails.whatsapp)
            setTwitter(profileDetails.twitter)
            setYoutube(profileDetails.youtube)
            setWebsite(profileDetails.website)
            setImageUrl(profileDetails.logoUrl)
            setInstagram(profileDetails.instagram)
            setFacebook(profileDetails.facebook)
            setBrandName(profileDetails.brandName)
            setPhoneNumber(profileDetails.phoneNumber)
            setSecondaryPhoneNumber(profileDetails.secondaryPhoneNumber)
            setAddress(profileDetails.address ? profileDetails.address : "")
            setEmailId(profileDetails.emailId)
            setTermsAndConditions(profileDetails.termsAndConditions ? profileDetails.termsAndConditions : "")
            setPrivacyPolicy(profileDetails.privacyPolicy ? profileDetails.privacyPolicy : "")
            setShippingPolicy(profileDetails.shippingPolicy ? profileDetails.shippingPolicy : "")
            setDeliveryCharge(profileDetails.deliveryCharge)
            setHappyCustomers(profileDetails.happyCustomers)
            setReturnPolicy(profileDetails.returnPolicy ? profileDetails.returnPolicy : "")
            setAbout(profileDetails.about ? profileDetails.about : "")

            dispatch(ProfileActions.stateReset({
                fetchProfileDetailsSuccessful: false
            }))
        }
    }, [fetchProfileDetailsSuccessful]);

    useEffect(() => {
        if (!updatingProfile && updateProfileSuccessful) {
            dispatch(ProfileActions.stateReset({
                updateProfileSuccessful: false
            }))
            message.success("Profile updated successfully");
        }
    }, [updateProfileSuccessful])


    const handleClickCancel = () => {
        onNavigate(Router, "/profiles");
    };
    const handleClickUpdate = () => {
        dispatch(ProfileActions.updateProfileRequest({
            firstName,
            lastName,
            website,
            instagram,
            facebook,
            brandName,
            phoneNumber,
            secondaryPhoneNumber,
            address,
            emailId,
            termsAndConditions,
            privacyPolicy,
            returnPolicy,
            about,
            shippingPolicy,
            deliveryCharge,
            happyCustomers,
            whatsapp,
            twitter,
            youtube,
            logoUrl: imageUrl
        }))
    };


    return (
        <Wrapper>
            <Head>
				<link
					rel="stylesheet"
					href="//cdn.quilljs.com/1.2.6/quill.snow.css"
				/>
			</Head>
            <TopHeader />
            <SideMenu selectedKey={"12"} />
            <div className="container">
                <div className="card-root">
                    <div>
                        <Row gutter={24}>
                            <Col span={12}>
                                <InputContainer
                                    title="First Name"
                                    value={firstName}
                                    onChange={(e) => setFirstName(e.target.value)}
                                />
                                <InputContainer
                                    title="Last Name"
                                    value={lastName}
                                    onChange={(e) => setLastName(e.target.value)}
                                />
                            </Col>
                            <Col span={12}>
                                <InputContainer
                                    title="Website"
                                    value={website}
                                    onChange={(e) => setWebsite(e.target.value)}
                                />
                                <InputContainer
                                    title="Instagram"
                                    value={instagram}
                                    onChange={(e) => setInstagram(e.target.value)}
                                />
                            </Col>
                            <Col span={12}>
                                <InputContainer
                                    title="WhatsApp"
                                    value={whatsapp}
                                    onChange={(e) => setWhatsapp(e.target.value)}
                                />
                                <InputContainer
                                    title="Twitter"
                                    value={twitter}
                                    onChange={(e) => setTwitter(e.target.value)}
                                />

                            </Col>
                            <Col span={12}>
                                <InputContainer
                                    title="Youtube"
                                    value={youtube}
                                    onChange={(e) => setYoutube(e.target.value)}
                                />
                                <InputContainer
                                    title="Facebook"
                                    value={facebook}
                                    onChange={(e) => setFacebook(e.target.value)}
                                />
                            </Col>
                            <Col span={12}>
                                <InputContainer
                                    title="Brand Name"
                                    value={brandName}
                                    onChange={(e) => setBrandName(e.target.value)}
                                />
                                <InputContainer
                                    title="Email ID"
                                    value={emailId}
                                    onChange={(e) => setEmailId(e.target.value)}
                                />
                            </Col>
                            <Col span={12}>
                                <InputContainer
                                    title="Phone Number"
                                    value={phoneNumber}
                                    onChange={(e) => setPhoneNumber(e.target.value)}
                                />
                                <InputContainer
                                    title="Secondary Phone Number"
                                    value={secondaryPhoneNumber}
                                    onChange={(e) => setSecondaryPhoneNumber(e.target.value)}
                                />
                            </Col>
                            <Col span={12}>

                                <InputContainer
                                    title="Delivery Charge"
                                    value={deliveryCharge}
                                    onChange={(e) => setDeliveryCharge(e.target.value)}
                                />
                                </Col>
                            <Col span={12}>
                                <InputContainer
                                    title="Happy Customers"
                                    value={happyCustomers}
                                    onChange={(e) => setHappyCustomers(e.target.value)}
                                />

                            </Col>
                            <Col span={12}>
                                <Title level={5} className="title">Logo Upload</Title>
                                <Upload
                                    name="avatar"
                                    listType="picture-card"
                                    className="avatar-uploader"
                                    showUploadList={false}
                                    onStart={(file) => {
                                        console.log("onStart", file, file.name);
                                    }}
                                    onSuccess={(data) => {
                                        __func__uploadImage(data, "web");
                                    }}
                                    onError={(err) => {
                                        console.log(err);
                                        message.error(`${err} File upload failed.`);
                                    }}
                                    customRequest={customRequest}
                                >
                                    {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%', height: "100%" }} /> : (
                                        <div>
                                            {webLoading ? <LoadingOutlined /> : <PlusOutlined />}
                                            <div style={{ marginTop: 8 }}>Upload</div>
                                        </div>
                                    )}
                                </Upload>
                            </Col>
                            <Col span={24}>
                            <Title level={5} className="title">Address</Title>
                                <ReactQuill
									value={address}
									onChange={setAddress}
								/>

                            </Col>
                            <Col span={24}>
                            <Title level={5} className="title">Terms and Conditions</Title>
                                <ReactQuill
									value={termsAndConditions}
									onChange={setTermsAndConditions}
								/>

                            </Col>
                            <Col span={24}>
                            <Title level={5} className="title">Privacy Policy</Title>
                                  <ReactQuill
									value={privacyPolicy}
									onChange={setPrivacyPolicy}
								/>
                            </Col>
                            <Col span={24}>
                            <Title level={5} className="title">Return, Refund and Cancellation</Title>
                                  <ReactQuill
									value={returnPolicy}
									onChange={setReturnPolicy}
								/>
                            </Col>
                            <Col span={24}>
                            <Title level={5} className="title">Shipping Policy</Title>
                                <ReactQuill
									value={shippingPolicy}
									onChange={setShippingPolicy}
								/>

                            </Col> 
                            <Col span={24}>
                            <Title level={5} className="title">About</Title>
                                 <ReactQuill
									value={about}
									onChange={setAbout}
								/>

                            </Col>
                        </Row>
                        <div className="row">
                            {/* <Button className="cancel-btn" onClick={handleClickCancel}>
                                Cancel
							</Button> */}
                            <Button loading={updatingProfile} className="create-btn" onClick={handleClickUpdate}>
                                Save
							</Button>
                        </div>
                    </div>
                </div>
            </div>
        </Wrapper>
    );
};

export default EditProfile;

const Wrapper = styled.div`
	background-color: ${Themes.backgroundMain};
	height: 100vh;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		display: flex;
		justify-content: center;
		align-items: center;
		.card-root {
			width: 80%;
			background-color: ${Themes.white};
			padding: 40px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.cancel-btn {
		background-color: ${Themes.cancelButton};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.row {
		display: flex;
		flex-direction: row;
		justify-content: center;
		align-items: center;
	}
	.customInputBox{
		border-radius: 5px;
    	background-color: #f0f0f0;
    	border: none;
		height: 40px;
		width: 90px;
		margin-left:30px;
	}	
	.title1{
		margin-top:20px;
		margin-left:30px
	}
	.hide{
		display:none;
	}
	.title{
        margin-top:20px
    }
`;
