import { useState, useEffect } from "react";
import styled from "styled-components";
import { message, Row, Col, Button, Select, Typography, Input, Upload, } from "antd";
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';

import TopHeader from "../../components/TopHeader";
import SideMenu from "../../components/SideMenu";
import ContainerHeader from "../../components/ContainerHeader";
import InputContainer from "../../components/InputContainer";

import HeaderActions from "../../redux/actions/headers"
import CollectionActions from "../../redux/actions/collections"
import { useDispatch, useSelector } from "react-redux";
import Themes from "../../themes/themes";
import { onNavigate } from "../../util/navigation";
import Router from "next/router";
import axios from 'axios';
import { API_URL } from '../../constants';
import MenuItem from "antd/lib/menu/MenuItem";

const { Title } = Typography;

const CreateHeader = () => {
    const dispatch = useDispatch()
    const { error, creatingHeader, createHeaderSuccessful } = useSelector(state => state.headers)
    const {error: collectionError, collections, fetchingCollections, fetchCollectionSuccessful} = useSelector(state => state.collections)

    const [accessToken, setAccessToken] = useState("");
    const [title, setTitle] = useState("");
    const [priority, setPriority] = useState(0);
    const [isEnabled, setIsEnabled] = useState(false);
    const [subMenu, setSubMenu] = useState([]);

    useEffect(()=>{
		var queryParams = ""
		dispatch(CollectionActions.getCollectionRequest(queryParams))
	},[])

	useEffect(()=> {
		if(!fetchingCollections && fetchCollectionSuccessful){
			dispatch(CollectionActions.stateReset({
				fetchCollectionSuccessful : false
			}))
		}
	},[fetchCollectionSuccessful])

    useEffect(() => {
        let token = localStorage.getItem('accessToken');
        setAccessToken(token);
    }, [])

    useEffect(() => {
        dispatch(HeaderActions.stateReset({
            creatingHeader: false,
            createHeaderSuccessful: false,
        }))
    }, [])

    useEffect(() => {
        dispatch(HeaderActions.clearError())
    }, [])

    useEffect(() => {
        if (error)
            message.error(error)
    }, [error])

    useEffect(() => {
        if (!creatingHeader && createHeaderSuccessful) {
            dispatch(HeaderActions.stateReset({
                createHeaderSuccessful: false
            }))
            message.success("Header created successfully");
            onNavigate(Router, "/headers");
        }
    }, [createHeaderSuccessful])

    const handleClickCancel = () => {
        onNavigate(Router, "/headers");
    };
    const handleClickCreate = () => {
        if (title === "" || priority === 0) {
            return message.warn("Please fill all the fields")
        }
        dispatch(HeaderActions.createHeaderRequest({ title, priority, subMenu, isEnabled }))
    }

    const __func__addNewFields = () => {
        var newSubMenu = [];

        newSubMenu = newSubMenu.concat(subMenu);
        newSubMenu.push({
            name: "",
            isEnabled: false,
            type: "",
            collectionId: null
        });
        setSubMenu(newSubMenu);
    }

    const __func__removeSubMenu = async (index) => {
        var newSubMenu = [];
        await Promise.all(subMenu.map((i, menuIndex) => {
            if (index != menuIndex) {
                newSubMenu.push({
                    name: i.name,
                    isEnabled: i.isEnabled,
                    type: i.type,
                    collectionId: i.collectionId
                })
            }
		}))
		setSubMenu(newSubMenu)
    }


    const __func__updateSubMenu = async (index, name, type, isEnabled, collectionId = null) => {
		var newSubMenu = [];
		await Promise.all(subMenu.map((i, menuIndex) => {
            if (index == menuIndex) {
                newSubMenu.push({
                    name: name,
                    isEnabled: isEnabled,
                    type: type,
                    collectionId: collectionId
                })
            } else {
                newSubMenu.push({
                    name: i.name,
                    isEnabled: i.isEnabled,
                    type: i.type,
                    collectionId: i.collectionId
                })
            }
		}))
		setSubMenu(newSubMenu)
	}



    return (
        <Wrapper>
            <TopHeader />
            <SideMenu selectedKey={"8"} />
            <div className="container">
                <div className="card-root">
                    <ContainerHeader title="Create Header"
                        onClickBackButton={() => Router.back()}
                    />
                    <div>
                        <Row gutter={24}>
                            <Col span={12}>
                                <InputContainer
                                    title="Title"
                                    value={title}
                                    onChange={(e) => setTitle(e.target.value)}
                                />
                            </Col>
                            <Col span={12}>
                                <InputContainer
                                    title="Priority"
                                    value={priority}
                                    onChange={(e) => setPriority(e.target.value)}
                                />
                            </Col>
                            <Col span={24}>
                                <Title level={5} className="title" style={{ display: "inline-block" }}>Enable</Title>
                                {isEnabled
                                    ? <img
                                        style={{ width: 50, height: 30, marginLeft: 50, cursor: "pointer" }}
                                        onClick={() => { setIsEnabled(false) }}
                                        src={require("../../Images/toggle-success-button.png")} />
                                    : <img
                                        style={{ width: 50, height: 30, marginLeft: 50, cursor: "pointer" }}
                                        onClick={() => { setIsEnabled(true) }}
                                        src={require("../../Images/toggle-failure-button.png")} />
                                }
                            </Col>
                            <Col span={24} style={{marginTop: 30, width: "100%"}}>
                                {subMenu.length ? subMenu.map((menu, index) => {
									return (
									<Row gutter={24}>
										<Col span={6}>
                                        <InputContainer
												title="Name"
												value={menu.name}
												onChange={async(e) => {
													await __func__updateSubMenu(index, e.target.value, menu.type, menu.isEnabled, menu.collectionId)
												}}
											/>
										</Col>
										
										<Col span={6}>
                                            <Title level={5} className="title">Type</Title>
                                            <Select
                                                placeholder=""
                                                className="select"
                                                value={menu.type}
                                                onChange={async(value) => {
													await __func__updateSubMenu(index, menu.name, value, menu.isEnabled, menu.collectionId)
												}}
                                                style={{ width: '100%',backgroundColor:"#f0f0f0"}}
                                            >
                                                    <Option value="bestSale">Best Sale</Option>
													<Option value="collection">Collection</Option>
													<Option value="combo">Combo</Option>
													<Option value="featured">Featured</Option>
													<Option value="newArrival">New Arrival</Option>
													<Option value="wholeSale">Whole Sale</Option>
                                            </Select>
										</Col>

                                        {menu.type === "collection" ? (<Col span={6}>
											<Title level={5} className="title">Collection</Title>
                                                <Select style={{ width: '100%' }} placeholder="Select the collection" value={menu.collectionId} onChange={async (value) => {
                                                       await __func__updateSubMenu(index, menu.name, menu.type, menu.isEnabled, value)
                                                    }}>
                                                        {collections.map(item => {
                                                            return <Option value={item._id}>{item.name}</Option>
                                                        })}
                                                </Select>
										</Col>) :<Col span={6}></Col> }
										
										<Col span={4} >
											<Title level={5} className="title" style={{}}>Enabled</Title>
												{menu.isEnabled 
													? <img 
														style={{width: 35, height: 20, cursor: "pointer" }} 
														onClick={async () => {await  await __func__updateSubMenu(index, menu.name, menu.type, false, menu.collectionId)}} 
														src={require("../../Images/toggle-success-button.png")}/>
													: <img 
														style={{width: 35, height: 20, cursor: "pointer" }}
														onClick={async () => {await  await __func__updateSubMenu(index, menu.name, menu.type, true, menu.collectionId)}} 
														src={require("../../Images/toggle-failure-button.png")}/>
												}
											</Col>
										<Col span={2} style={{marginTop: 50}}>
											<MinusCircleOutlined onClick={() => {__func__removeSubMenu(index)}} />
										</Col>
									</Row>
									)
								}) : null}
                                <Button
                                    type="dashed"
                                    onClick={() => __func__addNewFields()}
                                    style={{ left: "40%", marginBottom: "3%", marginTop: "3%"  }}
                                    icon={<PlusOutlined />}
                                >
                                    Add Sub Menu
                                </Button>
                            </Col>

                        </Row>
                        <div className="row">
                            <Button className="cancel-btn" onClick={handleClickCancel}>
                                Cancel
							</Button>
                            <Button loading={creatingHeader} className="create-btn" onClick={handleClickCreate}>
                                Create
							</Button>
                        </div>
                    </div>
                </div>
            </div>
        </Wrapper>
    );
};

export default CreateHeader;

const Wrapper = styled.div`
	background-color: ${Themes.backgroundMain};
	height: 100vh;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		display: flex;
		justify-content: center;
		align-items: center;
		.card-root {
			width: 80%;
			background-color: ${Themes.white};
			padding: 40px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.cancel-btn {
		background-color: ${Themes.cancelButton};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.customInputBox{
		border-radius: 5px;
    	background-color: #f0f0f0;
    	border: none;
		height: 40px;
		width: 90px;
		margin-left:30px;

	}
	.title1{
		margin-top:20px;
		margin-left:30px;
	}
	.row {
		display: flex;
		flex-direction: row;
		justify-content: center;
		align-items: center;
	}
	.title{
        margin-top:20px
    }
	.hide{
		display:none;
	}
`;
