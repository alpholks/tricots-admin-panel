import { useEffect, useState } from "react";
import styled from "styled-components"
import {message,Row,Col,Button,Typography, Input} from "antd"

import TopHeader from "../../../components/TopHeader/TopHeader"
import SideMenu from "../../../components/SideMenu/SideMenu"
import ContainerHeader from "../../../components/ContainerHeader/ContainerHeader"
import MapComponent from "../../../components/MapComponent/MapComponent"
import InputContainer from "../../../components/InputContainer/InputContainer"

import StaffActions from "../../../redux/actions/staffs"
import Themes from "../../../themes/themes"
import {onNavigate} from "../../../util/navigation"
import Router,{useRouter}from "next/router"
import { useDispatch, useSelector } from "react-redux"
const { Title } = Typography;

const ViewStaff = () =>{

    const router = useRouter()
    const dispatch = useDispatch()
    
    

    const {error,staffDetail,fetchingStaffDetail,fetchStaffDetailSuccessfull} = useSelector(state =>state.staffs)

    const {staffId} = router.query
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [emailId, setEmailId] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [password,setPassword] = useState("");
    const [buildingNumber, setBuildingNumber] = useState("");
	const [zoneNumber, setZoneNumber] = useState("");
	const [streetNumber, setStreetNumber] = useState("");

    
    useEffect(()=>{
		dispatch(StaffActions.stateReset({
            fetchingStaffDetail : false,
			fetchStaffDetailSuccessfull : false,
		}))
	},[])

    useEffect(()=>{
		dispatch(StaffActions.clearError())
	},[])

    useEffect(()=>{
		if(error){
            message.error(error)
			dispatch(StaffActions.clearError())
        }    
	},[error])

    useEffect(()=>{
        dispatch(StaffActions.getStaffDetailsRequest(staffId))
	},[])
    
    useEffect(()=> {
		if(!fetchingStaffDetail && fetchStaffDetailSuccessfull){

            setFirstName(staffDetail.firstName);
            setLastName(staffDetail.lastName);
            setEmailId(staffDetail.email);
            setPhoneNumber(staffDetail.mobile);
            setZoneNumber(staffDetail.zoneNumber);
            setBuildingNumber(staffDetail.buildingNumber);
            setStreetNumber(staffDetail.streetNumber);
            setPassword(staffDetail.password)

			dispatch(StaffActions.stateReset({
				fetchStaffDetailSuccessfull : false
			}))
		}
	},[fetchStaffDetailSuccessfull])



    const handleClickCancel = () => {
		onNavigate(Router, "/staffs");
	};
    
    return(
        <Wrapper>
            <TopHeader/>
            <SideMenu selectedKey={"4"}/>
            <div className="container">
                    <div className="card-root">
                        <ContainerHeader title="Staff"
                            onClickBackButton={() => Router.back()}
                        />
                        <div>
                            <Row gutter={24}>
                                <Col span={12}>
                                    <InputContainer
                                        disabled
                                        title="First Name"
                                        value={firstName}
                                        onChange={(e) => setFirstName(e.target.value)}
                                    />
                                </Col>  
                                <Col span={12}>
                                    <InputContainer
                                        disabled
                                        title="Last Name"
                                        value={lastName}
                                        onChange={(e) => setLastName(e.target.value)}
                                    />
                                </Col>    
                            </Row>    
                            <Row gutter={24}>
                                <Col span={12}>
                                    <InputContainer
                                        disabled
                                        title="E mail"
                                        value={emailId}
                                        onChange={(e) => setEmailId(e.target.value)}
                                    />
                                </Col>  
                                <Col span={12}>
                                    <InputContainer
                                        disabled
                                        title="Mobile Number"
                                        value={phoneNumber}
                                        onChange={(e) => setPhoneNumber(e.target.value)}
                                    />
                                </Col>    
                            </Row>   
                            <Col span={12}>
			                        <InputContainer
                                        
                                        title="password"
                                        value={password}
                                        // onChange={(e) => setPhoneNumber(e.target.value)}
                                    />
                            </Col>    
                            <Col span={24}>
                            <Row>
								<InputContainer
									title="Bulding No."
									placeholder="Bulding No."
									value={buildingNumber}
									onChange={(e) => setBuildingNumber(e.target.value)}
								/>
								<Col>
								<Title level={5} className="title1">Zone Number</Title>
								<Input
									placeholder="Zone No"
									value={zoneNumber}
									onChange={(e) => setZoneNumber(e.target.value)}
									className="customInputBox"
								/>
								</Col>
								<Col>
								<Title level={5} className="title1">Street Number</Title>
								<Input
									placeholder="Street No"
									value={streetNumber}
									onChange={(e) => setStreetNumber(e.target.value)}
									className="customInputBox"
								/>
								</Col>
								</Row>
						    </Col>

                        </div>
                    </div>
                </div>
        </Wrapper>    
    );
};

export default ViewStaff

const Wrapper = styled.div`

background-color:${Themes.backgroundMain};
height:100vh;
width:100vw;
overflow:scroll;
.container{
    margin-left:256px;
    padding: 20px;
    margin-top: 10vh;
    display:flex;
    justify-content:center;
    align-items:center;
    .card-root{
        width:80%;
        background-color: ${Themes.white};
        padding:40px;
    }
}
.create-btn{
    background-color:${Themes.primary};
    border:none;
    margin:20px;
    width:200px;
    height:40px;
    font-weight:bold;
    font-size:16px;
    border-radius:5px;
    color:${Themes.white}
}
.cancel-btn{
    background-color:${Themes.cancelButton};
    border:none;
    margin:20px;
    width:200px;
    height:40px;
    font-weight:bold;
    font-size:16px;
    border-radius:5px;
    color:${Themes.white};
}
.customInputBox{
    border-radius: 5px;
    background-color: #f0f0f0;
    border: none;
    height: 40px;
    width: 90px;
    margin-left:30px;
}	
.title1{
    margin-top:20px;
    margin-left:30px
}
.row{
    display:flex;
    flex-direction:row;
    justify-content:center;
    align-items:center;
}

`;
