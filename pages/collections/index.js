import { useEffect, useState } from "react";
import styled from "styled-components";
import { message, Switch  } from "antd";
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';

import TopHeader from "../../components/TopHeader";
import SideMenu from "../../components/SideMenu";
import ContainerHeader,{FilterOptions} from "../../components/ContainerHeader";
import TableComponent, { UsualActions } from "../../components/Table";
import CenteredModal from "../../components/CenteredModal";

import CollectionActions from "../../redux/actions/collections"
import Themes from "../../themes/themes";
import Router from "next/router";
import { onNavigate } from "../../util/navigation";
import { useDispatch, useSelector } from "react-redux";

const Collections = () => {
	const dispatch = useDispatch()
	const {error, collection, collections, fetchingCollections, fetchCollectionSuccessful,deletingCollection,deleteCollectionSuccessful, updatingCollection, updateCollectionSuccessful,} = useSelector(state => state.collections)

	const [deleteConfirmationModal, setDeleteModalOpen] = useState(false);
	const [deleteId, setDeleteId] = useState("");
	const [searchText, setSearchText] = useState("")
	useEffect(()=>{
		dispatch(CollectionActions.stateReset({
			fetchingCollections : false,
			fetchCollectionSuccessful : false,

			deletingCollection:false,
			deleteCollectionSuccessful : false,

			updatingCollection: false, 
			updateCollectionSuccessful: false,
		}))
	},[])

	useEffect(()=>{
		if(error){
			message.error(error)
			dispatch(CollectionActions.clearError())
		}
	},[error])

	useEffect(()=>{
		var queryParams = ""
		dispatch(CollectionActions.getCollectionRequest(queryParams))
	},[])

	useEffect(()=> {
		if(!fetchingCollections && fetchCollectionSuccessful){
			dispatch(CollectionActions.stateReset({
				fetchCollectionSuccessful : false
			}))
		}
	},[fetchCollectionSuccessful])

	useEffect(()=> {
		if(!deletingCollection && deleteCollectionSuccessful){
			dispatch(CollectionActions.stateReset({
				deleteCollectionSuccessful:false
			}))
			setDeleteModalOpen(false);
			message.success("Collection Deleted successfully");
			dispatch(CollectionActions.getCollectionRequest())
		}
	},[deleteCollectionSuccessful])

	useEffect(()=>{
		var queryParams = searchText ? `?search=${searchText}` : "";
		dispatch(CollectionActions.getCollectionRequest(queryParams))
	},[searchText])

	useEffect(()=>{
		if(!updatingCollection && updateCollectionSuccessful){
			dispatch(CollectionActions.stateReset({
				updateCollectionSuccessful : false
			}))
			message.success("Collection updated successfully");
			dispatch(CollectionActions.getCollectionRequest(""))
		}
	},[updateCollectionSuccessful])

	const handleOpenDeleteModal = (_id) => {
		setDeleteId(_id)
		setDeleteModalOpen(true);
	};

	const handleDeleteCollection = () => {
		dispatch(CollectionActions.deleteCollectionRequest(deleteId))
	};

	const handleCancel = () => {
		setDeleteModalOpen(false);
	};

	const handleNavigation = (path) => {
		return onNavigate(Router, path);
	};

	const handleStatus = (collectionId, name, status) => {
		dispatch(CollectionActions.updateCollectionRequest(collectionId, {name, isEnabled: status}))	
	};

	const handleFeatureStatus = (collectionId, name, status) => {
		dispatch(CollectionActions.updateCollectionRequest(collectionId, {name, isFeatured: status}))	
	};


	const columns = [
		{title: "Web Image", dataIndex: "imageId",key: "imageUrl", 
            render: imageId => (
                <img style={{width: 70, height: 70, borderRadius: 10}} src={imageId.imageUrl}/>
            ) 
        },
		{title: "Name", dataIndex: "name", key: "name"},
		{title: "Priority", dataIndex: "priority", key: "priority"},
        {title: "Feature Collection",
            render: text => {
                if (text.isFeatured) {
					return (<img style={{ width: 50, height: 30, marginLeft: 50, cursor: "pointer" }} src={require("../../Images/toggle-success-button.png")} onClick={() => handleFeatureStatus(text._id, text.name, false)} />)
                } else {
					return (<img style={{ width: 50, height: 30, marginLeft: 50, cursor: "pointer" }} src={require("../../Images/toggle-failure-button.png")} onClick={() => handleFeatureStatus(text._id, text.name, true)} />)
                }
            }
        },
		{title: "Enable (Public View)",
            render: text => {
                if (text.isEnabled) {
                    return ( <img style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer" }} src={require("../../Images/toggle-success-button.png")} onClick={() => handleStatus(text._id, text.name, false)}/>)
                } else {
                    return ( <img style={{width: 50, height: 30, marginLeft: 50, cursor: "pointer"}} src={require("../../Images/toggle-failure-button.png")} onClick={() => handleStatus(text._id, text.name, true)}/>)
                }
            }
        },
		{title: "Action",key: "_id",
			render: (text, record) => (
				<UsualActions 
					onEdit={() => handleNavigation("/collections/edit/" + text._id)}
					// onView={() => handleNavigation("/collections/view/" + text._id)}
					onView={false}
					onDelete={() => handleOpenDeleteModal(text._id)}
				/>
			)
		},
	];

	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"4"} />
			<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=your_api_key_here"></script>

			<div className="container">
				<div className="card-root">
					<ContainerHeader
						title="Collections"
						button="Create"
						onClickButton={() => handleNavigation("/collections/create")}
					/>
					<FilterOptions 
						search	
						inputValue={searchText}
						placeholder={"Search"}
						onChangeInput={(e) => setSearchText(e.target.value)}
					/>
					<TableComponent loading={fetchingCollections} data={collections} columns={columns} />
					<CenteredModal
						visible={deleteConfirmationModal}
						loading={deletingCollection}
						submit={"Delete"}
						cancel="Cancel"
						handleSubmit={handleDeleteCollection}
						handleCancel={handleCancel}
						title="Delete Collection"
					>
						<p>Are you sure you want to delete collection ?</p>
					</CenteredModal>
				</div>
			</div>
		</Wrapper>
	);
};

export default Collections;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow-y: scroll;
	overflow-x: hidden;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
			width: 100%;
			background-color: #fff;
			padding: 20px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
	}
`;
