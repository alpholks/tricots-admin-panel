import { useState, useEffect } from "react";
import styled from "styled-components";
import { Button, message, DatePicker, Space, Select, Row } from "antd";
import moment from "moment"

import TopHeader from "../../../components/TopHeader";
import SideMenu from "../../../components/SideMenu";
import ContainerHeader, { FilterOptions } from "../../../components/ContainerHeader";
import TableComponent, { UsualActions } from "../../../components/Table";
import CenteredModal from "../../../components/CenteredModal";

import LocationActions from "../../../redux/actions/locations"
import Themes from "../../../themes/themes";
import Router, { useRouter } from "next/router";
import { onNavigate } from "../../../util/navigation";
import { useDispatch, useSelector } from "react-redux";

const { RangePicker } = DatePicker;
const { Option } = Select;

const Locations = () => {
	const dispatch = useDispatch()
	const router = useRouter()
	const { error, locationHistory, fetchingLocationHistory, fetchLocationHistorySuccessful } = useSelector(state => state.locations)

	const [locationId, setLocationId] = useState("");
	const [isDownloadModalVisible, setIsDownloadModalVisible] = useState(false);
	const [searchText, setSearchText] = useState("")
	const [date, setDate] = useState()
	const [selectedValue, setSelectedValue] = useState()
	const options = [
		{ name: "All", value: "" },
		{ name: "Scheduled", value: "scheduled" },
		{ name: "In Progress", value: "in_progress" },
		{ name: "Completed", value: "completed" },
		{ name: "Cancelled", value: "cancelled" },
		{ name: "Half Completed", value: "half_completed" },
		{ name: "Replaced", value: "replaced" }
	]
	// const [historyDate,setHistoryDate] = useState("");
	// const [historyTime,setHistoryTime] = useState("");
	// const [historyStaffName,setHistoryStaffName] = useState("");
	// const [status,setStatus] = useState("");


	useEffect(() => {
		dispatch(LocationActions.stateReset({

			fetchingLocationHistory: false,
			fetchLocationHistorySuccessful: false

		}))
	}, [])

	useEffect(() => {
		dispatch(LocationActions.clearError())
	}, [])

	useEffect(() => {
		if (error) {
			message.error(error)
			dispatch(LocationActions.clearError())
		}
	}, [error])

	useEffect(() => {

		let { locationId } = router.query

		console.log(locationId);

		if (locationId) {
			setLocationId(locationId)
		}
		else {
			onNavigate(Router, "locations")
		}

	}, [])

	useEffect(() => {
		var queryParams = ""
		if (locationId) {
			dispatch(LocationActions.getLocationHistoryRequest(locationId, queryParams))
		}
	}, [locationId])

	useEffect(() => {
		if (!fetchingLocationHistory && fetchLocationHistorySuccessful) {
			dispatch(LocationActions.stateReset({
				fetchLocationHistorySuccessful: false
			}))
		}
	}, [fetchLocationHistorySuccessful])

	useEffect(() => {
		var queryParams = "";
		if (searchText) {
			queryParams += `&search=${searchText}`;
		}

		if (date) {
			var startDate = moment(date[0]).format("YYYY-MM-DD");
			var endDate = moment(date[1]).format("YYYY-MM-DD");
			queryParams += `&startDate=${startDate}&endDate=${endDate}`;
		}

		if (selectedValue) {
			queryParams += `&status=${selectedValue}`;
		}

		if (queryParams) {
			queryParams = queryParams.substring(1);
		}

		if (locationId) {
			dispatch(LocationActions.getLocationHistoryRequest(locationId, queryParams))
		}
	}, [searchText, date, selectedValue])


	const columns = [
		{
			title: "Date",
			dataIndex: "createdAt",
			key: "createdAt",
			render: (text, record) => {
				var res = text?.slice(0, 10)
				return (res)
			}
		},
		{
			title: "Time",
			key: "createdAt",
			dataIndex: "createdAt",
			render: (text, record) => {
				var res = text?.slice(11, 16)
				return (res)
			}
		},
		{
			title: "Staff Name",
			key: "staffId",
			dataIndex: "staffId",
			render: (text, record) => (
				<div>{text?.firstName + " " + text?.lastName}</div>
			)
		},
		{
			title: "Status",
			key: "status",
			dataIndex: "status",
		},
	];

	const onDownloadClick = async () => {
		const histories  = locationHistory
		const rows = [
			["Date", "Time", "Staff Name", "Bin Name","Status"]
		]
		await Promise.all(histories?.map(history =>{
			rows.push([moment(history.scheduledAt).format("YYYY-MM-DD"), history.time, history.staffId?.firstName + " " +history.staffId?.lastName,history.binId?.name,history.status])
		}))

		let csvContent = "data:text/csv;charset=utf-8,"
			+ rows.map(e => e.join(",")).join("\n");
		var encodedUri = encodeURI(csvContent);
		var link = document.createElement("a");
		link.setAttribute("href", encodedUri);
		link.setAttribute("download", "location_summary_"+locationId+".csv");
		document.body.appendChild(link); // Required for FF

		link.click(); // This will download the data file named "my_data.csv".
		setIsDownloadModalVisible(false)
	}
	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"3"} />
			<div className="container">
				<div className="card-root">
					<CenteredModal 
						loading={false}
						visible={isDownloadModalVisible}
						handleSubmit={onDownloadClick}
						handleCancel={() =>setIsDownloadModalVisible(false)}
						cancel={"Cancel"}
						submit={"Download"}
						title={"Download confirmation"} >
							Are you want to download the location summary?
						</CenteredModal>
					<ContainerHeader
						title="Location History"
						onClickBackButton={() => Router.back()}
					/>

					<FilterOptions
						datePicker
						select
						search
						options={options}
						onChangeDate={(value) => setDate(value)}
						date={date}
						selectedValue={selectedValue}
						inputValue={searchText}
						selectkey={"name"}
						selectvalue={"value"}
						placeholder={"Search"}
						onChangeSelect={(value) => setSelectedValue(value)}
						onChangeInput={(e) => setSearchText(e.target.value)}
						downloadButton={"Download"}
						onClickDownloadButton={() => setIsDownloadModalVisible(true)}
					/>
					<TableComponent loading={false} data={locationHistory} columns={columns} />
				</div>
			</div>
		</Wrapper>
	);
};

export default Locations;

const Wrapper = styled.div`
	background-color: #e5e5e5;
	height: 100vh;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px;
		margin-top: 10vh;
		.card-root {
			background-color: #fff;
			padding: 20px;
		}
	}
	.history-btn {
		background-color: ${Themes.primary};
		border: none;
		border-radius: 5px;
	}
	.filter-row{
		margin-bottom : 30px;
	}
`;

const data = [
	{
		key: "1",
		date: "11-12-2020",
		time: "11:30",
		customer: "Havis",
		replaceBinName: "BIN 23",
		collectedBinName: "BIN 24",
		staff: "Jarvis",
		status: "Replaced"
	},
	{
		key: "2",
		date: "11-12-2020",
		time: "11:30",
		customer: "Havis",
		replaceBinName: "BIN 23",
		collectedBinName: "BIN 24",
		staff: "Jarvis",
		status: "Replaced"
	},
	{
		key: "3",
		date: "11-12-2020",
		time: "11:30",
		customer: "Havis",
		replaceBinName: "BIN 23",
		collectedBinName: "BIN 24",
		staff: "Jarvis",
		status: "Replaced"
	},
	{
		key: "4",
		date: "11-12-2020",
		time: "11:30",
		customer: "Havis",
		replaceBinName: "BIN 23",
		collectedBinName: "BIN 24",
		staff: "Jarvis",
		status: "Replaced"
	},
	{
		key: "5",
		date: "11-12-2020",
		time: "11:30",
		customer: "Havis",
		replaceBinName: "BIN 23",
		collectedBinName: "BIN 24",
		staff: "Jarvis",
		status: "Replaced"
	},

];
