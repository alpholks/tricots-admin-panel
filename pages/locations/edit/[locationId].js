import { useState, useEffect } from "react";
import styled from "styled-components";
import { message, Row, Col, Button , Input, Typography, DatePicker , Select} from "antd";

import TopHeader from "../../../components/TopHeader";
import SideMenu from "../../../components/SideMenu";
import ContainerHeader from "../../../components/ContainerHeader";
import ViewLocationMap from "../../../components/MapComponent";
import InputContainer from "../../../components/InputContainer";

import CustomerActions from "../../../redux/actions/customers"
import LocationActions from "../../../redux/actions/locations"
import Themes from "../../../themes/themes";
import { onNavigate } from "../../../util/navigation";
import Router,{ useRouter } from "next/router";
import { useDispatch , useSelector} from "react-redux";

const { Title } = Typography;

const { RangePicker } = DatePicker;
const { Option } = Select;

const CreateLocation = () => {
	const router = useRouter()
	const dispatch = useDispatch()

	const CustomerState = useSelector(state => state.customers)
	const {error, locationDetails, updatingLocation, updateLocationSuccessful, fetchLocationDetailsSuccessful, fetchingLocationDetails} = useSelector(state => state.locations)


	const [locationName, setLocationName] = useState("")
	const [locationId, setLocationId] = useState("")
	const [customerId, setCustomerId] = useState("")
	const [binId, setBinId] = useState("")
	const [location, setLocation] = useState({latitude: 0, longitude: 0})
	const [mapView, setMapLoaded] = useState(false)
	const [buildingNumber, setBuildingNumber] = useState("");
	const [zoneNumber, setZoneNumber] = useState("");
	const [streetNumber, setStreetNumber] = useState("");
	// const [startDate, setStartDate] = useState("");
	// const [endDate, setEndDate] = useState("");
	// const [contractDescription,setContractDescription] = useState("");
	const [contactPerson,setContactPerson] = useState("");
	const [contactNumber,setContactNumber] = useState("");
	const [contractStartDate,setContractStartDate] = useState("");
	const [contractEndDate,setContractEndDate] = useState("");
	const [salesRepresentative,setSalesRepresentative] = useState("")
	const [frequency,setFrequency] = useState("");
	const [customFrequency,setCustomFrequency] = useState("");


	useEffect(()=>{
		dispatch(CustomerActions.stateReset({
			fetchingCustomers : false,
			fetchCustomerSuccessful : false,
		}))
		dispatch(LocationActions.stateReset({
			updatingLocation:false,
			updateLocationSuccessful : false,

			fetchLocationDetailsSuccessful: false,
			fetchingLocationDetails: false
		}))
		let {locationId} = router.query
		if(locationId){
			setLocationId(locationId)
		}else {
			onNavigate(Router, "locations")
		}
	},[])

	useEffect(()=>{
		dispatch(CustomerActions.clearError())
	},[])

	useEffect(()=>{
		if(CustomerState.error || error){
			message.error(error)
			dispatch(CustomerActions.clearError())
		}
	},[CustomerState.error, error])

	useEffect(()=>{
		if(locationId){
		dispatch(CustomerActions.getCustomerRequest())
		console.log(locationId)
		dispatch(LocationActions.getLocationDetailsRequest(locationId))
	}
	},[locationId])

	useEffect(()=> {
		if(!CustomerState.fetchingCustomers && CustomerState.fetchCustomerSuccessful){
			dispatch(CustomerActions.stateReset({
				fetchCustomerSuccessful : false
			}))
		}
	},[CustomerState.fetchCustomerSuccessful])

	useEffect(()=> {
		if(!fetchingLocationDetails && fetchLocationDetailsSuccessful){
			dispatch(LocationActions.stateReset({
				fetchLocationDetailsSuccessful : false
			}))
			if (locationDetails.frequency == "daily" || locationDetails.frequency == "alternative" || locationDetails.frequency == "weeklytwice" || locationDetails.frequency == "weeklyonce" || locationDetails.frequency == "weeklythrice" || locationDetails.frequency == "monthlyonce" || locationDetails.frequency == "onetime" ){
			
				setLocationName(locationDetails.name)
				setLocation({latitude: locationDetails.latitude, longitude: locationDetails.longitude})
				setCustomerId(locationDetails.customerId._id)
				setMapLoaded(true)
				setContactPerson(locationDetails.contactPerson)
				setContactNumber(locationDetails.contactPerson)
				setContractStartDate(locationDetails.contractStartDate)
				setContractEndDate(locationDetails.contractEndDate)
				setFrequency(locationDetails.frequency)
				setCustomFrequency(locationDetails.frequency)
				setSalesRepresentative(locationDetails.salesRepresentative)
				setBuildingNumber(locationDetails.buildingNumber)
				setZoneNumber(locationDetails.zoneNumber)
				setStreetNumber(locationDetails.streetNumber)
				// setContractDescription(locationDetails.contractDescription)
			
			
			}		
			else{

				setLocationName(locationDetails.name)
				setLocation({latitude: locationDetails.latitude, longitude: locationDetails.longitude})
				setCustomerId(locationDetails.customerId._id)
				setMapLoaded(true)
				setContactPerson(locationDetails.contactPerson)
				setContactNumber(locationDetails.contactPerson)
				setContractStartDate(locationDetails.contractStartDate)
				setContractEndDate(locationDetails.contractEndDate)
				setFrequency("custom")
				setCustomFrequency(locationDetails.frequency)
				setSalesRepresentative(locationDetails.salesRepresentative)
				setBuildingNumber(locationDetails.buildingNumber)
				setZoneNumber(locationDetails.zoneNumber)
				setStreetNumber(locationDetails.streetNumber)
				// setContractDescription(locationDetails.contractDescription)

			}
			


			// setBinId(locationDetails.binId._id)
		}
	},[fetchLocationDetailsSuccessful])

	useEffect(()=> {
		if(!updatingLocation && updateLocationSuccessful){
			dispatch(LocationActions.stateReset({
				updateLocationSuccessful : false
			}))
			message.success("location updated successfully");
		}
	},[updateLocationSuccessful])

	const handleClickCancel = () => {
		onNavigate(Router, "/locations");
	};
	const handleClickUpdate = () => {
		console.log(locationName,customerId, location.latitude,location.longitude)
		
		if(locationName == ""|| customerId == ""){
			return message.warn("Please fill all the fields")
		}
		if(frequency=="custom"){
			dispatch(LocationActions.updateLocationRequest(locationId,{
				name : locationName,
				customerId: customerId,
				contactPerson,
				contactNumber,
				contractStartDate,
				contractEndDate,
				salesRepresentative,
				frequency:customFrequency,
				buildingNumber,
				zoneNumber,
				streetNumber,
				latitude : location.latitude,
				longitude : location.longitude,
			}))
		}
		else{
			dispatch(LocationActions.updateLocationRequest(locationId,{
				name : locationName,
				customerId: customerId,
				contractStartDate,
				contractEndDate,
				contactPerson,
				contactNumber,
				frequency,			
				salesRepresentative,	
				buildingNumber,
				zoneNumber,
				streetNumber,
				latitude : location.latitude,
				longitude : location.longitude,
			}))
		}
		
		// dispatch(LocationActions.updateLocationRequest(locationId, {
		// 	name : locationName,
		// 	customerId: customerId,
		// 	// startDate,
		// 	// endDate,
		// 	buildingNumber,
		// 	contractDescription,
		// 	zoneNumber,
		// 	streetNumber,
		// 	latitude : location.latitude,
		// 	longitude : location.longitude,
		// 	binId: "5fd911572c5d95437c36eedd"
		// }))
	};

	// const location = {
	// 	firstName: "Madhan",
	// 	lastName: "Kumar",
	// 	emailId: "madhandeveloper20@gmail.com",
	// 	phoneNumber: "+91 8072827934",
	// 	address: "2/287.136, Gujarathi St, Kozhikode, Kerala",
	// };
	// useEffect(() => {
	// 	setFirstName(customer.firstName);
	// 	setLastName(location.lastName);
	// 	setEmailId(location.emailId);
	// 	setPhoneNumber(location.phoneNumber);
	// 	setAddress(location.address);
	// }, []);
	return (
		<Wrapper>
			<TopHeader />
			<SideMenu selectedKey={"3"} />
			<div className="container">
				<div className="card-root">
					<ContainerHeader
						title="Location Name"
						onClickBackButton={() => Router.back()}
						// input="Location 1"
						// inputValue={locationName}
						// onChangeInput={setLocationName}
						// optionsArrayOfObjects={true}
						// options={CustomerState.customers}
						// defaultSelected={customerId}
						// selectkey={"firstName"}
						// selectvalue={"_id"}
						// onChangeSelect={setCustomerId}
					/>
					<Row justify="space-evenly" className="filter-row" >
						<Select  placeholder="Select Customer" style={{ width: 180 }} value={customerId} onChange={(value) =>setCustomerId(value)}>
							{CustomerState.customers.map(customer => {
								return <Option value={customer._id}>{customer.firstName}</Option>
							})}
						</Select>
						
						<Input style={{ width: 180, marginLeft : 20 }}  placeholder="Location name" value={locationName} onChange={e=> setLocationName(e.target.value)} />
					</Row>
					<Row gutter={24}>
					<Col span={12}>
						<InputContainer
									title="Contract Person"
									value={contactPerson}
									onChange={(e) => setContactPerson(e.target.value)}
						/>
						<InputContainer
									title="Contract Start Date"
									value={contractStartDate}
									onChange={(e) => setContractStartDate(e.target.value)}
						/>
						<InputContainer
									title="Sales Representative"
									value={salesRepresentative}
									onChange={(e) => setSalesRepresentative(e.target.value)}
						/>
						<div className={frequency=="custom"?"":"hide"}>
						<InputContainer
									title="Custom"
									value={customFrequency}
									onChange={(e) => setCustomFrequency(e.target.value)}
						/>
						</div>
					</Col>
					<Col span={12}>
						<InputContainer
									title="Contact Number"
									value={contactNumber}
									onChange={(e) => setContactNumber(e.target.value)}
						/>
						<InputContainer
									title="Contract End Date"
									value={contractEndDate}
									onChange={(e) => setContractEndDate(e.target.value)}
						/>
						<Title level={5} className="title">Frequency</Title>
								<Select
                                      placeholder=""
                                      className="select"
                                      value={frequency}
                                      onChange={(value) => setFrequency(value)}
                                      style={{ width: '100%',backgroundColor:"#f0f0f0"}}
                                >
                                        <Option value="daily">Daily</Option>
                                        <Option value="altenative">Alternatively</Option>
										<Option value="weeklyonce">Weekly Once</Option>
										<Option value="weeklytwice">Weekly Twice</Option>
										<Option value="weeklythrice">Weekly Thrice</Option>
										<Option value="monthlyonce">Monthly Once</Option>
										<Option value="onetime">One Time</Option>
										<Option value="custom">Custom</Option>
                                </Select>

					</Col>
					<Col span={24}>
					<Row>
								<InputContainer
									title="Bulding No."
									placeholder="Bulding No."
									value={buildingNumber}
									onChange={(e) => setBuildingNumber(e.target.value)}
								/>
								<Col>
								<Title level={5} className="title1">Zone Number</Title>
								<Input
									placeholder="Zone No"
									value={zoneNumber}
									onChange={(e) => setZoneNumber(e.target.value)}
									className="customInputBox"
								/>
								</Col>
								<Col>
								<Title level={5} className="title1">Street Number</Title>
								<Input
									placeholder="Street No"
									value={streetNumber}
									onChange={(e) => setStreetNumber(e.target.value)}
									className="customInputBox"
								/>
								</Col>
								</Row>
								</Col>
					
					</Row	>
					<Col span={24}>
					{mapView &&
							<ViewLocationMap  
							onClickMap={async ({latitude, longitude}) => await setLocation({latitude : latitude, longitude : longitude})}
							initialCenter={{lat: location.latitude, lng: location.longitude}} 
							markers={[{name : 1, position : {lat: location.latitude, lng: location.longitude}}]}
							style={{height : 450, marginTop:30}}
							/>
							}						
					</Col>
					<div className="row">
						<Button className="cancel-btn" onClick={handleClickCancel}>
							Cancel
						</Button>
						<Button loading={updatingLocation} className="create-btn" onClick={handleClickUpdate}>
							Save
						</Button>
					</div>
				</div>
				
			</div>
		</Wrapper>
	);
};

export default CreateLocation;

const Wrapper = styled.div`
	background-color: ${Themes.backgroundMain};
	height: 100vh;
	width: 100vw;
	overflow: scroll;
	.container {
		margin-left: 256px;
		padding: 20px 20px 20px 20px;
		margin-top: 10vh;
		display: flex;
		justify-content: center;
		align-items: center;
		height : 115%;

		.card-root {
			width: 80%;
			height : 100%;
			background-color: ${Themes.white};
			padding: 40px 40px 40px 40px;
		}
	}
	.create-btn {
		background-color: ${Themes.primary};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.cancel-btn {
		background-color: ${Themes.cancelButton};
		border: none;
		margin: 20px;
		width: 200px;
		height: 40px;
		font-weight: bold;
		font-size: 16px;
		border-radius: 5px;
		color: ${Themes.white};
	}
	.row {
		height:350px;
		display: flex;
		flex-direction: row;
		justify-content: center;
		align-items: center;
	}
	.customInputBox{
		border-radius: 5px;
    	background-color: #f0f0f0;
    	border: none;
		height: 40px;
		width: 90px;
		margin-left:30px;
	}
	.title1{
        margin-top:20px;
        margin-left:30px;
	}
	.hide{
		display:none
	}
`;
