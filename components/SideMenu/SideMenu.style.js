import styled from "styled-components";
import Themes from "../../themes/themes";

const Wrapper = styled.section`
	height: 100vh;
	position: fixed;
	margin-top: 10vh;
	z-index: 1;
	.ant-menu:not(.ant-menu-horizontal) .ant-menu-item-selected {
		background-color: ${Themes.primary};
		color: #fff;
		border: none;
		font-size: 18px;
	}

	.ant-menu-item-active {
		color: #000;
	}
	.ant-menu-item {
		font-size: 18px;
	}
	.ant-menu-vertical .ant-menu-item::after,
	.ant-menu-vertical-left .ant-menu-item::after,
	.ant-menu-vertical-right .ant-menu-item::after,
	.ant-menu-inline .ant-menu-item::after {
		border: none;
	}
	.ant-menu-item {
		justify-content: center;
	}
`;

export default Wrapper;
