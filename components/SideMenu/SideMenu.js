import React from "react";
import Wrapper from "./SideMenu.style";
import { Menu } from "antd";
import { onNavigate } from "../../util/navigation";
import Router from "next/router";

const TopHeader = (props) => {
	const { selectedKey } = props;
	return (
		<Wrapper>
			<Menu
				style={{ width: 256, height: "100%" }}
				defaultSelectedKeys={[selectedKey]}
				mode="inline"
			>
				{routes.map((route, index) => {
					return (
						<Menu.Item
							onClick={() => {
								onNavigate(Router, route.path);
							}}
							key={index + 1}
						>
							{route.name}
						</Menu.Item>
					);
				})}
			</Menu>
		</Wrapper>
	);
};

export default TopHeader;

const routes = [
	{
		path: "/dashboard",
		name: "Dashboard",
	},
	{
		path: "/banners",
		name: "Banners",
	},
	// {
	// 	path: "/categories",
	// 	name: "Categories",
	// },
	{
		path: "/charts",
		name: "Charts",
	},
	{
		path: "/collections",
		name: "Collections",
	},
	{
		path: "/combos",
		name: "Combos",
	},
	{
		path: "/customers",
		name: "Customers",
	},
	{
		path: "/feedbacks",
		name: "Feedback",
	},
	// {
	// 	path: "/gallery",
	// 	name: "Gallery",
	// },
	{
		path: "/headers",
		name: "Headers",
	},
	{
		path: "/orders",
		name: "Orders",
	},
	{
		path: "/products",
		name: "Products",
	},
	{
		path: "/promocodes",
		name: "Promocodes",
	},
	{
		path: "/profile",
		name: "Profile",
	},
	{
		path: "/ratings",
		name: "Ratings",
	},
	{
		path: "/subscribes",
		name: "Subscribes",
	},
];
