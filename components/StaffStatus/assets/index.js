const Images = {
    cancelled:require("./images/cancelledcross.png"),
    completed:require("./images/completedtick.png"),
    inprogress:require("./images/yellowtick.png"),
    scheduled:require("./images/notstartedtick.png")
}
export { Images }

const ComponentAssets = {
	Images
}
export default ComponentAssets
