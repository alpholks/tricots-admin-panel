import React from "react";
import { Button, Modal } from "antd";

import Wrapper from "./CenteredModal.style";

const InputContainer = (props) => {
	const {
		loading,
		visible,
		handleSubmit,
		handleCancel,
		cancel,
		submit,
		children,
		title,
	} = props;
	return (
		<Wrapper>
			<Modal
				visible={visible}
				title={title}
				onOk={handleSubmit}
				onCancel={handleCancel}
				cancelButtonProps={{ style: { display: "none" } }}
				footer={[
					<Button key="back" onClick={handleCancel}>
						{cancel ? cancel : "Cancel"}
					</Button>,
					<Button
						key="submit"
						type="primary"
						loading={loading}
						onClick={handleSubmit}
					>
						{submit ? submit : "Ok"}
					</Button>,
				]}
			>
				{children}
				{/* <Button key="back" className="cancel-btn" onClick={handleCancel}>
					{cancel ? cancel : "Cancel"}
				</Button>
				<Button
					key="submit"
					className="ok-btn"
					loading={loading}
					onClick={handleSubmit}
				>
					{submit ? submit : "Ok"}
				</Button> */}
			</Modal>
		</Wrapper>
	);
};

export default InputContainer;
