import styled from "styled-components";
import Themes from "../../themes/themes";

const Wrapper = styled.section`
	width: 100%;
	.action-icons {
		cursor: pointer;
	}
	.ant-table-thead .ant-table-cell {
		background-color: ${Themes.primary};
		color: ${Themes.white};
		font-size: 16px;
		font-weight: bold;
	}
`;

export default Wrapper;
