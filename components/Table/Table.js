import React from "react";
import Wrapper from "./Table.style";
import { Table, Space } from "antd";

import { Images } from "./assets/index";

const TableComponent = (props) => {
	const { columns, data, loading } = props;
	return (
		<Wrapper>
			<Table loading={loading} columns={columns} dataSource={data?.length != 0 ? data : []} pagination={{ pageSize: 10 }} />
		</Wrapper>
	);
};

export const UsualActions = ({ onEdit, onDelete, onView }) => {
	return (
		<Space size="middle">
			{ onEdit == false ? null : <img className="action-icons" style={{width: 30, height: 30, }} src={Images.edit} onClick={onEdit} /> }
			{ onView == false ? null : <img className="action-icons" style={{width: 30, height: 30, }} src={Images.view} onClick={onView} /> }
			{ onDelete == false ? null : <img className="action-icons" style={{width: 30, height: 30, }} src={Images.delete} onClick={onDelete} /> }
		</Space>
	);
};

export const ViewAction = ({onView})=>{
	return(
		<Space size="middle">
			<img className="action-icons" src={Images.view} onClick={onView} />
		</Space>
	)
};

export const TaskActions = ({ onEdit, onDelete, onCopy }) => {
	return (
		<Space size="middle">
			{/* <img className="action-icons" src={Images.edit} onClick={onEdit} /> */}
			<img className="action-icons" src={Images.copy} onClick={onCopy} />
			{/* <img className="action-icons" src={Images.delete} onClick={onDelete} /> */}
		</Space>
	);
};

export default TableComponent;
