import styled from "styled-components";
import Themes from "../../themes/themes";

const Wrapper = styled.section`
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	margin-bottom : 20px;
	.card-header-actions {
		display: flex;
		flex-direction: row;
		justify-content: space-between;
		width : 100%;
		.search-box {
			width : 200px;
			height: 40px;
			margin-right: 30px;
		}
		.create-btn {
			background-color: ${Themes.primary};
			border: none;
			border-radius: 5px;
		}
		.input-box {
		border-radius: 5px;
		/* background-color: ${Themes.inputBackground}; */
		/* border: none; */
		height: 40px;
		width: 200px;
	}
		/* .select {
			margin-right: 30px;
			width: 300px;
			height: 40px;
		} */
	}
	.ant-select-selector {
		background-color: ${Themes.card};
		border : 1px solid ${Themes.borderColor};
	}
	.ant-select-single:not(.ant-select-customize-input) .ant-select-selector {
		height : 40px;
		display : flex;
		align-items : center;
		justify-content : space-between;
		flex-direction : row;
		color : #000;
		width : 100%;
		padding-right : 30px;
	}
	.ant-select-single.ant-select-lg:not(.ant-select-customize-input) .ant-select-selector {
		/* align-items: center;
		justify-content: center; */
		
	}	

	.ant-select-arrow {
		color : ${Themes.primary};
		/* left : 100px; */
		font-size : 13px;
	}

`;


export const FilterWrapper = styled.section`
display:flex;
flex-direction : row;
width: 100%;
justify-content:space-between;
margin-top: 0px;
margin-bottom: 30px;
.search-box{
	width : 300px;
	margin-right : 30px;
}
.select{
	width : 300px;
	margin-right : 30px;
}
.time-picker {
	margin-right : 30px;
}
.create-btn{
	background-color: ${Themes.primary};
	border: none;
	border-radius: 5px;
}

`;
export default Wrapper;
