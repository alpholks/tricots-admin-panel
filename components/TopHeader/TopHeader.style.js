import styled from "styled-components";
import Themes from "../../themes/themes";

const Wrapper = styled.section`
	padding: 10px 40px;
	position: fixed;
	z-index: 5;
	width: 100vw;
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	background-color: ${Themes.white};

	.logoImage {
		width: 100px;
		height: 50px;
		// object-fit: contain;
	}
	.rightContainer {
		display: flex;
		flex-direction: row;
		align-items: center;
	}
	.brand {
		font-size: 20px;
		margin-left: 10px;
		margin-top: 7px;
		display: inline;
	}
	.username {
		font-size: 18px;
		padding-right: 20px;
		
	}
	.userImageContainer {
		width: 50px;
		height: 50px;
		border-radius: 100px;
		background-color: #800000;
		display: flex;
		justify-content: center;
		align-items: center;
		cursor:pointer;
	}
	.userLogo {
		color: #fff;
		font-size: 20px;
	}
`;

export default Wrapper;
