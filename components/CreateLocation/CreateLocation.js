import React from "react";
import Wrapper from "./CreateLocation.style";
import { GoogleApiWrapper, Map, InfoWindow, Marker } from "google-maps-react";
import { Images } from "./assets/index";

// ...

export class MapContainer extends React.Component {
	render() {
		return (
			<Wrapper>
				<Map
					google={this.props.google}
					zoom={14}
					streetView={false}
					streetViewControlOptions={false}
					draggable={true}
					streetViewControl={false}
					initialCenter={{lat: 37.759703, lng: -122.428093}}
					style={{ ...this.props.style}}
				>
					
					<Marker
						name={"Dolores park"}
						position={{ lat: 37.759703, lng: -122.428093 }}
						icon={{
							url: Images.marker,
							anchor: new google.maps.Point(32, 32),
							scaledSize: new google.maps.Size(30, 30),
						}}
					/>
				</Map>
			</Wrapper>
		);
	}
}

export default GoogleApiWrapper({
	apiKey: "AIzaSyArlFk-HOh3cE5xgZf2CoS7qlgcIPZMiY4",
})(MapContainer);
