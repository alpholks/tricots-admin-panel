import styled from 'styled-components'
import UGTheme from '../../themes/UGTheme'

const Wrapper = styled.section`
	width: 450px;
	padding : 20px;
	.heading {
		font-weight : 600;
	}
    .icon{
        color : ${UGTheme.primary};
    }
	.profile-wrapper {
		width: 60px;
		height: 60px;
		border-radius: 30px;
		overflow: hidden;
		img {
			width: 100%;
			height: 100%;
			object-fit: cover;
		}
	}
	.user-details {
		width : 100%;
		margin-left: 20px;
		flex: 1;
		.name {
			font-size: 16px;
			/* font-family: "Roboto-Regular"; */
			color: ${UGTheme.white};
		}
		.designation {
			font-size: 16px;
			/* font-family: "Roboto-Regular"; */
			color: ${UGTheme.white};
		}
	}
		.heading {
		color: ${UGTheme.white};
		/* font-family: "Roboto-Medium";   */
		margin-bottom: 20px;
		font-size:medium;
		font-weight : bold;
		
	}
	.status {
		p {
			color: ${UGTheme.green};
			font-size: 16px;
			/* font-family: "Roboto-Regular"; */
			margin: 0;
			text-align: right;
		}
	}

        .Address1{
            color :${UGTheme.white};
            display:flex;
            justify-content : center;
            margin-top : 30px;
        }
           .Address2{
            color :${UGTheme.white};
            display:flex;
            justify-content : center;
        }
	.image-scroller {
		display: flex;
		flex-direction: row;
		justify-content: space-between;
		align-items: center;
        .FullWidth{
            width : 100%;
        }
		.forwardIcon {
			width: 40px;
			height: 40px;
			border-radius: 40px;
			background-color: ${UGTheme.backgroundMain};
			display: flex;
			align-items: center;
			justify-content: center;
		}
	}


	.description {
		padding-bottom: 20px;
		font-size: 14px;
		/* font-family: "Roboto-Regular"; */
		color: ${UGTheme.white};
		text-align: justify;
		text-justify: inter-word;
		/* margin-top : 20px; */
	}
	.description2 {
		display: flex;
		flex-direction: row;
		justify-content: space-between;
		border-top: 1px solid rgba(0, 0, 0, 0.2);
		margin-top: 20px;
		padding: 20px 0;
		font-size: 14px;
		/* font-family: "Roboto-Regular"; */
		color: ${UGTheme.white};
		.rightAlign {
			align-items: center;
		}
	}

	.status-order-wrapper {
		padding: 20px;
		padding-bottom: 0px;
		background: ${UGTheme.backgroundMain};
	}
	.sub-heading {
		color: ${UGTheme.primary};
		font-size: 14px;
		/* font-family: "Roboto-Regular"; */
		margin-bottom: 10px;
		cursor: pointer;
	}
	.status-order-row {
		display: flex;
		position: relative;
		padding-bottom: 20px;
		.status-date-time {
			font-size: 12px;
			color: ${UGTheme.white};
			text-align: right;
			width: 100px;
			margin-top: 10px;
			font-weight : 100;
			p {
				margin: 0;
			}
		}
		&:nth-last-of-type(1) {
			.status-icon:before {
				border-left: none;
			}
		}
	}
	.status-icon {
		flex: none;
		width: 45px;
		height: 45px;
		border-radius: 100px;
		border: 2px solid ${UGTheme.white};
		margin: 0 20px;
		background: #fff;
		display: flex;
		align-items: center;
		justify-content: center;
		img {
			width: 25px;
			position: relative;
		}
		&:before {
			content: '';
			border-left: 1px dashed ${UGTheme.white};
			height: calc(100% - 50px);
			position: absolute;
			bottom: -1px;
		}
	}
	.status-state {
		color: ${UGTheme.white};
		font-size: 14px;
		/* font-family: 'Roboto-Medium'; */
		font-weight : bold;
	}
	.status-desc {
		color: ${UGTheme.white};
		font-size: smaller;
		/* font-family: 'Roboto-Regular'; */
	}
	.download-link {
		color: ${UGTheme.primary};
		width : 50%;
		font-size: 13px;
		/* font-family: 'Roboto-Regular'; */
		cursor: pointer;
	}
	.quotations-btn {
		width: 40%;
		background: ${UGTheme.primary};
		border-radius: 4px;
		border: none;
		color: #fff;
		height : 30px;
		cursor: pointer;
	}
	.horizontal-line {
		border-bottom: 1px solid rgba(0, 0, 0, 0.2);
		margin: 0 -44px;
	}
	.ant-select-selector {
		border: 1px solid ${UGTheme.borderColor};
		background: ${UGTheme.backgroundMain};
	}
	.ant-select-selection-item {
		border: 1px solid ${UGTheme.borderColor};
		background:${UGTheme.backgroundMain};
	}
	.ant-select-single:not(.ant-select-customize-input) .ant-select-selector {
		border: 1px solid ${UGTheme.borderColor};
		background:${UGTheme.backgroundMain};
	}
	.ant-select-arrow{
    color : ${UGTheme.white}
}
	.ant-picker {
		background-color: ${UGTheme.backgroundMain};
		color: ${UGTheme.white};
		border: 1px solid ${UGTheme.borderColor};
		.ant-picker-focused {
			color: ${UGTheme.white};
		}
	}
	.ant-picker-input {
		input {
			color: ${UGTheme.white};
		}
	}
	.form-group {
		display: flex;
		margin: 20px 0;
		align-items: center;
		label {
			flex: none;
			font-size: 16px;
			color: #fff;
			margin-right: 30px;
			width: 100px;
		}
		.ant-select {
			flex: 1;
		}
		textarea.ant-input {
			background: ${UGTheme.backgroundMain};
			resize: none;
			padding : 10px;
		}
	}
	.saveBtn {
		margin-left: 130px;
		background: ${UGTheme.primary};
		width: 100px;
		height: 40px;
		border-radius: 4px;
		border: none;
		color: #fff;
	}
	.ant-input::placeholder {
		/* Chrome, Firefox, Opera, Safari 10.1+ */
		color: ${UGTheme.inputPlaceholder};
		opacity: 1; /* Firefox */
		font-size: smaller;
	}
	.ant-input:-ms-input-placeholder {
		/* Internet Explorer 10-11 */
		color: ${UGTheme.inputPlaceholder};
		opacity: 1; /* Firefox */
		font-size: smaller;
	}
	.ant-input::-ms-input-placeholder {
		/* Microsoft Edge */
		color: ${UGTheme.inputPlaceholder};
		opacity: 1; /* Firefox */
		font-size: smaller;
	}
	.ant-select-selection-placeholder{
			color: ${UGTheme.inputPlaceholder};
		opacity: 1; /* Firefox */
		font-size: smaller;
	}
	.rowStyle {
		justifyContent: 'space-between',
		marginTop: 50,
		alignItems: 'center'
	}
`

export default Wrapper
