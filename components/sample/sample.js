import PropTypes from 'prop-types'
import React from 'react'
import Wrapper from './ActiveWorkPopup.style'
import { Images } from './assets/index'
import { ArrowLeftOutlined, ArrowRightOutlined, CloseOutlined } from '@ant-design/icons'

// const { TextArea } = Input
export default function ActiveWorkPopup (props) {
  ActiveWorkPopup.propTypes = {
    communityWOVisible: PropTypes.any,
    setFirstImage: PropTypes.any,
    firstImage: PropTypes.any,
    setQuotationsVisible: PropTypes.any,
    setForm: PropTypes.any,
    setWorkOrderStatusVisible: PropTypes.any
  }

  const {
    communityWOVisible,
    setFirstImage,
    firstImage,
    setQuotationsVisible,
    setForm,
    setWorkOrderStatusVisible
  } = props
  return (
		<Wrapper>
			{communityWOVisible === 'Inspection' && (
				<React.Fragment>
					<div className="heading">Inspection #2222</div>
					<div className="row">
						<div className="profile-wrapper">
							<img src="https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-4-300x300.png" />
						</div>
						<div className="user-details">
							<div className="name">Fathima Ansar Ali</div>
							<div className="designation">RWA</div>
						</div>
						<div className="status">
							<p>Current Status</p>
							<p>Scheduled on 09 Jun 2020</p>
						</div>
					</div>
					<div className="description2">
						<p>Work order Title</p>
						<p>Entitity 1 out 0f 2</p>
					</div>
					<div className="image-scroller">
						<div className="forwardIcon" onClick={() => setFirstImage(!firstImage)}>
							{/* <img src={Images.backword} /> */}
							<ArrowLeftOutlined className="icon" />
						</div>
						<div className="image-container">
							<img src={firstImage ? Images.complaintPic : Images.PatrolImage} width={300} height={200} />
						</div>
						<div className="forwardIcon" onClick={() => setFirstImage(!firstImage)}>
							{/* <img src={Images.forward} width={30} /> */}
							<ArrowRightOutlined className="icon" />
						</div>
					</div>

					<div className="Address1"> Corridor Gate, Floor #2, Near Cooler,</div>
					<div className="Address2">Golden Height, GH Appartments</div>
					<div className="row rowStyle" >
						<button className="quotations-btn" onClick={() => setQuotationsVisible(true)}>
							Cancel WO
						</button>
						<button className="quotations-btn" onClick={() => setForm(true)}>
							Form
						</button>
					</div>
				</React.Fragment>
			)}
			{communityWOVisible === 'PM' && (
				<React.Fragment>
					<div className="heading">PM #2222</div>
					<div className="row">
						<div className="profile-wrapper">
							<img src="https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-4-300x300.png" />
						</div>
						<div className="user-details">
							<div className="name">Fathima Ansar Ali</div>
							<div className="designation">RWA</div>
						</div>
						<div className="status">
							<p>Current Status</p>
							<p>Scheduled on 09 Jun 2020</p>
						</div>
					</div>
					<div className="description2">
						<p>Work order Title</p>
					</div>
					<div className="image-scroller">
						<img className={'FullWidth'} src={Images.complaintPic} />
					</div>
					<div className="Address1"> Corridor Gate, Floor #2, Near Cooler,</div>
					<div className="Address2">Golden Height, GH Appartments</div>
					<div className="row rowStyle" >
						<button className="quotations-btn" onClick={() => setQuotationsVisible(true)}>
							Cancel WO
						</button>
						<button className="quotations-btn" onClick={() => setForm(true)}>
							Form
						</button>
					</div>
				</React.Fragment>
			)}
			{(communityWOVisible === 'FMR' || communityWOVisible === 'FMC') && (
				<React.Fragment>

					<div className="description">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
						labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
						laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
						voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
						non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</div>
					<div className="status-order-wrapper">
						<div className="sub-heading" onClick={() => setWorkOrderStatusVisible(true)}>
							Change status of Work Order
						</div>
						<div className="status-order-row">
							<div className="status-date-time">
								<p>7 Jun 2020</p>
								<p>04:30 pm</p>
							</div>
							<div className="status-icon"><img src={Images.status1} /></div>
							<div className="status-details">
								<div className="status-state">Assigned</div>
								<div className="status-desc">
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, s
								</div>
							</div>
						</div>
						<div className="status-order-row">
							<div className="status-date-time">
								<p>8 Jun 2020</p>
								<p>05:30 pm</p>
							</div>
							<div className="status-icon"><img src={Images.status2} /></div>
							<div className="status-details">
								<div className="status-state">Started</div>
								<div className="status-desc">
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, s
								</div>
							</div>
						</div>
						<div className="status-order-row">
							<div className="status-date-time">
								<p>9 Jun 2020</p>
								<p>06:30 pm</p>
							</div>
							<div className="status-icon"><img src={Images.status3} /></div>
							<div className="status-details">
								<div className="status-state">Ongoing</div>
								<div className="status-desc">
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, s
								</div>
							</div>
						</div>
					</div>
					<div
						className="row"
						style={{ justifyContent: 'space-between', marginTop: 35, alignItems: 'center' }}
					>
						<div className="download-link">Download Attachments ( 5 )</div>
						<button className="quotations-btn" onClick={() => setQuotationsVisible(true)}>
							Quotations
						</button>
					</div>
				</React.Fragment>
			)}
		</Wrapper>
  )
}

export const IconClose = () => {
  return (
		<Wrapper>
			<CloseOutlined className="icon" />
		</Wrapper>
  )
}
