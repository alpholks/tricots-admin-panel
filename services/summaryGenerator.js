// services/reportGenerator.js

import jsPDF from "jspdf";
import "jspdf-autotable";

import { format } from "date-fns";


const summaryGenerator = data => {

  const doc = new jsPDF();

  const tableColumn = ["Date", "Start Time", "End Time", "Status", ];
  
  const tableRows = [];

  data.forEach(ticket => {
    const ticketData = [
      ticket.scheduledAt,
      ticket.startTime,
      ticket.endTime,
      ticket.status,
    //   format(new Date(ticket.updated_at), "yyyy-MM-dd")
    ];
    tableRows.push(ticketData);
  });


  doc.autoTable(tableColumn, tableRows, { startY: 20 });
  const date = Date().split(" ");
  const dateStr = date[0] + date[1] + date[2] + date[3] + date[4];
  doc.text("Staff History", 14, 15);
  doc.save(`report_${dateStr}.pdf`);
};

export default summaryGenerator;