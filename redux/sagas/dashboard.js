import { call, select, put } from "redux-saga/effects";
import request from "../../util/request";

import { API_URL } from "../../constants";
import {
	getDashboardSuccess,
	getDashboardFailure,

	getDashboardLocationFailure,
	getDashboardLocationSuccess,

} from "../actions/dashboard";

export function * dashboardGet({queryParams}) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/dashboard" + queryParams, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getDashboardSuccess(response.data));
		} else {
			yield put(getDashboardFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getDashboardFailure(error.toString()));
	}
}

export function * dashboardLocationsGet() {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/dashboard/locations", {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getDashboardLocationSuccess(response.data));
		} else {
			yield put(getDashboardLocationFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getDashboardLocationFailure(error.toString()));
	}
}

const DashboardSaga = {
	dashboardGet,
	dashboardLocationsGet
};

export default DashboardSaga;
