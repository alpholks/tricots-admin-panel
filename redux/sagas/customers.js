import { call, select, put } from "redux-saga/effects";
import request from "../../util/request";

import { API_URL } from "../../constants";
import {
	getCustomerSuccess,
	getCustomerFailure,

	createCustomerSuccess,
	createCustomerFailure,

	updateCustomerSuccess,
	updateCustomerFailure,
	
	deleteCustomerSuccess, 
	deleteCustomerFailure, 
	
	getCustomerDetailsSuccess, 
	getCustomerDetailsFailure,

	getCustomerHistorySuccess,
	getCustomerHistoryFailure
} from "../actions/customers";

export function * customersGet({queryParams}) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + `/admin/customers${queryParams}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getCustomerSuccess(response.data));
		} else {
			yield put(getCustomerFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getCustomerFailure(error.toString()));
	}
}

export function * customerDetailsGet({ customerId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/customers/" + customerId, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getCustomerDetailsSuccess(response.data));
		} else {
			yield put(getCustomerDetailsFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getCustomerDetailsFailure(error.toString()));
	}
}

export function * customerCreate({ data }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/customers", {
			method: "POST",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
			body: JSON.stringify(data),
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(createCustomerSuccess(response.data));
		} else {
			yield put(createCustomerFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getCustomerFailure(error.toString()));
	}
}

export function * customerUpdate({ customerId, data }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/customers/" + customerId,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
				body: JSON.stringify(data),
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(updateCustomerSuccess(response.data));
		} else {
			yield put(updateCustomerFailure(response.meta.message));
		}
	} catch (error) {
		yield put(updateCustomerFailure(error.toString()));
	}
}

export function * customerDelete({ customerId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/customers/" + customerId,
			{
				method: "DELETE",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(deleteCustomerSuccess(response.data));
		} else {
			yield put(deleteCustomerFailure(response.meta.message));
		}
	} catch (error) {
		yield put(deleteCustomerFailure(error.toString()));
	}
}

export function * customerHistoryGet({ customerId }){

	console.log(customerId);
	
	const state = yield select();
	const accessToken = state.auth.accessToken;

	
		const queryParams = `customerId=${customerId}`;
	

	try{
		const response = yield call(
			request,
			API_URL + `/admin/activities/customers?${queryParams}` ,
			{
				method:"GET",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
			}
			);
			if (response && response.apiStatus === "SUCCESS") {
				yield put(getCustomerHistorySuccess(response.data.data));
			} else {
				yield put(getCustomerHistoryFailure(response.meta.message));
			}
	}
	catch{
		yield put(getCustomerHistoryFailure(error.toString()));
	}

}

const CustomerSaga = {
	customersGet,
	customerDetailsGet,
	customerCreate,
	customerUpdate,
	customerDelete,
	customerHistoryGet
};

export default CustomerSaga;
