import { call, select, put } from "redux-saga/effects";
import request from "../../util/request";

import { API_URL } from "../../constants";
import {
	getCategorySuccess,
	getCategoryFailure,

	getCategoryDetailsSuccess, 
	getCategoryDetailsFailure,

	createCategorySuccess,
	createCategoryFailure,

	updateCategorySuccess,
	updateCategoryFailure,
	
	deleteCategorySuccess, 
	deleteCategoryFailure, 
	
} from "../actions/categories";

export function * categoriesGet({queryParams}) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + `/admin/categories${queryParams}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getCategorySuccess(response.data));
		} else {
			yield put(getCategoryFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getCategoryFailure(error.toString()));
	}
}

export function * categoryDetailsGet({ categoryId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/categories/" + categoryId, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getCategoryDetailsSuccess(response.data));
		} else {
			yield put(getCategoryDetailsFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getCategoryDetailsFailure(error.toString()));
	}
}

export function * categoryCreate({ data }) {
	const state = yield select();
	console.log(data, ">>>>>?????")
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/categories", {
			method: "POST",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
			body: JSON.stringify(data),
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(createCategorySuccess(response.data));
		} else {
			yield put(createCategoryFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getCategoryFailure(error.toString()));
	}
}

export function * categoryUpdate({ categoryId, data }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/categories/" + categoryId,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
				body: JSON.stringify(data),
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(updateCategorySuccess(response.data));
		} else {
			yield put(updateCategoryFailure(response.meta.message));
		}
	} catch (error) {
		yield put(updateCategoryFailure(error.toString()));
	}
}

export function * categoryDelete({ categoryId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/categories/" + categoryId,
			{
				method: "DELETE",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(deleteCategorySuccess(response.data));
		} else {
			yield put(deleteCategoryFailure(response.meta.message));
		}
	} catch (error) {
		yield put(deleteCategoryFailure(error.toString()));
	}
}

const CategorySaga = {
	categoriesGet,
	categoryDetailsGet,
	categoryCreate,
	categoryUpdate,
	categoryDelete,
};

export default CategorySaga;
