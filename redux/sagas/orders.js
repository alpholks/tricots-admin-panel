import { call, select, put } from "redux-saga/effects";
import request from "../../util/request";

import { API_URL } from "../../constants";
import {
	getOrderSuccess,
	getOrderFailure,

	getOrderDetailsSuccess, 
	getOrderDetailsFailure,

	createOrderSuccess,
	createOrderFailure,

	updateOrderSuccess,
	updateOrderFailure,
	
	deleteOrderSuccess, 
	deleteOrderFailure, 
	
} from "../actions/orders";

export function * ordersGet({queryParams}) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + `/admin/orders${queryParams}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getOrderSuccess(response.data));
		} else {
			yield put(getOrderFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getOrderFailure(error.toString()));
	}
}

export function * orderDetailsGet({ orderId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/orders/" + orderId, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getOrderDetailsSuccess(response.data));
		} else {
			yield put(getOrderDetailsFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getOrderDetailsFailure(error.toString()));
	}
}

export function * orderCreate({ data }) {
	const state = yield select();
	console.log(data, ">>>>>?????")
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/orders", {
			method: "POST",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
			body: JSON.stringify(data),
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(createOrderSuccess(response.data));
		} else {
			yield put(createOrderFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getOrderFailure(error.toString()));
	}
}

export function * orderUpdate({ orderId, data }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/orders/" + orderId,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
				body: JSON.stringify(data),
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(updateOrderSuccess(response.data));
		} else {
			yield put(updateOrderFailure(response.meta.message));
		}
	} catch (error) {
		yield put(updateOrderFailure(error.toString()));
	}
}

export function * orderDelete({ orderId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/orders/" + orderId,
			{
				method: "DELETE",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(deleteOrderSuccess(response.data));
		} else {
			yield put(deleteOrderFailure(response.meta.message));
		}
	} catch (error) {
		yield put(deleteOrderFailure(error.toString()));
	}
}

const OrderSaga = {
	ordersGet,
	orderDetailsGet,
	orderCreate,
	orderUpdate,
	orderDelete,
};

export default OrderSaga;
