import { call, select, put } from "redux-saga/effects";
import request from "../../util/request";

import { API_URL } from "../../constants";
import {
	getPromocodeSuccess,
	getPromocodeFailure,

	getPromocodeDetailsSuccess, 
	getPromocodeDetailsFailure,

	createPromocodeSuccess,
	createPromocodeFailure,

	updatePromocodeSuccess,
	updatePromocodeFailure,
	
	deletePromocodeSuccess, 
	deletePromocodeFailure, 
	
} from "../actions/promocodes";

export function * promocodesGet({queryParams}) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + `/admin/promocodes${queryParams}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getPromocodeSuccess(response.data));
		} else {
			yield put(getPromocodeFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getPromocodeFailure(error.toString()));
	}
}

export function * promocodeDetailsGet({ promocodeId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/promocodes/" + promocodeId, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getPromocodeDetailsSuccess(response.data));
		} else {
			yield put(getPromocodeDetailsFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getPromocodeDetailsFailure(error.toString()));
	}
}

export function * promocodeCreate({ data }) {
	const state = yield select();
	console.log(data, ">>>>>?????")
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + "/admin/promocodes", {
			method: "POST",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
			body: JSON.stringify(data),
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(createPromocodeSuccess(response.data));
		} else {
			yield put(createPromocodeFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getPromocodeFailure(error.toString()));
	}
}

export function * promocodeUpdate({ promocodeId, data }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/promocodes/" + promocodeId,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
				body: JSON.stringify(data),
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(updatePromocodeSuccess(response.data));
		} else {
			yield put(updatePromocodeFailure(response.meta.message));
		}
	} catch (error) {
		yield put(updatePromocodeFailure(error.toString()));
	}
}

export function * promocodeDelete({ promocodeId }) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(
			request,
			API_URL + "/admin/promocodes/" + promocodeId,
			{
				method: "DELETE",
				headers: {
					"Content-Type": "application/json;charset=utf-8",
					"Authorization": `JWT ${accessToken}`
				},
			}
		);
		if (response && response.apiStatus === "SUCCESS") {
			yield put(deletePromocodeSuccess(response.data));
		} else {
			yield put(deletePromocodeFailure(response.meta.message));
		}
	} catch (error) {
		yield put(deletePromocodeFailure(error.toString()));
	}
}

const PromocodeSaga = {
	promocodesGet,
	promocodeDetailsGet,
	promocodeCreate,
	promocodeUpdate,
	promocodeDelete,
};

export default PromocodeSaga;
