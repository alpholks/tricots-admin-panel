import { call, select, put } from "redux-saga/effects";
import request from "../../util/request";

import { API_URL } from "../../constants";
import {
	getSubscribeSuccess,
	getSubscribeFailure,
	
} from "../actions/subscribes";

export function * subscribesGet({queryParams}) {
	const state = yield select();
	const accessToken = state.auth.accessToken;
	try {
		const response = yield call(request, API_URL + `/admin/subscribes${queryParams}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json;charset=utf-8",
				"Authorization": `JWT ${accessToken}`
			},
		});
		if (response && response.apiStatus === "SUCCESS") {
			yield put(getSubscribeSuccess(response.data));
		} else {
			yield put(getSubscribeFailure(response.meta.message));
		}
	} catch (error) {
		yield put(getSubscribeFailure(error.toString()));
	}
}

const SubscribeSaga = {
	subscribesGet,
};

export default SubscribeSaga;
