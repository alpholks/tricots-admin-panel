import { all, takeLatest } from 'redux-saga/effects'

import AuthActions from '../actions/auth'
import CustomersActions from '../actions/customers'
import LocationActions from '../actions/locations'
import StaffsActions from '../actions/staffs'
import WasteBinActions from'../actions/wastebins'
import TaskActions from'../actions/tasks'
import DashboardActions from'../actions/dashboard'
import BannerActions from'../actions/banners'
import CategoryActions from'../actions/categories'
import ChartActions from'../actions/charts'
import CollectionActions from'../actions/collections'
import FeedbackActions from'../actions/feedbacks'
import ProductActions from'../actions/products'
import ComboActions from'../actions/combos'
import HeaderActions from'../actions/headers'
import OrderActions from'../actions/orders'
import PromocodeActions from'../actions/promocodes'
import RatingActions from'../actions/ratings'
import ProfileActions from'../actions/profiles'
import SubscribeActions from'../actions/subscribes'

import AuthSaga from './auth'
import CustomerSaga from './customers'
import LocationSaga from './locations'
import StaffSaga from './staffs'
import WasteBinSaga from './wastebins'
import TaskSaga from './tasks'
import DashboardSaga from './dashboard'
import BannerSaga from './banners'
import CategorySaga from './categories'
import ChartSaga from './charts'
import CollectionSaga from './collections'
import FeedbackSaga from './feedbacks'
import ProductSaga from './products'
import ComboSaga from './combos'
import HeaderSaga from './headers'
import OrderSaga from './orders'
import ProfileSaga from './profiles'
import PromocodeSaga from './promocodes'
import RatingSaga from './ratings'
import SubscribeSaga from './subscribes'

function * rootSaga () {
	yield all([
		takeLatest(AuthActions.actionTypes.LOGIN_REQUEST, AuthSaga.loginPost),
		takeLatest(AuthActions.actionTypes.LOGOUT_REQUEST, AuthSaga.logoutPost),

		takeLatest(CustomersActions.actionTypes.GET_CUSTOMERS_REQUEST, CustomerSaga.customersGet),
		takeLatest(CustomersActions.actionTypes.GET_CUSTOMER_DETAILS_REQUEST, CustomerSaga.customerDetailsGet),
		takeLatest(CustomersActions.actionTypes.CREATE_CUSTOMER_REQUEST, CustomerSaga.customerCreate),
		takeLatest(CustomersActions.actionTypes.UPDATE_CUSTOMER_REQUEST, CustomerSaga.customerUpdate),
		takeLatest(CustomersActions.actionTypes.DELETE_CUSTOMER_REQUEST, CustomerSaga.customerDelete),
		takeLatest(CustomersActions.actionTypes.GET_CUSTOMER_HISTORY_REQUEST, CustomerSaga.customerHistoryGet),


		takeLatest(LocationActions.actionTypes.GET_LOCATIONS_REQUEST, LocationSaga.locationsGet),
		takeLatest(LocationActions.actionTypes.GET_LOCATION_DETAILS_REQUEST, LocationSaga.locationDetailsGet),
		takeLatest(LocationActions.actionTypes.CREATE_LOCATION_REQUEST, LocationSaga.locationCreate),
		takeLatest(LocationActions.actionTypes.UPDATE_LOCATION_REQUEST, LocationSaga.locationUpdate),
		takeLatest(LocationActions.actionTypes.DELETE_LOCATION_REQUEST, LocationSaga.locationDelete),
		takeLatest(LocationActions.actionTypes.GET_LOCATION_HISTORY_REQUEST, LocationSaga.locationHistoryGet),
		
		takeLatest(StaffsActions.actionTypes.GET_STAFFS_REQUEST, StaffSaga.staffsGet),
		takeLatest(StaffsActions.actionTypes.CREATE_STAFF_REQUEST, StaffSaga.staffCreate),
		takeLatest(StaffsActions.actionTypes.GET_STAFF_DETAILS_REQUEST, StaffSaga.staffGetDetail),
		takeLatest(StaffsActions.actionTypes.UPDATE_STAFF_REQUEST, StaffSaga.staffUpdate),
		takeLatest(StaffsActions.actionTypes.DELETE_STAFF_REQUEST, StaffSaga.staffDelete),
		takeLatest(StaffsActions.actionTypes.GET_STAFF_HISTORY_REQUEST, StaffSaga.staffHistoryGet),
		takeLatest(StaffsActions.actionTypes.GET_STAFF_HISTORY_DETAIL_REQUEST, StaffSaga.staffHistoryDetailGet),

		takeLatest(WasteBinActions.actionTypes.GET_WASTEBIN_REQUEST, WasteBinSaga.wasteBinGet),
		takeLatest(WasteBinActions.actionTypes.CREATE_WASTEBIN_REQUEST, WasteBinSaga.wasteBinCreate),
		takeLatest(WasteBinActions.actionTypes.GET_WASTEBIN_DETAILS_REQUEST, WasteBinSaga.wasteBinGetDetail),
		takeLatest(WasteBinActions.actionTypes.UPDATE_WASTEBIN_REQUEST, WasteBinSaga.wasteBinUpdate),
		takeLatest(WasteBinActions.actionTypes.DELETE_WASTEBIN_REQUEST, WasteBinSaga.wasteBinDelete),
		takeLatest(WasteBinActions.actionTypes.GET_WASTEBIN_HISTORY_REQUEST, WasteBinSaga.wasteBinHistoryGet),

		takeLatest(TaskActions.actionTypes.GET_TASKS_REQUEST, TaskSaga.tasksGet),
		takeLatest(TaskActions.actionTypes.GET_TASK_DETAILS_REQUEST, TaskSaga.taskDetailsGet),
		takeLatest(TaskActions.actionTypes.CREATE_TASK_REQUEST, TaskSaga.taskCreate),
		takeLatest(TaskActions.actionTypes.UPDATE_TASK_REQUEST, TaskSaga.taskUpdate),
		takeLatest(TaskActions.actionTypes.DELETE_TASK_REQUEST, TaskSaga.taskDelete),

		takeLatest(DashboardActions.actionTypes.GET_DASHBOARD_REQUEST, DashboardSaga.dashboardGet),
		takeLatest(DashboardActions.actionTypes.GET_DASHBOARD_LOCATION_REQUEST, DashboardSaga.dashboardLocationsGet),

		takeLatest(BannerActions.actionTypes.GET_BANNERS_REQUEST, BannerSaga.bannersGet),
		takeLatest(BannerActions.actionTypes.GET_BANNER_DETAILS_REQUEST, BannerSaga.bannerDetailsGet),
		takeLatest(BannerActions.actionTypes.CREATE_BANNER_REQUEST, BannerSaga.bannerCreate),
		takeLatest(BannerActions.actionTypes.UPDATE_BANNER_REQUEST, BannerSaga.bannerUpdate),
		takeLatest(BannerActions.actionTypes.DELETE_BANNER_REQUEST, BannerSaga.bannerDelete),

		takeLatest(CategoryActions.actionTypes.GET_CATEGORIES_REQUEST, CategorySaga.categoriesGet),
		takeLatest(CategoryActions.actionTypes.GET_CATEGORY_DETAILS_REQUEST, CategorySaga.categoryDetailsGet),
		takeLatest(CategoryActions.actionTypes.CREATE_CATEGORY_REQUEST, CategorySaga.categoryCreate),
		takeLatest(CategoryActions.actionTypes.UPDATE_CATEGORY_REQUEST, CategorySaga.categoryUpdate),
		takeLatest(CategoryActions.actionTypes.DELETE_CATEGORY_REQUEST, CategorySaga.categoryDelete),

		takeLatest(ChartActions.actionTypes.GET_CHARTS_REQUEST, ChartSaga.chartsGet),
		takeLatest(ChartActions.actionTypes.GET_CHART_DETAILS_REQUEST, ChartSaga.chartDetailsGet),
		takeLatest(ChartActions.actionTypes.CREATE_CHART_REQUEST, ChartSaga.chartCreate),
		takeLatest(ChartActions.actionTypes.UPDATE_CHART_REQUEST, ChartSaga.chartUpdate),
		takeLatest(ChartActions.actionTypes.DELETE_CHART_REQUEST, ChartSaga.chartDelete),

		takeLatest(CollectionActions.actionTypes.GET_COLLECTIONS_REQUEST, CollectionSaga.collectionsGet),
		takeLatest(CollectionActions.actionTypes.GET_COLLECTION_DETAILS_REQUEST, CollectionSaga.collectionDetailsGet),
		takeLatest(CollectionActions.actionTypes.CREATE_COLLECTION_REQUEST, CollectionSaga.collectionCreate),
		takeLatest(CollectionActions.actionTypes.UPDATE_COLLECTION_REQUEST, CollectionSaga.collectionUpdate),
		takeLatest(CollectionActions.actionTypes.DELETE_COLLECTION_REQUEST, CollectionSaga.collectionDelete),

		takeLatest(FeedbackActions.actionTypes.GET_FEEDBACKS_REQUEST, FeedbackSaga.feedbacksGet),

		takeLatest(ProductActions.actionTypes.GET_PRODUCTS_REQUEST, ProductSaga.productsGet),
		takeLatest(ProductActions.actionTypes.GET_PRODUCT_DETAILS_REQUEST, ProductSaga.productDetailsGet),
		takeLatest(ProductActions.actionTypes.CREATE_PRODUCT_REQUEST, ProductSaga.productCreate),
		takeLatest(ProductActions.actionTypes.UPDATE_PRODUCT_REQUEST, ProductSaga.productUpdate),
		takeLatest(ProductActions.actionTypes.DELETE_PRODUCT_REQUEST, ProductSaga.productDelete),

		takeLatest(ComboActions.actionTypes.GET_COMBOS_REQUEST, ComboSaga.combosGet),
		takeLatest(ComboActions.actionTypes.GET_COMBO_DETAILS_REQUEST, ComboSaga.comboDetailsGet),
		takeLatest(ComboActions.actionTypes.CREATE_COMBO_REQUEST, ComboSaga.comboCreate),
		takeLatest(ComboActions.actionTypes.UPDATE_COMBO_REQUEST, ComboSaga.comboUpdate),
		takeLatest(ComboActions.actionTypes.DELETE_COMBO_REQUEST, ComboSaga.comboDelete),

		takeLatest(HeaderActions.actionTypes.GET_HEADERS_REQUEST, HeaderSaga.headersGet),
		takeLatest(HeaderActions.actionTypes.GET_HEADER_DETAILS_REQUEST, HeaderSaga.headerDetailsGet),
		takeLatest(HeaderActions.actionTypes.CREATE_HEADER_REQUEST, HeaderSaga.headerCreate),
		takeLatest(HeaderActions.actionTypes.UPDATE_HEADER_REQUEST, HeaderSaga.headerUpdate),
		takeLatest(HeaderActions.actionTypes.DELETE_HEADER_REQUEST, HeaderSaga.headerDelete),

		takeLatest(OrderActions.actionTypes.GET_ORDERS_REQUEST, OrderSaga.ordersGet),
		takeLatest(OrderActions.actionTypes.GET_ORDER_DETAILS_REQUEST, OrderSaga.orderDetailsGet),
		takeLatest(OrderActions.actionTypes.UPDATE_ORDER_REQUEST, OrderSaga.orderUpdate),

		takeLatest(ProfileActions.actionTypes.GET_PROFILE_DETAILS_REQUEST, ProfileSaga.profileDetailsGet),
		takeLatest(ProfileActions.actionTypes.UPDATE_PROFILE_REQUEST, ProfileSaga.profileUpdate),

		takeLatest(PromocodeActions.actionTypes.GET_PROMOCODES_REQUEST, PromocodeSaga.promocodesGet),
		takeLatest(PromocodeActions.actionTypes.GET_PROMOCODE_DETAILS_REQUEST, PromocodeSaga.promocodeDetailsGet),
		takeLatest(PromocodeActions.actionTypes.CREATE_PROMOCODE_REQUEST, PromocodeSaga.promocodeCreate),
		takeLatest(PromocodeActions.actionTypes.UPDATE_PROMOCODE_REQUEST, PromocodeSaga.promocodeUpdate),
		takeLatest(PromocodeActions.actionTypes.DELETE_PROMOCODE_REQUEST, PromocodeSaga.promocodeDelete),

		takeLatest(RatingActions.actionTypes.GET_RATINGS_REQUEST, RatingSaga.ratingsGet),
		takeLatest(RatingActions.actionTypes.GET_RATING_DETAILS_REQUEST, RatingSaga.ratingDetailsGet),
		takeLatest(RatingActions.actionTypes.CREATE_RATING_REQUEST, RatingSaga.ratingCreate),
		takeLatest(RatingActions.actionTypes.UPDATE_RATING_REQUEST, RatingSaga.ratingUpdate),
		takeLatest(RatingActions.actionTypes.DELETE_RATING_REQUEST, RatingSaga.ratingDelete),

		takeLatest(SubscribeActions.actionTypes.GET_SUBSCRIBES_REQUEST, SubscribeSaga.subscribesGet),

	])
}

export default rootSaga
