import { actionTypes } from "../actions/promocodes";

export const initialState = {

    fetchingPromocodes : false,
    fetchPromocodeSuccessful : false,
    promocodes : [],

    fetchingPromocodeDetails : false,
    fetchPromocodeDetailsSuccessful : false,
    promocodeDetails: null,

    creatingPromocode : false,
    createPromocodeSuccessful : false,
    promocode : null,

    updatingPromocode : false,
    updatePromocodeSuccessful : false,
    
    deletingPromocode : false,
    deletePromocodeSuccessful : false,

	error: null,
};

function promocodeReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } };
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState };

		case actionTypes.GET_PROMOCODES_REQUEST: return { ...state, ...{ fetchingPromocodes: true } };
		case actionTypes.GET_PROMOCODES_FAILURE: return { ...state, ...{ fetchingPromocodes: false, error: action.error }};
		case actionTypes.GET_PROMOCODES_SUCCESS: return { ...state, ...{ fetchingPromocodes: false, fetchPromocodeSuccessful:true,  promocodes: action.promocodes }};

        case actionTypes.GET_PROMOCODE_DETAILS_REQUEST: return { ...state, ...{ fetchingPromocodeDetails: true } };
		case actionTypes.GET_PROMOCODE_DETAILS_FAILURE: return { ...state, ...{ fetchingPromocodeDetails: false, error: action.error }};
		case actionTypes.GET_PROMOCODE_DETAILS_SUCCESS: return { ...state, ...{ fetchingPromocodeDetails: false, fetchPromocodeDetailsSuccessful:true,  promocodeDetails: action.promocodeDetails }};

        case actionTypes.CREATE_PROMOCODE_REQUEST: return { ...state, ...{ creatingPromocode: true } };
		case actionTypes.CREATE_PROMOCODE_FAILURE: return { ...state, ...{ creatingPromocode: false, error: action.error }};
		case actionTypes.CREATE_PROMOCODE_SUCCESS: return { ...state, ...{ creatingPromocode: false, createPromocodeSuccessful:true,  promocode: action.promocode }};

        case actionTypes.UPDATE_PROMOCODE_REQUEST: return { ...state, ...{ updatingPromocode: true } };
		case actionTypes.UPDATE_PROMOCODE_FAILURE: return { ...state, ...{ updatingPromocode: false, error: action.error }};
        case actionTypes.UPDATE_PROMOCODE_SUCCESS: return { ...state, ...{ updatingPromocode: false, updatePromocodeSuccessful:true,  promocode: action.promocode }};
        
        case actionTypes.DELETE_PROMOCODE_REQUEST: return { ...state, ...{ deletingPromocode: true } };
		case actionTypes.DELETE_PROMOCODE_FAILURE: return { ...state, ...{ deletingPromocode: false, error: action.error }};
        case actionTypes.DELETE_PROMOCODE_SUCCESS: return { ...state, ...{ deletingPromocode: false, deletePromocodeSuccessful:true,  promocode: null }};
        
		default:return state;
	}
}

export default promocodeReducer;
