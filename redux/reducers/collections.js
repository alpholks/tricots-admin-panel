import { actionTypes } from "../actions/collections";

export const initialState = {

    fetchingCollections : false,
    fetchCollectionSuccessful : false,
    collections : [],

    fetchingCollectionDetails : false,
    fetchCollectionDetailsSuccessful : false,
    collectionDetails: null,

    creatingCollection : false,
    createCollectionSuccessful : false,
    collection : null,

    updatingCollection : false,
    updateCollectionSuccessful : false,
    
    deletingCollection : false,
    deleteCollectionSuccessful : false,

	error: null,
};

function collectionReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } };
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState };

		case actionTypes.GET_COLLECTIONS_REQUEST: return { ...state, ...{ fetchingCollections: true } };
		case actionTypes.GET_COLLECTIONS_FAILURE: return { ...state, ...{ fetchingCollections: false, error: action.error }};
		case actionTypes.GET_COLLECTIONS_SUCCESS: return { ...state, ...{ fetchingCollections: false, fetchCollectionSuccessful:true,  collections: action.collections }};

        case actionTypes.GET_COLLECTION_DETAILS_REQUEST: return { ...state, ...{ fetchingCollectionDetails: true } };
		case actionTypes.GET_COLLECTION_DETAILS_FAILURE: return { ...state, ...{ fetchingCollectionDetails: false, error: action.error }};
		case actionTypes.GET_COLLECTION_DETAILS_SUCCESS: return { ...state, ...{ fetchingCollectionDetails: false, fetchCollectionDetailsSuccessful:true,  collectionDetails: action.collectionDetails }};

        case actionTypes.CREATE_COLLECTION_REQUEST: return { ...state, ...{ creatingCollection: true } };
		case actionTypes.CREATE_COLLECTION_FAILURE: return { ...state, ...{ creatingCollection: false, error: action.error }};
		case actionTypes.CREATE_COLLECTION_SUCCESS: return { ...state, ...{ creatingCollection: false, createCollectionSuccessful:true,  collection: action.collection }};

        case actionTypes.UPDATE_COLLECTION_REQUEST: return { ...state, ...{ updatingCollection: true } };
		case actionTypes.UPDATE_COLLECTION_FAILURE: return { ...state, ...{ updatingCollection: false, error: action.error }};
        case actionTypes.UPDATE_COLLECTION_SUCCESS: return { ...state, ...{ updatingCollection: false, updateCollectionSuccessful:true,  collection: action.collection }};
        
        case actionTypes.DELETE_COLLECTION_REQUEST: return { ...state, ...{ deletingCollection: true } };
		case actionTypes.DELETE_COLLECTION_FAILURE: return { ...state, ...{ deletingCollection: false, error: action.error }};
        case actionTypes.DELETE_COLLECTION_SUCCESS: return { ...state, ...{ deletingCollection: false, deleteCollectionSuccessful:true,  collection: null }};
        
		default:return state;
	}
}

export default collectionReducer;
