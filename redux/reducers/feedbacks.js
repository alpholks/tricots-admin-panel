import { actionTypes } from "../actions/feedbacks";

export const initialState = {

    fetchingFeedbacks : false,
    fetchFeedbackSuccessful : false,
    feedbacks : [],

	error: null,
};

function feedbackReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } };
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState };

		case actionTypes.GET_FEEDBACKS_REQUEST: return { ...state, ...{ fetchingFeedbacks: true } };
		case actionTypes.GET_FEEDBACKS_FAILURE: return { ...state, ...{ fetchingFeedbacks: false, error: action.error }};
		case actionTypes.GET_FEEDBACKS_SUCCESS: return { ...state, ...{ fetchingFeedbacks: false, fetchFeedbackSuccessful:true,  feedbacks: action.feedbacks }};

		default:return state;
	}
}

export default feedbackReducer;
