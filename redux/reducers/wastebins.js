import { actionTypes } from "../actions/wastebins";

export const initialState = {

    fetchingWasteBins : false,
    fetchWasteBinsSuccessfull : false,
    wasteBins : [],

    fetchingWasteBinHistory : false,
    fetchWasteBinHistorySuccessfulll : false,
    wasteBinHistory : [],

    creatingWasteBin : false,
    createWasteBinSuccessfull : false,
    wasteBin : null,

    fetchingWasteBinDetails:false,
    fetchWasteBinDetailsSuccessfull:false,
    wasteBinDetails:null,

    updatingWasteBin : false,
    updateWasteBinsSuccessful : false,
    
    deletingWasteBins : false,
    deleteWasteBinSuccessfull : false,
    
	error: null,
};

function wasteBinReducer(state = initialState, action) {
    
    switch (action.type) {

		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } };
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState };

		case actionTypes.GET_WASTEBIN_REQUEST: return { ...state, ...{fetchingWasteBins: true } };
		case actionTypes.GET_WASTEBIN_FAILURE: return { ...state, ...{fetchingWasteBins: false, error: action.error }};
		case actionTypes.GET_WASTEBIN_SUCCESS: return { ...state, ...{fetchingWasteBins: false, fetchWasteBinsSuccessful:true,  wasteBins: action.wasteBins }};
        
        case actionTypes.GET_WASTEBIN_HISTORY_REQUEST: return {...state,...{fetchingWasteBinHistory: true}};
        case actionTypes.GET_WASTEBIN_HISTORY_FAILURE: return {...state,...{fetchingWasteBinHistory: false,error:action.error}};
        case actionTypes.GET_WASTEBIN_HISTORY_SUCCESS: return {...state,...{fetchingWasteBinHistory :false, fetchWasteBinHistorySuccessfulll:true, wasteBinHistory:action.wasteBinHistory}};

        case actionTypes.CREATE_WASTEBIN_REQUEST: return { ...state, ...{ creatingWasteBin: true } };
		case actionTypes.CREATE_WASTEBIN_FAILURE: return { ...state, ...{ creatingWasteBin: false, error: action.error }};
		case actionTypes.CREATE_WASTEBIN_SUCCESS: return { ...state, ...{ creatingWasteBin: false, createWasteBinSuccessful:true,  wasteBinDetails: action.wasteBin }};

        case actionTypes.GET_WASTEBIN_DETAILS_REQUEST: return { ...state, ...{ fetchingWasteBinDetails: true } };
		case actionTypes.GET_WASTEBIN_DETAILS_FAILURE: return { ...state, ...{ fetchingWasteBinDetails: false, error: action.error }};
		case actionTypes.GET_WASTEBIN_DETAILS_SUCCESS: console.log(action.wasteBinDetail);return { ...state, ...{ fetchingWasteBinDetails: false, fetchWasteBinDetailsSuccessfull:true,  wasteBinDetails: action.wasteBinDetail }};
        
        case actionTypes.UPDATE_WASTEBIN_REQUEST: return { ...state, ...{ updatingWasteBin: true } };
		case actionTypes.UPDATE_WASTEBIN_FAILURE: return { ...state, ...{ updatingWasteBin: false, error: action.error }};
        case actionTypes.UPDATE_WASTEBIN_SUCCESS: return { ...state, ...{ updatingWasteBin: false, updateWasteBinSuccessful:true,  wasteBin: action.WasteBin }};
        
        case actionTypes.DELETE_WASTEBIN_REQUEST: return { ...state, ...{ deletingWasteBins: true } };
		case actionTypes.DELETE_WASTEBIN_FAILURE: return { ...state, ...{ deletingWasteBins: false, error: action.error }};
        case actionTypes.DELETE_WASTEBIN_SUCCESS: return { ...state, ...{ deletingWasteBins: false, deleteWasteBinSuccessfull:true,  wasteBin: null }};
        

		default:return state;
	}
}

export default wasteBinReducer;
