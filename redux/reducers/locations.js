import { actionTypes } from "../actions/locations";

export const initialState = {

    fetchingLocations : false,
    fetchLocationSuccessful : false,
    locations : [],

    fetchingLocationDetails : false,
    fetchLocationDetailsSuccessful : false,
    locationDetails: null,

    creatingLocation : false,
    createLocationSuccessful : false,
    location : null,

    updatingLocation : false,
    updateLocationSuccessful : false,
    
    deletingLocation : false,
    deleteLocationSuccessful : false,

    fetchingLocationHistory : false,
    fetchLocationHistorySuccessful : false,
    locationHistory: [],
    
	error: null,
};

function locationReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } };
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState };

		case actionTypes.GET_LOCATIONS_REQUEST: return { ...state, ...{ fetchingLocations: true } };
		case actionTypes.GET_LOCATIONS_FAILURE: return { ...state, ...{ fetchingLocations: false, error: action.error }};
		case actionTypes.GET_LOCATIONS_SUCCESS: return { ...state, ...{ fetchingLocations: false, fetchLocationSuccessful:true,  locations: action.locations }};

        case actionTypes.GET_LOCATION_DETAILS_REQUEST: return { ...state, ...{ fetchingLocationDetails: true } };
		case actionTypes.GET_LOCATION_DETAILS_FAILURE: return { ...state, ...{ fetchingLocationDetails: false, error: action.error }};
		case actionTypes.GET_LOCATION_DETAILS_SUCCESS: return { ...state, ...{ fetchingLocationDetails: false, fetchLocationDetailsSuccessful:true,  locationDetails: action.locationDetails }};

        case actionTypes.CREATE_LOCATION_REQUEST: return { ...state, ...{ creatingLocation: true } };
		case actionTypes.CREATE_LOCATION_FAILURE: return { ...state, ...{ creatingLocation: false, error: action.error }};
		case actionTypes.CREATE_LOCATION_SUCCESS: return { ...state, ...{ creatingLocation: false, createLocationSuccessful:true,  location: action.location }};

        case actionTypes.UPDATE_LOCATION_REQUEST: return { ...state, ...{ updatingLocation: true } };
		case actionTypes.UPDATE_LOCATION_FAILURE: return { ...state, ...{ updatingLocation: false, error: action.error }};
        case actionTypes.UPDATE_LOCATION_SUCCESS: return { ...state, ...{ updatingLocation: false, updateLocationSuccessful:true,  location: action.location }};
        
        case actionTypes.DELETE_LOCATION_REQUEST: return { ...state, ...{ deletingLocation: true } };
		case actionTypes.DELETE_LOCATION_FAILURE: return { ...state, ...{ deletingLocation: false, error: action.error }};
        case actionTypes.DELETE_LOCATION_SUCCESS: return { ...state, ...{ deletingLocation: false, deleteLocationSuccessful:true,  location: null }};
        
        case actionTypes.GET_LOCATION_HISTORY_REQUEST: return { ...state, ...{ fetchingLocationHistory: true } };
		case actionTypes.GET_LOCATION_HISTORY_FAILURE: return { ...state, ...{ fetchingLocationHistory: false, error: action.error }};
		case actionTypes.GET_LOCATION_HISTORY_SUCCESS: return { ...state, ...{ fetchingLocationHistory: false, fetchLocationHistorySuccessful:true,  locationHistory: action.locationHistory }};

		default:return state;
	}
}

export default locationReducer;
