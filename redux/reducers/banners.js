import { actionTypes } from "../actions/banners";

export const initialState = {

    fetchingBanners : false,
    fetchBannerSuccessful : false,
    banners : [],

    fetchingBannerDetails : false,
    fetchBannerDetailsSuccessful : false,
    bannerDetails: null,

    creatingBanner : false,
    createBannerSuccessful : false,
    banner : null,

    updatingBanner : false,
    updateBannerSuccessful : false,
    
    deletingBanner : false,
    deleteBannerSuccessful : false,

	error: null,
};

function bannerReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } };
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState };

		case actionTypes.GET_BANNERS_REQUEST: return { ...state, ...{ fetchingBanners: true } };
		case actionTypes.GET_BANNERS_FAILURE: return { ...state, ...{ fetchingBanners: false, error: action.error }};
		case actionTypes.GET_BANNERS_SUCCESS: return { ...state, ...{ fetchingBanners: false, fetchBannerSuccessful:true,  banners: action.banners }};

        case actionTypes.GET_BANNER_DETAILS_REQUEST: return { ...state, ...{ fetchingBannerDetails: true } };
		case actionTypes.GET_BANNER_DETAILS_FAILURE: return { ...state, ...{ fetchingBannerDetails: false, error: action.error }};
		case actionTypes.GET_BANNER_DETAILS_SUCCESS: return { ...state, ...{ fetchingBannerDetails: false, fetchBannerDetailsSuccessful:true,  bannerDetails: action.bannerDetails }};

        case actionTypes.CREATE_BANNER_REQUEST: return { ...state, ...{ creatingBanner: true } };
		case actionTypes.CREATE_BANNER_FAILURE: return { ...state, ...{ creatingBanner: false, error: action.error }};
		case actionTypes.CREATE_BANNER_SUCCESS: return { ...state, ...{ creatingBanner: false, createBannerSuccessful:true,  banner: action.banner }};

        case actionTypes.UPDATE_BANNER_REQUEST: return { ...state, ...{ updatingBanner: true } };
		case actionTypes.UPDATE_BANNER_FAILURE: return { ...state, ...{ updatingBanner: false, error: action.error }};
        case actionTypes.UPDATE_BANNER_SUCCESS: return { ...state, ...{ updatingBanner: false, updateBannerSuccessful:true,  banner: action.banner }};
        
        case actionTypes.DELETE_BANNER_REQUEST: return { ...state, ...{ deletingBanner: true } };
		case actionTypes.DELETE_BANNER_FAILURE: return { ...state, ...{ deletingBanner: false, error: action.error }};
        case actionTypes.DELETE_BANNER_SUCCESS: return { ...state, ...{ deletingBanner: false, deleteBannerSuccessful:true,  banner: null }};
        
		default:return state;
	}
}

export default bannerReducer;
