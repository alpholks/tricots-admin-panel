import { actionTypes } from "../actions/auth";

export const initialState = {
	loggingIn: false,
	logInSuccessful: false,

	loggingOut: false,
	logOutSuccessful: false,
	accessToken: null,

	error: null,
};

function authReducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR:return { ...state, ...{ error: null } };
		case actionTypes.STATE_RESET:return { ...state, ...action.resetState };

		case actionTypes.LOGIN_REQUEST:return { ...state, ...{ loggingIn: true } };
		case actionTypes.LOGIN_FAILURE:return {...state,...{ loggingIn: false, logInSuccessful: false, error: action.error }};
		case actionTypes.LOGIN_SUCCESS: localStorage.setItem("accessToken", action.accessToken); return {...state,...{loggingIn: false,logInSuccessful: true,accessToken: action.accessToken}};

		case actionTypes.LOGOUT_REQUEST:return { ...state, ...{ loggingOut: true } };
		case actionTypes.LOGIN_FAILURE:return {...state,...{ loggingOut: false, logOutSuccessful: false, error: action.error }};
		case actionTypes.LOGOUT_SUCCESS:localStorage.removeItem("accessToken"); return {...state, ...{ loggingOut : false, logOutSuccessful: true } };

		default:return state;
	}
}

export default authReducer;
