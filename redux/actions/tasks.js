export const actionTypes = {
	CLEAR_ERROR: "TASKS/CLEAR_ERROR",
	STATE_RESET: "TASKS/STATE_RESET",

	GET_TASKS_REQUEST: "TASKS/GET_TASKS_REQUEST",
	GET_TASKS_SUCCESS: "TASKS/GET_TASKS_SUCCESS",
    GET_TASKS_FAILURE: "TASKS/GET_TASKS_FAILURE",

    GET_TASK_DETAILS_REQUEST: "TASKS/GET_TASK_DETAILS_REQUEST",
	GET_TASK_DETAILS_SUCCESS: "TASKS/GET_TASK_DETAILS_SUCCESS",
    GET_TASK_DETAILS_FAILURE: "TASKS/GET_TASK_DETAILS_FAILURE",
    
    CREATE_TASK_REQUEST: "TASKS/CREATE_TASK_REQUEST",
	CREATE_TASK_SUCCESS: "TASKS/CREATE_TASK_SUCCESS",
    CREATE_TASK_FAILURE: "TASKS/CREATE_TASK_FAILURE",

    UPDATE_TASK_REQUEST: "TASKS/UPDATE_TASK_REQUEST",
	UPDATE_TASK_SUCCESS: "TASKS/UPDATE_TASK_SUCCESS",
    UPDATE_TASK_FAILURE: "TASKS/UPDATE_TASK_FAILURE",
    
    DELETE_TASK_REQUEST: "TASKS/DELETE_TASK_REQUEST",
	DELETE_TASK_SUCCESS: "TASKS/DELETE_TASK_SUCCESS",
	DELETE_TASK_FAILURE: "TASKS/DELETE_TASK_FAILURE",
};

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR });
export const stateReset = (resetState) => ({type: actionTypes.STATE_RESET,resetState,});

export const getTasksRequest = (queryParams) => ({type: actionTypes.GET_TASKS_REQUEST, queryParams});
export const getTasksFailure = (error) => ({type: actionTypes.GET_TASKS_FAILURE, error});
export const getTasksSuccess = (tasks) => ({type: actionTypes.GET_TASKS_SUCCESS, tasks});

export const getTaskDetailsRequest = (taskId) => ({type: actionTypes.GET_TASK_DETAILS_REQUEST, taskId});
export const getTaskDetailsFailure = (error) => ({type: actionTypes.GET_TASK_DETAILS_FAILURE, error});
export const getTaskDetailsSuccess = (taskDetails) => ({type: actionTypes.GET_TASK_DETAILS_SUCCESS, taskDetails});

export const createTaskRequest = (data) => ({type: actionTypes.CREATE_TASK_REQUEST, data});
export const createTaskFailure = (error) => ({type: actionTypes.CREATE_TASK_FAILURE, error});
export const createTaskSuccess = (task) => ({type: actionTypes.CREATE_TASK_SUCCESS, task});

export const updateTaskRequest = (taskId, data) => ({type: actionTypes.UPDATE_TASK_REQUEST, taskId, data});
export const updateTaskFailure = (error) => ({type: actionTypes.UPDATE_TASK_FAILURE, error});
export const updateTaskSuccess = (task) => ({type: actionTypes.UPDATE_TASK_SUCCESS, task});

export const deleteTaskRequest = (taskId) => ({type: actionTypes.DELETE_TASK_REQUEST, taskId});
export const deleteTaskFailure = (error) => ({type: actionTypes.DELETE_TASK_FAILURE, error});
export const deleteTaskSuccess = () => ({type: actionTypes.DELETE_TASK_SUCCESS });

const TaskActions = {
	actionTypes,

	clearError,
	stateReset,

    getTasksRequest,
    getTasksFailure,
    getTasksSuccess,

    getTaskDetailsRequest,
    getTaskDetailsFailure,
    getTaskDetailsSuccess,

    createTaskRequest,
    createTaskFailure,
    createTaskSuccess,

    updateTaskRequest,
    updateTaskFailure,
    updateTaskSuccess,

    deleteTaskRequest,
    deleteTaskFailure,
    deleteTaskSuccess,

};

export default TaskActions;
