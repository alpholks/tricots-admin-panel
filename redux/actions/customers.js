export const actionTypes = {
	CLEAR_ERROR: "CUSTOMERS/CLEAR_ERROR",
	STATE_RESET: "CUSTOMERS/STATE_RESET",

	GET_CUSTOMERS_REQUEST: "CUSTOMERS/GET_CUSTOMERS_REQUEST",
	GET_CUSTOMERS_SUCCESS: "CUSTOMERS/GET_CUSTOMERS_SUCCESS",
    GET_CUSTOMERS_FAILURE: "CUSTOMERS/GET_CUSTOMERS_FAILURE",

    GET_CUSTOMER_DETAILS_REQUEST: "CUSTOMERS/GET_CUSTOMER_DETAILS_REQUEST",
	GET_CUSTOMER_DETAILS_SUCCESS: "CUSTOMERS/GET_CUSTOMER_DETAILS_SUCCESS",
    GET_CUSTOMER_DETAILS_FAILURE: "CUSTOMERS/GET_CUSTOMER_DETAILS_FAILURE",

    
    CREATE_CUSTOMER_REQUEST: "CUSTOMERS/CREATE_CUSTOMER_REQUEST",
	CREATE_CUSTOMER_SUCCESS: "CUSTOMERS/CREATE_CUSTOMER_SUCCESS",
    CREATE_CUSTOMER_FAILURE: "CUSTOMERS/CREATE_CUSTOMER_FAILURE",

    UPDATE_CUSTOMER_REQUEST: "CUSTOMERS/UPDATE_CUSTOMER_REQUEST",
	UPDATE_CUSTOMER_SUCCESS: "CUSTOMERS/UPDATE_CUSTOMER_SUCCESS",
    UPDATE_CUSTOMER_FAILURE: "CUSTOMERS/UPDATE_CUSTOMER_FAILURE",
    
    DELETE_CUSTOMER_REQUEST: "CUSTOMERS/DELETE_CUSTOMER_REQUEST",
	DELETE_CUSTOMER_SUCCESS: "CUSTOMERS/DELETE_CUSTOMER_SUCCESS",
    DELETE_CUSTOMER_FAILURE: "CUSTOMERS/DELETE_CUSTOMER_FAILURE",
    
    GET_CUSTOMER_HISTORY_REQUEST: "CUSTOMERS/GET_CUSTOMER_HISTORY_REQUEST",
	GET_CUSTOMER_HISTORY_SUCCESS: "CUSTOMERS/GET_CUSTOMER_HISTORY_SUCCESS",
    GET_CUSTOMER_HISTORY_FAILURE: "CUSTOMERS/GET_CUSTOMER_HISTORY_FAILURE",
};

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR });
export const stateReset = (resetState) => ({type: actionTypes.STATE_RESET,resetState,});

export const getCustomerRequest = (queryParams) => ({type: actionTypes.GET_CUSTOMERS_REQUEST, queryParams});
export const getCustomerFailure = (error) => ({type: actionTypes.GET_CUSTOMERS_FAILURE, error});
export const getCustomerSuccess = (customers) => ({type: actionTypes.GET_CUSTOMERS_SUCCESS, customers});

export const getCustomerDetailsRequest = (customerId) => ({type: actionTypes.GET_CUSTOMER_DETAILS_REQUEST, customerId});
export const getCustomerDetailsFailure = (error) => ({type: actionTypes.GET_CUSTOMER_DETAILS_FAILURE, error});
export const getCustomerDetailsSuccess = (customerDetails) => ({type: actionTypes.GET_CUSTOMER_DETAILS_SUCCESS, customerDetails});

export const createCustomerRequest = (data) => ({type: actionTypes.CREATE_CUSTOMER_REQUEST, data});
export const createCustomerFailure = (error) => ({type: actionTypes.CREATE_CUSTOMER_FAILURE, error});
export const createCustomerSuccess = (customer) => ({type: actionTypes.CREATE_CUSTOMER_SUCCESS, customer});

export const updateCustomerRequest = (customerId, data) => ({type: actionTypes.UPDATE_CUSTOMER_REQUEST, customerId, data});
export const updateCustomerFailure = (error) => ({type: actionTypes.UPDATE_CUSTOMER_FAILURE, error});
export const updateCustomerSuccess = (customer) => ({type: actionTypes.UPDATE_CUSTOMER_SUCCESS, customer});

export const deleteCustomerRequest = (customerId) => ({type: actionTypes.DELETE_CUSTOMER_REQUEST, customerId});
export const deleteCustomerFailure = (error) => ({type: actionTypes.DELETE_CUSTOMER_FAILURE, error});
export const deleteCustomerSuccess = () => ({type: actionTypes.DELETE_CUSTOMER_SUCCESS });


export const getCustomerHistoryRequest = (customerId) => ({type: actionTypes.GET_CUSTOMER_HISTORY_REQUEST, customerId});
export const getCustomerHistoryFailure = (error) => ({type: actionTypes.GET_CUSTOMER_HISTORY_FAILURE, error});
export const getCustomerHistorySuccess = (customerHistory) => ({type: actionTypes.GET_CUSTOMER_HISTORY_SUCCESS, customerHistory});


const AuthActions = {
	actionTypes,

	clearError,
	stateReset,

    getCustomerRequest,
    getCustomerFailure,
    getCustomerSuccess,

    getCustomerDetailsRequest,
    getCustomerDetailsFailure,
    getCustomerDetailsSuccess,

    createCustomerRequest,
    createCustomerFailure,
    createCustomerSuccess,

    updateCustomerRequest,
    updateCustomerFailure,
    updateCustomerSuccess,

    deleteCustomerRequest,
    deleteCustomerFailure,
    deleteCustomerSuccess,

    getCustomerHistoryRequest,
    getCustomerHistorySuccess,
    getCustomerHistoryFailure

};

export default AuthActions;
