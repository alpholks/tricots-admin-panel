export const actionTypes = {
	CLEAR_ERROR: "SUBSCRIBES/CLEAR_ERROR",
	STATE_RESET: "SUBSCRIBES/STATE_RESET",

	GET_SUBSCRIBES_REQUEST: "SUBSCRIBES/GET_SUBSCRIBES_REQUEST",
	GET_SUBSCRIBES_SUCCESS: "SUBSCRIBES/GET_SUBSCRIBES_SUCCESS",
    GET_SUBSCRIBES_FAILURE: "SUBSCRIBES/GET_SUBSCRIBES_FAILURE",

};

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR });
export const stateReset = (resetState) => ({type: actionTypes.STATE_RESET,resetState,});

export const getSubscribeRequest = (queryParams) => ({type: actionTypes.GET_SUBSCRIBES_REQUEST, queryParams});
export const getSubscribeFailure = (error) => ({type: actionTypes.GET_SUBSCRIBES_FAILURE, error});
export const getSubscribeSuccess = (subscribes) => ({type: actionTypes.GET_SUBSCRIBES_SUCCESS, subscribes});

const AuthActions = {
	actionTypes,

	clearError,
	stateReset,

    getSubscribeRequest,
    getSubscribeFailure,
    getSubscribeSuccess,

};

export default AuthActions;
