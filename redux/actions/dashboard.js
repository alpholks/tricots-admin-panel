export const actionTypes = {
	CLEAR_ERROR: "DASHBOARD/CLEAR_ERROR",
	STATE_RESET: "DASHBOARD/STATE_RESET",

	GET_DASHBOARD_REQUEST: "DASHBOARD/GET_DASHBOARD_REQUEST",
	GET_DASHBOARD_SUCCESS: "DASHBOARD/GET_DASHBOARD_SUCCESS",
	GET_DASHBOARD_FAILURE: "DASHBOARD/GET_DASHBOARD_FAILURE",
	
	GET_DASHBOARD_LOCATION_REQUEST: "DASHBOARD/GET_DASHBOARD_LOCATION_REQUEST",
	GET_DASHBOARD_LOCATION_SUCCESS: "DASHBOARD/GET_DASHBOARD_LOCATION_SUCCESS",
    GET_DASHBOARD_LOCATION_FAILURE: "DASHBOARD/GET_DASHBOARD_LOCATION_FAILURE",
};

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR });
export const stateReset = (resetState) => ({type: actionTypes.STATE_RESET,resetState,});

export const getDashboardRequest = (queryParams) => ({type: actionTypes.GET_DASHBOARD_REQUEST, queryParams});
export const getDashboardFailure = (error) => ({type: actionTypes.GET_DASHBOARD_FAILURE, error});
export const getDashboardSuccess = (dashboard) => ({type: actionTypes.GET_DASHBOARD_SUCCESS, dashboard});

export const getDashboardLocationRequest = () => ({type: actionTypes.GET_DASHBOARD_LOCATION_REQUEST});
export const getDashboardLocationFailure = (error) => ({type: actionTypes.GET_DASHBOARD_LOCATION_FAILURE, error});
export const getDashboardLocationSuccess = (dashboardLocations) => ({type: actionTypes.GET_DASHBOARD_LOCATION_SUCCESS, dashboardLocations});

const DashboardActions = {
	actionTypes,

	clearError,
	stateReset,

    getDashboardRequest,
    getDashboardFailure,
	getDashboardSuccess,
	
	getDashboardLocationRequest,
    getDashboardLocationFailure,
    getDashboardLocationSuccess,

};

export default DashboardActions;
