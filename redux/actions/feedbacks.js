export const actionTypes = {
	CLEAR_ERROR: "FEEDBACKS/CLEAR_ERROR",
	STATE_RESET: "FEEDBACKS/STATE_RESET",

	GET_FEEDBACKS_REQUEST: "FEEDBACKS/GET_FEEDBACKS_REQUEST",
	GET_FEEDBACKS_SUCCESS: "FEEDBACKS/GET_FEEDBACKS_SUCCESS",
    GET_FEEDBACKS_FAILURE: "FEEDBACKS/GET_FEEDBACKS_FAILURE",

};

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR });
export const stateReset = (resetState) => ({type: actionTypes.STATE_RESET,resetState,});

export const getFeedbackRequest = (queryParams) => ({type: actionTypes.GET_FEEDBACKS_REQUEST, queryParams});
export const getFeedbackFailure = (error) => ({type: actionTypes.GET_FEEDBACKS_FAILURE, error});
export const getFeedbackSuccess = (feedbacks) => ({type: actionTypes.GET_FEEDBACKS_SUCCESS, feedbacks});


const AuthActions = {
	actionTypes,

	clearError,
	stateReset,

    getFeedbackRequest,
    getFeedbackFailure,
    getFeedbackSuccess,

};

export default AuthActions;
